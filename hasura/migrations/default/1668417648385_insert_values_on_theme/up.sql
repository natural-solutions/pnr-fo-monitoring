
INSERT INTO "app"."theme"("id", "label") VALUES (E'biodiversity', E'Biodiversité');

INSERT INTO "app"."theme"("id", "label") VALUES (E' aquatic_environment', E'Milieu aquatique et ZH');

INSERT INTO "app"."theme"("id", "label") VALUES (E'culture', E'Culture');

INSERT INTO "app"."theme"("id", "label") VALUES (E'landscape', E'Paysage');

INSERT INTO "app"."theme"("id", "label") VALUES (E'architecture', E'Architecture');

INSERT INTO "app"."theme"("id", "label") VALUES (E'farming', E'Agriculture');

INSERT INTO "app"."theme"("id", "label") VALUES (E'forest', E'Forêt');

INSERT INTO "app"."theme"("id", "label") VALUES (E'planning_policy', E'Politique d\'aménagement');

INSERT INTO "app"."theme"("id", "label") VALUES (E'waste', E'Déchets');

INSERT INTO "app"."theme"("id", "label") VALUES (E'energy', E'Energie');

INSERT INTO "app"."theme"("id", "label") VALUES (E'skill', E'Savoir-faire');

INSERT INTO "app"."theme"("id", "label") VALUES (E'tourism', E'Tourisme');

INSERT INTO "app"."theme"("id", "label") VALUES (E'territorial_education', E'Education au territoire');
