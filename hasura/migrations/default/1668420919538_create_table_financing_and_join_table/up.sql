
CREATE TABLE "app"."financing" ("id" text NOT NULL, "label" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));

CREATE TABLE "app"."project_financing" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "project_id" uuid NOT NULL, "financing_id" text NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("project_id") REFERENCES "app"."project"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("financing_id") REFERENCES "app"."financing"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;
