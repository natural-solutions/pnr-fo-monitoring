CREATE TABLE "app"."project" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "created_at" timestamptz DEFAULT now(), "updated_at" timestamptz DEFAULT now(), "label" varchar, "title" varchar NOT NULL, "header" text NOT NULL, "photo" varchar NOT NULL, "status" varchar NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));
CREATE OR REPLACE FUNCTION "app"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_app_project_updated_at"
BEFORE UPDATE ON "app"."project"
FOR EACH ROW
EXECUTE PROCEDURE "app"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_app_project_updated_at" ON "app"."project" 
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
