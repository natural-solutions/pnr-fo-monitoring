
-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "app"."project" add column "description_short" text
--  null;

alter table "app"."project" alter column "document_name" drop not null;
alter table "app"."project" add column "document_name" text;
