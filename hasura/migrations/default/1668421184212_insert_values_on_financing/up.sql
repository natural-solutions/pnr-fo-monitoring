
INSERT INTO "app"."financing"("id", "label") VALUES (E'pnrfo', E'PnrFO');

INSERT INTO "app"."financing"("id", "label") VALUES (E'aesn', E'AESN');

INSERT INTO "app"."financing"("id", "label") VALUES (E'fgen', E'FGEN');

INSERT INTO "app"."financing"("id", "label") VALUES (E'feder', E'FEDER');

INSERT INTO "app"."financing"("id", "label") VALUES (E'feader', E'FEADER');

INSERT INTO "app"."financing"("id", "label") VALUES (E'leader', E'LEADER');

INSERT INTO "app"."financing"("id", "label") VALUES (E'grand_est_region', E'Région Grand Est');

INSERT INTO "app"."financing"("id", "label") VALUES (E'aube_department', E'Département de l\'Aube');

INSERT INTO "app"."financing"("id", "label") VALUES (E'haute_marne_department', E'Département de la Haute-Marne');

INSERT INTO "app"."financing"("id", "label") VALUES (E'grand_est_dreal', E'DREAL Grand Est');

INSERT INTO "app"."financing"("id", "label") VALUES (E'town', E'Commune');

INSERT INTO "app"."financing"("id", "label") VALUES (E'other', E'Autre');
