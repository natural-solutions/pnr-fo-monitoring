alter table "app"."territory" alter column "coords" drop not null;
alter table "app"."territory" add column "coords" point;
