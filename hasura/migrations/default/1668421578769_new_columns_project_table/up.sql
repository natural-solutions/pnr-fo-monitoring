
alter table "app"."project" add column "budget" numeric
 null;

alter table "app"."project" add column "contact_pnrfo" text
 null;

alter table "app"."project" add column "document_name" text
 null;

alter table "app"."project" add column "year" integer
 null;

alter table "app"."project" add column "public_description" text
 null;

alter table "app"."project" add column "private_description" text
 null;

alter table "app"."project" add column "owner" text
 null;

alter table "app"."project" add column "partner" text
 null;
