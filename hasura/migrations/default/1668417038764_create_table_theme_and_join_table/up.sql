
CREATE TABLE "app"."theme" ("id" text NOT NULL, "label" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));

CREATE TABLE "app"."project_theme" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "project_id" uuid NOT NULL, "theme_id" text NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("project_id") REFERENCES "app"."project"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("theme_id") REFERENCES "app"."theme"("id") ON UPDATE restrict ON DELETE restrict);
CREATE EXTENSION IF NOT EXISTS pgcrypto;
