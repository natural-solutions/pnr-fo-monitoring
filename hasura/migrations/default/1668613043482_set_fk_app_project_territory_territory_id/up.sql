alter table "app"."project_territory"
  add constraint "project_territory_territory_id_fkey"
  foreign key ("territory_id")
  references "app"."territory"
  ("id") on update restrict on delete restrict;
