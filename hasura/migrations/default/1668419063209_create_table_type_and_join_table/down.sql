
DROP TABLE "app"."project_type";

DELETE FROM "app"."type" WHERE "id" = 'works';

DELETE FROM "app"."type" WHERE "id" = 'entertainment';

DELETE FROM "app"."type" WHERE "id" = 'course';

DELETE FROM "app"."type" WHERE "id" = 'valuation';

DELETE FROM "app"."type" WHERE "id" = 'partnership';

DELETE FROM "app"."type" WHERE "id" = 'advice';

DELETE FROM "app"."type" WHERE "id" = 'financial_support';

DELETE FROM "app"."type" WHERE "id" = 'technical_support';

DROP TABLE "app"."type";
