
CREATE TABLE "app"."type" ("id" text NOT NULL, "label" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));

INSERT INTO "app"."type"("id", "label") VALUES (E'technical_support', E'Accompagnement technique');

INSERT INTO "app"."type"("id", "label") VALUES (E'financial_support', E'Accompagnement financier');

INSERT INTO "app"."type"("id", "label") VALUES (E'advice', E'Conseil');

INSERT INTO "app"."type"("id", "label") VALUES (E'partnership', E'Partenariat');

INSERT INTO "app"."type"("id", "label") VALUES (E'valuation', E'Valorisation');

INSERT INTO "app"."type"("id", "label") VALUES (E'course', E'Formation');

INSERT INTO "app"."type"("id", "label") VALUES (E'entertainment', E'Animation');

INSERT INTO "app"."type"("id", "label") VALUES (E'works', E'Travaux');

CREATE TABLE "app"."project_type" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "project_id" uuid NOT NULL, "type_id" text NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("project_id") REFERENCES "app"."project"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("type_id") REFERENCES "app"."type"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;
