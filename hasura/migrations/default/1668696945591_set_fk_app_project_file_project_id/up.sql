alter table "app"."project_file"
  add constraint "project_file_project_id_fkey"
  foreign key ("project_id")
  references "app"."project"
  ("id") on update cascade on delete cascade;
