
CREATE TABLE "app"."territory" ("id" numeric NOT NULL, "label" text NOT NULL, "coords" point NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));

CREATE TABLE "app"."project_territory" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "project_id" uuid NOT NULL, "territory_id" numeric NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("project_id") REFERENCES "app"."project"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("territory_id") REFERENCES "app"."territory"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;
