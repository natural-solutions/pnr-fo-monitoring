# Getting started

```bash
npm start
```

and go to:

http://monitoring.localdomain:8888

## Exemples npm run

Create seed

```bash
npm run hasura:seed:create territories -- --from-table app.territory
```

## Production

**Caution**

```bash
export ENV=production
```

**Hasura CLI**  
Enter in the Hasura container and run

```bash
/usr/bin/hasura-cli --endpoint http://hasura:8080 --project /tmp/hasura-project --admin-secret <HASURA_GRAPHQL_ADMIN_SECRET> <your_cmd>
```
