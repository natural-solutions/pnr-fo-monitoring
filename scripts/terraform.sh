#!/usr/bin/env bash

set -e

source ./docker/.env

terraform() {
    docker run --env-file "$(pwd)/terraform/.env" --rm \
        --network "$PROJECT"  \
        --volume "$(pwd)/terraform":/workspace \
        -w /workspace \
        -it hashicorp/terraform:light "$@"
}

terraform init 
terraform apply -auto-approve