#!/usr/bin/env bash

set -e

# Pre-pre-flight? 🤷
if [[ -n "$MSYSTEM" ]]; then
    echo "Seems like you are using an MSYS2-based system (such as Git Bash) which is not supported. Please use WSL instead.";
    exit 1
fi

# shellcheck source=/dev/null
source ./docker/.env

DOCKER_VERSION=$(docker version --format '{{.Server.Version}}')
VERSION_PARTS=(${DOCKER_VERSION//./ })

if ((${VERSION_PARTS[0]} < 24 || (${VERSION_PARTS[0]} == 0 && ${VERSION_PARTS[1]} < 0))); then
    COMPOSE_COMMAND='docker-compose'
else
    COMPOSE_COMMAND='docker compose'
fi


if [ "$ENV" == "production" ]; then
    $COMPOSE_COMMAND --project-name="${PROJECT}" -f ./docker/docker-compose.yml -f ./docker/docker-compose.prod.yml -f ./docker/docker-compose.backup.yml "$@"
elif [ "$ENV" == "proxy" ]; then
    $COMPOSE_COMMAND --project-name="${PROJECT}" -f ./docker/docker-compose.yml -f ./docker/docker-compose.backup.yml -f ./docker/docker-compose.proxy.yml "$@"
else
    $COMPOSE_COMMAND --project-name="${PROJECT}" --project-directory=./docker "$@"
fi
