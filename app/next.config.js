/** @type {import('next').NextConfig} */
const withPlugins = require("next-compose-plugins");
const removeImports = require("next-remove-imports")();
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  i18n: {
    locales: ["en", "fr"],
    defaultLocale: "fr",
  },
  publicRuntimeConfig: {
    GRAPHQL_API_URL: process.env.GRAPHQL_API_URL,
    IMAGOR_SERVER_PATH_PREFIX: process.env.IMAGOR_SERVER_PATH_PREFIX,
    KEYCLOAK_ISSUER: process.env.KEYCLOAK_ISSUER,
    KEYCLOACK_ADMIN_URL: `${process.env.CANONICAL_URL}/auth/admin/realms/${process.env.KEYCLOAK_REALM_ID}`,
    KEYCLOAK_CLIENT_ID: process.env.KEYCLOAK_CLIENT_ID,
  },
  serverRuntimeConfig: {
    KEYCLOAK_CLIENT_SECRET: process.env.KEYCLOAK_CLIENT_SECRET,
  },
};

module.exports = withPlugins([removeImports({})], nextConfig);
