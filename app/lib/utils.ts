//type

import add from "date-fns/add";
import format from "date-fns/format";
import {
  ErrorMessage,
  ObjectName,
  QueryVariables,
  QueryVariablesStr,
  TProject,
  TFile,
} from "../types/app-types";
import { IMAGOR_PRESET_NAME, OBJ_CONF } from "./constants";
import { set } from "lodash";
import { GraphQLErrors } from "@apollo/client/errors";
import { GetServerSidePropsContext, GetServerSidePropsResult } from "next";
import { remark } from "remark";
import html from "remark-html";
import { App_Project_Bool_Exp } from "../generated/graphql";
import getConfig from "next/config";

export const slugify = (str: string) => {
  str = str.replace(/^\s+|\s+$/g, "");
  str = str.toLowerCase();
  const from =
    "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/,:;";
  const to =
    "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa-----";
  for (let i = 0; i < from.length; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  str = str
    .replace(/\s+/g, "-")
    .replace(/[^a-z0-9\.\-_]/g, "")
    .replace(/-+/g, "-");

  return str;
};

export const getCtxURL = (ctx: GetServerSidePropsContext) => {
  const url = new URL(`http://XXX${ctx.resolvedUrl}`);
  let ctxUrl = url.pathname;
  const params = Array.from(url.searchParams.keys()).map((param) => {
    return `${param}=${url.searchParams.get(param)}`;
  });
  if (params.length) {
    ctxUrl += `?${params.join("&")}`;
  }

  return ctxUrl;
};

export const handleGraphQLErrorsServerSide = <P>(
  graphQLErrors: GraphQLErrors,
  ctx: GetServerSidePropsContext
): GetServerSidePropsResult<P> => {
  for (let err of graphQLErrors) {
    console.log("err", err);

    switch (err.extensions.code) {
      case "invalid-jwt":
        return {
          redirect: {
            permanent: false,
            destination: `/login?callback_url=${encodeURIComponent(
              getCtxURL(ctx)
            )}`,
          },
        };
    }
  }
  return {
    props: {} as any,
  };
};

export const safeJSONParse = (input: any) => {
  try {
    return JSON.parse(input);
  } catch (error) {}
};

export const getMinioUrl = (path: string) => {
  // TODO get path prefix from .env
  return `/upload/${path}`;
};

export const markdownToHtml = async (input?: string | null) => {
  const result = await remark()
    .use(html)
    .process(input || "");
  return result.toString();
};

export const getProjectImgURL = (
  project: TProject | string,
  preset: IMAGOR_PRESET_NAME,
  filename?: string | null
) => {
  if (!filename) {
    return "";
  }
  const { publicRuntimeConfig } = getConfig();
  const projectId = typeof project == "string" ? project : project.id;

  return `${publicRuntimeConfig.IMAGOR_SERVER_PATH_PREFIX}/unsafe/${preset}/${projectId}/${filename}`;
};

const STATUS_MESSAGES: {
  [key: number]: string;
} = {
  400: "Bad Request",
  401: "Vous n'êtes pas connecté.",
  403: "Vous n'avez pas suffisament de droit.",
  409: "Cet objet existe déjà.",
};

export const getErrorMessage = async (
  label: string,
  response: Response | any
): Promise<ErrorMessage> => {
  const responseErrors: string[] = [];
  if (response.errors) {
    for (let err of response.errors as GraphQLErrors) {
      if (
        err.message === "no mutations exist" ||
        err.extensions.code == "invalid-jwt"
      ) {
        responseErrors.push("401");
      } else if (err.message) {
        responseErrors.push(err.message);
      } else if (err.extensions.code) {
        responseErrors.push(err.extensions.code as string);
      }
    }
  } else if (response.error?.message) {
    responseErrors.push(response.error?.message);
  } else if (response.message) {
    responseErrors.push(response.error?.message);
  } else if (response instanceof Response) {
    try {
      responseErrors.push(await response.text());
    } catch (error) {
      responseErrors.push(await response.clone().text());
    }
  } else if (typeof response == "string") {
    responseErrors.push(response);
  }

  return {
    label,
    message: responseErrors.length
      ? responseErrors.join(", ")
      : STATUS_MESSAGES[response.status] || "",
  };
};

export const getProjectUploadURL = async (projectId: string, file: TFile) => {
  const presignedResp = await fetch(
    `/api/projects/${projectId}/files/presigned_put?filename=${file.slugified}`
  );
  if (presignedResp.status != 200) {
    return Promise.reject(presignedResp);
  }

  return (await presignedResp.json()).url as string;
};

export const uploadProjectFile = async (projectId: string, file: TFile) => {
  try {
    const url = await getProjectUploadURL(projectId, file);
    return await uploadFile(file, url);
  } catch (error) {
    console.log("err", error);
    return Promise.reject(error);
  }
};

export const uploadFile = async (file: TFile, uploadUrl: string) => {
  try {
    const uploadResp = await fetch(uploadUrl, {
      method: "PUT",
      body: file,
    });
    if (uploadResp.status != 200) {
      const xmlStr = await uploadResp.text();
      const xml = new window.DOMParser().parseFromString(xmlStr, "text/xml");
      const messages = xml.getElementsByTagName("Message");

      return Promise.reject(messages[0].textContent);
    }
    return uploadResp;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deserializeRouterQuery = (
  input: QueryVariablesStr
): QueryVariables => {
  const res: QueryVariables = {};
  const page = Number(input.page);
  if (!isNaN(page)) {
    res.page = Math.max(1, page);
  }
  const limit = Number(input.limit);
  if (!isNaN(limit)) {
    res.limit = limit;
  }
  const order_by = safeJSONParse(input.order_by);
  if (order_by) {
    res.order_by = order_by;
  }
  const where = safeJSONParse(input.where);
  if (where) {
    res.where = where;
  }

  if (input.search) {
    res.search = input.search;
  }

  return res;
};

export const variablesToQs = (
  input: QueryVariables,
  options?: {
    exclude?: ("page" | "limit" | "order_by" | "where" | "search")[];
  }
): string => {
  const serialized: QueryVariablesStr = {
    page: (input.page || 1).toString(),
  };
  const limit = input.limit;
  if (!isNaN(input.limit!)) {
    serialized.limit = limit?.toString();
  }
  if (Array.isArray(input.order_by) && input.order_by.length > 0) {
    serialized.order_by = JSON.stringify(input.order_by);
  }

  if (Array.isArray(input.where) && input.where.length > 0) {
    serialized.where = JSON.stringify(input.where);
  }

  if (input.search) {
    serialized.search = input.search;
  }

  const qs = Object.keys(serialized)
    .filter((key) => !(options?.exclude || []).includes(key as any))
    .map((key) => {
      return `${key}=${(serialized as any)[key]}`;
    })
    .join("&");

  return qs;
};

export const qsFiltersToGraphQL = (
  objName: ObjectName,
  filters: any[],
  search?: string
) => {
  const objConf = OBJ_CONF[objName];
  const _and: App_Project_Bool_Exp[] = [];
  filters.forEach((filter) => {
    _and.push(
      filterToGraphQL({
        ...filter,
        ...objConf[filter.id],
      })
    );
  });
  if (search) {
    _and.push({
      _or: [
        "contact_pnrfo",
        "description_short",
        "header",
        "label",
        "owner",
        "partner",
        "public_description",
        "title",
        "project_financings.financing.label",
        "project_territories.territory.label",
        "project_themes.theme.label",
        "project_types.type.label",
      ].map((field) => {
        return set({}, field, {
          _ilike: `%${search}%`,
        });
      }),
    });
  }
  return { _and };
};

// TODO merge with constants.COL_TYPE_CONF ?
const filterToGraphQL = (filter: any) => {
  const conf: any = {
    uuid: {
      equals: () => ({
        _eq: filter.value,
      }),
      in: () => ({
        _in: filter.value,
      }),
    },
    select: {
      in: () => ({
        _in: filter.value,
      }),
      equals: () => ({
        _eq: filter.value,
      }),
      notEquals: () => ({
        _neq: filter.value,
      }),
    },
    "multi-select": {
      in: () => ({
        _in: filter.value,
      }),
      equals: () => ({
        _in: filter.value,
      }),
      notEquals: () => ({
        _nin: filter.value,
      }),
    },
    text: {
      equals: () => ({
        _eq: filter.value,
      }),
      notEquals: () => ({
        _neq: filter.value,
      }),
      in: () => ({
        _in: filter.value,
      }),
      contains: () => ({
        _ilike: `%${filter.value}%`,
      }),
      startsWith: () => ({
        _ilike: `${filter.value}%`,
      }),
      endsWith: () => ({
        _ilike: `%${filter.value}`,
      }),
      //TODO
      empty: () => ({}),
      //TODO
      notEmpty: () => ({}),
    },
    date: {
      equals: () => ({
        _gte: filter.value,
        _lt: format(
          add(new Date(filter.value), {
            days: 1,
          }),
          "yyyy-MM-dd"
        ),
      }),
      notEquals: {
        _or: () => {
          return [
            {
              [filter.id]: {
                _lt: filter.value,
              },
            },
            {
              [filter.id]: {
                _gte: format(
                  add(new Date(filter.value), {
                    days: 1,
                  }),
                  "yyyy-MM-dd"
                ),
              },
            },
          ];
        },
      },
      in: () => ({
        _in: filter.value,
      }),
      greaterThanOrEqualTo: () => ({
        _gte: filter.value,
      }),
      lessThanOrEqualTo: () => ({
        _lte: filter.value,
      }),
      lessThan: () => ({
        _lt: filter.value,
      }),
      //TODO
      betweenInclusive: () => {
        return {};
        /* filter.value[0] = format(filter.value[0], "yyyy-MM-dd");
        filter.value[1] = !filter.value[1]
          ? filter.value[1]
          : format(filter.value[1], "yyyy-MM-dd");
        return {
          _gte: filter.value[0],
          _lte: format(
            add(filter.value[1], {
              days: 1,
            }),
            "yyyy-MM-dd"
          ),
        }; */
      },
    },
    number: {
      equals: () => ({
        _eq: filter.value,
      }),
      notEquals: {
        _neq: filter.value,
      },
      in: () => ({
        _in: (filter.value || []).map((val: any) => val || 0),
      }),
      greaterThanOrEqualTo: () => ({
        _gte: filter.value,
      }),
      greaterThan: () => ({
        _gt: filter.value,
      }),
      lessThanOrEqualTo: () => ({
        _lte: filter.value,
      }),
      lessThan: () => ({
        _lt: filter.value,
      }),
      betweenInclusive: () => ({
        _gte: filter.value[0],
        _lte: filter.value[1],
      }),
      between: () => ({
        _gt: filter.value[0],
        _lt: filter.value[1],
      }),
    },
  };

  const val = conf[filter.filter || filter.type]?.[filter.operator];
  if (typeof val == "function") {
    return set({}, filter.id, val?.());
  }

  const res: any = {};
  for (const key in val) {
    if (Object.prototype.hasOwnProperty.call(val, key)) {
      const element = val[key];
      res[key] = val[key]();
    }
  }

  return res;
};
