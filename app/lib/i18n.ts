import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import CommonFR from "../public/locales/fr/common.json";

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    preload: ["fr"],
    lng: "fr",
    fallbackLng: "fr",
    ns: ["common"],
    nsSeparator: ".",
    defaultNS: "common",
    resources: {
      fr: {
        common: CommonFR,
      },
    },
    interpolation: {
      escapeValue: false,
    },
    react: {
      useSuspense: false,
    },
  });

export default i18n;
