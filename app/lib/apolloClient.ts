import { DefaultOptions, from, InMemoryCache } from "@apollo/client";
import { ApolloClient } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { onError } from "@apollo/client/link/error";
import { createUploadLink } from "apollo-upload-client";
import { Session } from "next-auth";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
const isServer = typeof window === "undefined";

const getAuthLink = (session?: Session) => {
  return setContext(async (_request, previousContext) => {
    const token = session?.accessToken;
    const error = session?.error;

    /* if (error === "RefreshAccessTokenError") {
      await signOut({ redirect: false });
      await signIn("keycloak");
      return;
    } */

    if (token) {
      return {
        headers: { Authorization: `Bearer ${token}` },
      };
    }

    return {
      headers: previousContext.headers,
    };
  });
};

const errorLink = onError(({ graphQLErrors, operation, forward }) => {
  if (graphQLErrors) {
    for (let err of graphQLErrors) {
      switch (err.extensions.code) {
        case "invalid-jwt":
          // TODO Try to modify the operation context with a new token
          const oldHeaders = operation.getContext().headers || {};
          delete oldHeaders.Authorization;
          operation.setContext({
            headers: {
              ...oldHeaders,
              //Authorization: getNewToken()
            },
          });

          return forward(operation);
      }
    }
  }
});

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: "no-cache",
    errorPolicy: "ignore",
  },
  query: {
    fetchPolicy: "no-cache",
    errorPolicy: "all",
  },
};

export const initializeApollo = (
  session?: Session | null,
  options = {
    isAnonymous: false,
  }
) => {
  const newHttpLink = createUploadLink({
    uri: publicRuntimeConfig.GRAPHQL_API_URL,
    credentials: "include",
    /* headers: session?.accessToken
      ? {
          Authorization: `Bearer ${session?.accessToken}`,
        }
      : {}, */
  });

  const links = [errorLink, newHttpLink];
  if (!options?.isAnonymous && session) {
    links.unshift(getAuthLink(session));
  }

  return new ApolloClient({
    ssrMode: isServer,
    link: from(links),
    cache: new InMemoryCache(),
    defaultOptions,
  });
};
