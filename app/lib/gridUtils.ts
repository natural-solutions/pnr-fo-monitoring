import { MRT_ColumnDef } from "material-react-table";
import { ObjectName, QueryVariables } from "../types/app-types";
import { COL_TYPE_FILTER_OPERATORS, OBJ_CONF } from "./constants";

export const getGridBuilder = <Type extends Record<string, any>>(
  obj: ObjectName,
  filters: QueryVariables["where"]
) => {
  const objConf = OBJ_CONF[obj];
  return {
    getColProps(
      colName: string,
      label: string = "",
      otherProps: Omit<MRT_ColumnDef<Type>, "header"> = {}
    ) {
      const colConf = objConf[colName];
      return {
        accessorKey: colName,
        header: label,
        meta: objConf[colName],
        filterFn: getCurColumnFilterMode(obj, colName, filters),
        columnFilterModeOptions: getColumnFilterModeOptions(obj, colName),
        filterVariant: colConf.filter,
        //TODO better
        muiTableHeadCellFilterTextFieldProps:
          (colConf.filter || colConf.type) !== "date"
            ? undefined
            : {
                type: "date",
              },
        ...otherProps,
      } as MRT_ColumnDef<Type>;
    },
  };
};

const getColumnFilterModeOptions = (
  obj: ObjectName,
  col: string
): typeof COL_TYPE_FILTER_OPERATORS[keyof typeof COL_TYPE_FILTER_OPERATORS] => {
  const objConf = OBJ_CONF[obj];
  const colConf = objConf[col];
  const filterName = colConf.filter || colConf.type;
  return COL_TYPE_FILTER_OPERATORS[filterName];
};

const getCurColumnFilterMode = (
  obj: ObjectName,
  col: string,
  filters: QueryVariables["where"]
) => {
  const operator = filters?.find((filter) => {
    return filter.id == col;
  })?.operator;
  const operators = getColumnFilterModeOptions(obj, col);
  return operators.includes(operator) ? operator : operators[0];
};
