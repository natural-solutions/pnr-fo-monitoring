import { ThemeOptions } from "@mui/material/styles";

const theme: ThemeOptions = {
  palette: {
    mode: "light",
    neutral: {
      main: "#707070",
      contrastText: "#fff",
    },
    primary: {
      main: "#8F5C9E",
    },
  },
  typography: {
    subtitle1: {
      fontSize: "1.25rem",
    },
  },
};

export default theme;
