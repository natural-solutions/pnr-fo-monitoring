import { GraphQLErrors } from "@apollo/client/errors";
import { useAppContext } from "./AppContext";
import { getErrorMessage } from "./utils";

export default function useHandleErrors() {
  const appContext = useAppContext();

  const handleResponse = async (
    response: Response | any,
    errorTitle: string = ""
  ) => {
    console.log("handleResponse Error", response);

    if (response.status === 401) {
      return appContext.loginDialog.show();
    }

    if (response.errors) {
      for (let err of response.errors as GraphQLErrors) {
        if (
          err.message === "no mutations exist" ||
          err.extensions.code == "invalid-jwt"
        ) {
          return appContext.loginDialog.show();
        }
      }
    }

    const title = `${response.status ? `${response.status}: ` : ""}${
      errorTitle || "Erreur"
    }`;
    const { message } = await getErrorMessage("", response);

    appContext.errorDialog.show({
      title,
      message,
    });
  };

  return {
    handleResponse,
  };
}
