import * as Minio from "minio";
import { IMAGOR_PRESETS } from "./constants";

export const getMinioClient = (env: typeof process.env) => {
  return new Minio.Client({
    endPoint: "minio",
    port: 8888,
    useSSL: false,
    accessKey: env.MINIO_ROOT_USER!,
    secretKey: env.MINIO_ROOT_PASSWORD!,
  });
};

export const getMinioClientDistant = (env: typeof process.env) => {
  const useSSL = env.MINIO_USE_SSL === "1";
  return new Minio.Client({
    endPoint: env.DOMAIN!,
    port: useSSL ? 443 : 8888,
    useSSL,
    accessKey: env.MINIO_ROOT_USER!,
    secretKey: env.MINIO_ROOT_PASSWORD!,
  });
};

export const removeFromMinioAndImagor = async (
  env: typeof process.env,
  objectName: string
) => {
  const minio = getMinioClient(env);
  const bucketLoader = env.MINIO_BUCKET_LOADER!;
  const bucketStorage = env.MINIO_BUCKET_STORAGE!;
  const bucketResult = env.MINIO_BUCKET_RESULT!;
  try {
    await minio.removeObject(bucketLoader, objectName);
  } catch (error) {
    console.log("removeObject ERR", bucketLoader, objectName, error);
  }
  try {
    const minio = getMinioClient(env);
    await minio.removeObject(bucketStorage, objectName);
  } catch (error) {
    console.log("removeObject ERR", bucketStorage, objectName);
  }
  for (const key in IMAGOR_PRESETS) {
    try {
      await minio.removeObject(bucketResult, `${key}/${objectName}`);
    } catch (error) {
      console.log("removeObject ERR", bucketResult, `${key}/${objectName}`);
    }
  }
};
