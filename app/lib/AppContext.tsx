import { useContext, createContext, useState, FC, useEffect } from "react";
import ErrorDialog, {
  ErrorDialogProps,
} from "../components/dialog/ErrorDialog";
import LoginDialog from "../components/dialog/LoginDialog";
import {
  manuallyIncrementPromiseCounter,
  manuallyDecrementPromiseCounter,
  usePromiseTracker,
} from "react-promise-tracker";
import AppLoader from "../components/dialog/AppLoader";
import { OnBoarding } from "../components/OnBoarding";
import { useRouter } from "next/router";

const StoreContext = createContext({} as any);

export const AppContextProvider: FC<any> = ({ children }) => {
  const router = useRouter();

  useEffect(() => {
    const onRouteChange = () => {
      setIsOnBoardingOpen(false);
    };
    router.events.on("routeChangeStart", onRouteChange);

    return () => {
      router.events.off("routeChangeStart", onRouteChange);
    };
  }, []);

  const { promiseInProgress } = usePromiseTracker({
    area: "loader",
  });
  const loader = {
    show() {
      manuallyIncrementPromiseCounter("loader");
    },
    hide() {
      setTimeout(() => {
        manuallyDecrementPromiseCounter("loader");
      }, 100);
    },
  };
  const [errorDialogProps, setErrorDialogProps] =
    useState<ErrorDialogProps | null>(null);
  const errorDialog = {
    show(props: ErrorDialogProps) {
      setErrorDialogProps({ ...props, isOpen: true });
    },
    hide() {
      setErrorDialogProps(null);
    },
  };

  const [isLoginDialOpen, setIsLoginDialOpen] = useState(false);
  const loginDialog = {
    show() {
      setIsLoginDialOpen(true);
    },
    hide() {
      setIsLoginDialOpen(false);
    },
  };

  const onLoginClose = () => {
    setIsLoginDialOpen(false);
  };

  const [isOnBoardingOpen, setIsOnBoardingOpen] = useState(false);
  const [hasOnBoardingShown, setHasOnBoardingShown] = useState(false);
  const onBoarding = {
    show() {
      setHasOnBoardingShown(true);
      setIsOnBoardingOpen(true);
    },
    tryAutoShow() {
      if (hasOnBoardingShown) {
        return;
      }
      if (["1", null].includes(localStorage.getItem("isOnBoardingAutoOpen"))) {
        setHasOnBoardingShown(true);
        setIsOnBoardingOpen(true);
      }
    },
  };

  return (
    <StoreContext.Provider
      value={{
        loader,
        errorDialog,
        loginDialog,
        onBoarding,
      }}
    >
      {children}
      <AppLoader isOpen={promiseInProgress} />
      {errorDialogProps && (
        <ErrorDialog
          handleClose={() => setErrorDialogProps(null)}
          {...errorDialogProps}
        />
      )}
      <LoginDialog open={isLoginDialOpen} onClose={onLoginClose} />
      <OnBoarding
        open={isOnBoardingOpen}
        onClose={() => setIsOnBoardingOpen(false)}
      />
    </StoreContext.Provider>
  );
};

export const useAppContext = (): {
  loader: {
    show: () => void;
    hide: () => void;
  };
  errorDialog: {
    show: (props: Partial<ErrorDialogProps>) => void;
    hide: () => void;
  };
  loginDialog: {
    show: () => void;
    hide: () => void;
  };
  onBoarding: {
    show: () => void;
    tryAutoShow: () => void;
  };
} => useContext(StoreContext);
