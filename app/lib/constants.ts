export enum IMAGOR_PRESET_NAME {
  "50x50" = "50x50",
  "200x200" = "200x200",
  "200x280" = "200x280",
  "300x225" = "300x225",
  "500x375" = "500x375",
  "800x600" = "800x600",
}

export const IMAGOR_PRESETS = {
  [IMAGOR_PRESET_NAME["50x50"]]: "50x50",
  [IMAGOR_PRESET_NAME["200x200"]]: "200x200",
  [IMAGOR_PRESET_NAME["200x280"]]: "200x280",
  [IMAGOR_PRESET_NAME["300x225"]]: "300x225",
  [IMAGOR_PRESET_NAME["500x375"]]: "500x375",
  [IMAGOR_PRESET_NAME["800x600"]]: "800x600",
} as const;

export const PROJECT_STEPS = [
  "cancelled",
  "to_launch",
  "in_progress",
  "complete",
] as const;
export const PROJECT_STATUSES = ["draft", "online", "offline"] as const;

export const PAGE_SIZE = 30;

export const OBJ_CONF: {
  project: {
    [colName: string]: {
      type: string;
      filter?: string;
    };
  };
} = {
  project: {
    id: {
      type: "uuid",
    },
    budget: {
      type: "number",
    },
    contact_pnrfo: {
      type: "text",
    },
    created_at: {
      type: "date",
    },
    header: {
      type: "text",
    },
    label: {
      type: "text",
    },
    owner: {
      type: "text",
    },
    partner: {
      type: "text",
    },
    photo: {
      type: "text",
    },
    private_description: {
      type: "text",
    },
    "project_territories.territory.id": {
      type: "text",
      filter: "multi-select",
    },
    "project_themes.theme.id": {
      type: "text",
      filter: "multi-select",
    },
    "project_types.type.id": {
      type: "text",
      filter: "multi-select",
    },
    public_description: {
      type: "text",
    },
    status: {
      type: "text",
      filter: "select",
    },
    step: {
      type: "text",
      filter: "select",
    },
    title: {
      type: "text",
    },
    updated_at: {
      type: "date",
    },
    year: {
      type: "number",
    },
  },
} as const;

// https://www.material-react-table.com/docs/guides/column-filtering#custom-filter-functions-per-column
export const COL_TYPE_FILTER_OPERATORS: {
  [x: string]: readonly string[];
} = {
  uuid: ["equals", "notEquals"],
  select: ["equals", "notEquals"],
  "multi-select": ["equals", "notEquals"],
  date: [
    "equals",
    "notEquals",
    "between",
    "betweenInclusive",
    "greaterThan",
    "greaterThanOrEqualTo",
    "lessThan",
    "lessThanOrEqualTo",
    "empty",
    "notEmpty",
  ],
  text: [
    "contains",
    "startsWith",
    "endsWith",
    "equals",
    "notEquals",
    "empty",
    "notEmpty",
  ],
  number: [
    "equals",
    "notEquals",
    "between",
    "betweenInclusive",
    "greaterThan",
    "greaterThanOrEqualTo",
    "lessThan",
    "lessThanOrEqualTo",
    "empty",
    "notEmpty",
  ],
} as const;

export const EXPORT_COLS = [
  {
    name: "step",
    label: "État",
  },
  {
    name: "label",
    label: "Titre court",
  },
  {
    name: "year",
    label: "Année",
  },
  {
    name: "budget",
    label: "Budget",
  },
  {
    name: "contact_pnrfo",
    label: "Contact PNR FO",
  },
  {
    name: "partner",
    label: "Partenaires",
  },
  {
    name: "owner",
    label: "Porteur",
  },
  {
    name: "project_territories",
    label: "Territoires",
    path: "territory.label",
  },
  {
    name: "project_financings",
    label: "Financement(s)",
    path: "financing.label",
  },
  {
    name: "project_themes",
    label: "Thèmes",
    path: "theme.label",
  },
  {
    name: "project_types",
    label: "Type",
    path: "type.label",
  },
  {
    name: "status",
    label: "Publication",
  },
  {
    name: "title",
    label: "Titre long",
  },
  {
    name: "public_description",
    label: "Description publique",
  },
  {
    name: "private_description",
    label: "Description privée",
  },
  {
    name: "created_at",
    label: "Créé le",
    type: "date",
  },
  {
    name: "updated_at",
    label: "Modifié le",
    type: "date",
  },
  {
    name: "deleted_at",
    label: "Supprimé le",
    type: "date",
  },
  {
    name: "id",
    label: "ID",
  },
];
