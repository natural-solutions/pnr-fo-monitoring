import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  InputAdornment,
  MenuItem,
  Paper,
  Stack,
  Step,
  StepContent,
  StepLabel,
  Stepper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { useRouter } from "next/router";
import { Box } from "@mui/system";
import { pick } from "lodash";
import React, { FC, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  FetchDataForProjectFormQuery,
  FetchProjectByPkQuery,
  InsertProjectDocument,
  InsertProjectMutation,
  InsertProjectMutationVariables,
  UpdateFullProjectByPkMutationVariables,
  UpdateProjectByPkDocument,
  UpdateProjectByPkMutation,
  UpdateProjectByPkMutationVariables,
  useSoftDeleteOneProjectMutation,
} from "../../generated/graphql";
import { AppSelect } from "../form-input/AppSelect";
import { AppTextField } from "../form-input/AppTextField";
import { CancelBtn } from "../form-input/CancelBtn";
import { SubmitBtn } from "../form-input/SubmitBtn";
import { MDEditorComponent as MDEditor } from "../form-input/MDEditor";
import { TFile, TProject, TProjectFile } from "../../types/app-types";
import { FileIcon } from "../FileIcon";
import {
  IMAGOR_PRESET_NAME,
  PROJECT_STATUSES,
  PROJECT_STEPS,
} from "../../lib/constants";
import { Delete, Edit, QuestionMark } from "@mui/icons-material";
import { ProjectFileForm } from "./ProjectFileForm";
import { useApolloClient } from "@apollo/client";
import { AppInputFile } from "../form-input/AppInputFile";
import { getProjectImgURL, uploadProjectFile } from "../../lib/utils";
import { DeleteBtn } from "../form-input/DeleteBtn";
import { useTranslation } from "react-i18next";
import useHandleErrors from "../../lib/useHandleErrors";
import ErrorDialog from "../dialog/ErrorDialog";
import MessageDialog from "../dialog/MessageDialog";
import { useAppContext } from "../../lib/AppContext";

Yup.setLocale({
  mixed: {
    default: "Champ non valide",
    required: "Champ requis",
    notType: "Valeur non valide",
  },
  array: {
    min: "Au moins un item est requis",
  },
});

const validationSchema = Yup.object({
  year: Yup.number().nullable().required(),
  label: Yup.string().nullable().required(),
  title: Yup.string().nullable().required(),
  project_types: Yup.array().min(1).required(),
  project_themes: Yup.array().min(1).required(),
  project_territories: Yup.array().min(1).required(),
  public_description: Yup.string().nullable().required(),
  contact_pnrfo: Yup.string().nullable().required(),
});

interface ProjectFormProps {
  project?: FetchProjectByPkQuery["app_project_by_pk"];
  dataset: FetchDataForProjectFormQuery;
}

export const ProjectForm: FC<ProjectFormProps> = ({ project, dataset }) => {
  const formApi = useForm<any>({
    resolver: yupResolver(validationSchema),
  });
  const client = useApolloClient();
  const [softDeleteOneProject] = useSoftDeleteOneProjectMutation();
  const { t } = useTranslation(["common"]);

  const theme = useTheme();
  const isSM = useMediaQuery(theme.breakpoints.up("sm"));

  const [editPhoto, setEditPhoto] = useState<TFile>();

  const [files, setFiles] = useState<TProjectFile[]>(
    project?.project_files || []
  );
  const [curFile, setCurFile] = useState<TProjectFile>();
  const [fileToDelete, setFileToDelete] = useState<TProjectFile>();
  const [deleteProject, setDeleteProject] = useState<boolean>(false);
  const [showErrorDialog, setShowErrorDialog] = useState<boolean>(false);
  const [showSuccessDialog, setshowSuccessDialog] = useState<boolean>(false);
  const [photoError, setPhotoError] = useState<boolean>(false);
  const router = useRouter();
  const handleErrors = useHandleErrors();
  const appCtx = useAppContext();

  useEffect(() => {
    if (project) {
      formApi.reset(project);
      formApi.setValue(
        "project_themes",
        project?.project_themes.map((p) => {
          return p.theme.id;
        })
      );
      formApi.setValue(
        "project_territories",
        project?.project_territories.map((p) => {
          return p.territory.id;
        })
      );
      formApi.setValue(
        "project_types",
        project?.project_types.map((p) => {
          return p.type.id;
        })
      );
      formApi.setValue(
        "project_financings",
        project?.project_financings.map((p) => {
          return p.financing.id;
        })
      );
    }
  }, [project]);

  useEffect(() => {
    if (curFile) {
      window.scrollTo(0, document.body.scrollHeight);
    }
  }, [curFile]);

  const onError = async () => {
    setShowErrorDialog(true);
  };

  const onSubmit = async () => {
    const formData = formApi.getValues();

    if (!editPhoto && !formData.photo) {
      setPhotoError(true);
      setShowErrorDialog(true);
    } else {
      appCtx.loader.show();
      const obj = !formData.id
        ? {
            ...pick(formData, [
              "label",
              "budget",
              "contact_pnrfo",
              "description_short",
              "title",
              "public_description",
              "private_description",
              "owner",
              "status",
              "step",
              "year",
              "partner",
              "type",
            ]),
            project_themes: (formData.project_themes || []).map(
              (themeId: string) => {
                return { theme_id: themeId };
              }
            ),
            project_types: (formData.project_types || []).map(
              (typeId: string) => {
                return { type_id: typeId };
              }
            ),
            project_financings: (formData.project_financings || []).map(
              (financingId: string) => {
                return { financing_id: financingId };
              }
            ),
            project_territories: (formData.project_territories || []).map(
              (territoryId: string) => {
                return { territory_id: territoryId };
              }
            ),
          }
        : {
            ...pick(formData, [
              "label",
              "budget",
              "contact_pnrfo",
              "description_short",
              "title",
              "public_description",
              "private_description",
              "owner",
              "status",
              "step",
              "year",
              "partner",
              "type",
            ]),
          };

      try {
        const result = !formData.id
          ? await insert(obj)
          : await update({
              id: formData.id,
              _set: obj,
              project_themes: formData.project_themes?.map(
                (projectTheme: any) => {
                  return {
                    theme_id:
                      typeof projectTheme === "string"
                        ? projectTheme
                        : projectTheme.theme.id,
                    project_id: formData.id,
                  };
                }
              ),
              project_territories: formData.project_territories?.map(
                (projectTerritory: any) => {
                  return {
                    territory_id:
                      typeof projectTerritory === "string"
                        ? projectTerritory
                        : projectTerritory.territory.id,
                    project_id: formData.id,
                  };
                }
              ),
              project_financings: formData.project_financings?.map(
                (projectFinancing: any) => {
                  return {
                    financing_id:
                      typeof projectFinancing === "string"
                        ? projectFinancing
                        : projectFinancing.financing.id,
                    project_id: formData.id,
                  };
                }
              ),
              project_types: formData.project_types?.map((projectType: any) => {
                return {
                  type_id:
                    typeof projectType === "string"
                      ? projectType
                      : projectType.type.id,
                  project_id: formData.id,
                };
              }),
            });

        setshowSuccessDialog(true);
        !formData.id
          ? router.push(`/admin/projects/${result.id}`)
          : router.replace(`/admin/projects/${formData.id}`);
      } catch (error) {
        handleErrors.handleResponse(error);
      }
      appCtx.loader.hide();
    }
  };

  const uploadPhoto = async (projectId: string) => {
    if (!editPhoto) {
      return Promise.reject("skip");
    }
    return await uploadProjectFile(projectId, editPhoto);
  };

  const insert = async (variables: InsertProjectMutationVariables) => {
    if (editPhoto) {
      variables.photo = editPhoto.slugified;
    }
    const response = await client.query<
      InsertProjectMutation,
      InsertProjectMutationVariables
    >({
      query: InsertProjectDocument,
      variables,
    });
    if (response.errors) {
      return Promise.reject(response);
    }
    const newObj = response.data.insert_app_project_one!;

    try {
      await uploadPhoto(newObj.id);
      setEditPhoto(undefined);
    } catch (uploadError) {
      if (uploadError !== "skip") {
        try {
          await client.query<
            UpdateProjectByPkMutation,
            UpdateProjectByPkMutationVariables
          >({
            query: UpdateProjectByPkDocument,
            variables: {
              id: newObj.id,
              _set: {
                photo: "",
              },
            },
          });
        } catch (error) {}

        handleErrors.handleResponse(uploadError, "Erreur envoi photo");
      }
    }

    return newObj;
  };

  const update = async (variables: UpdateFullProjectByPkMutationVariables) => {
    if (!variables._set) {
      return Promise.reject("This error should not append !");
    }
    try {
      await uploadPhoto(variables.id);
      variables._set.photo = editPhoto?.slugified;
      setEditPhoto(undefined);
    } catch (uploadError) {
      if (uploadError !== "skip") {
        handleErrors.handleResponse(uploadError, "Erreur envoi photo");
      }
    }

    const response = await fetch(`/api/projects/${variables.id}`, {
      method: "PATCH",
      body: JSON.stringify(variables),
    });
    const json = await response.json();

    if (json.errors) {
      return Promise.reject(json);
    }

    return json.data.update_app_project_by_pk!;
  };

  const deleteFile = async (fileToDelete: TProjectFile) => {
    appCtx.loader.show();
    await fetch(`/api/project_files/${fileToDelete.id}`, {
      method: "DELETE",
    });
    setFiles(
      files.filter((file) => {
        return file.id != fileToDelete.id;
      })
    );
    if (curFile?.id == fileToDelete.id) {
      setCurFile(undefined);
    }
    appCtx.loader.hide();
  };

  const deleteOneProject = async () => {
    const formData = formApi.getValues();
    appCtx.loader.show();
    try {
      await softDeleteOneProject({
        variables: {
          id: formData.id,
          _set: {
            deleted_at: "now()",
          },
        },
      });
      router.push("/admin");
    } catch (error: any) {
      console.log(error);
    }
    appCtx.loader.hide();
  };

  const formData: TProject = formApi.getValues();
  const photoUrl = getProjectImgURL(
    formData,
    IMAGOR_PRESET_NAME["300x225"],
    formData.photo
  );

  return (
    <Stack spacing={2} pb={10}>
      <form onSubmit={formApi.handleSubmit(onSubmit, onError)} noValidate>
        <Stepper orientation="vertical">
          <Step active={true}>
            <StepLabel>Champs publics</StepLabel>
            <StepContent>
              <Grid container spacing={2} sx={{ p: 1.5 }}>
                <Grid item xs={3}>
                  <AppTextField
                    formApi={formApi}
                    name={"year"}
                    textFieldProps={{
                      label: "Année",
                      type: "number",
                      required: true,
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <AppSelect
                    formApi={formApi}
                    name={"project_themes"}
                    required
                    selectProps={{
                      label: "Thématiques",
                      multiple: true,
                      renderValue: (value: any) => {
                        return value
                          .map((id: string) => {
                            return dataset?.app_theme.find(
                              (option: any) => option.id === id
                            )?.label;
                          })
                          .join(", ");
                      },
                    }}
                    renderChildren={(value: any) => {
                      return dataset?.app_theme.map((opt: any) => (
                        <MenuItem key={opt.id} value={opt.id}>
                          <Checkbox checked={value.includes(opt.id)} />
                          {opt.label}
                        </MenuItem>
                      ));
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <AppSelect
                    formApi={formApi}
                    name={"project_types"}
                    required
                    selectProps={{
                      label: "Type(s) de l'action/projet",
                      multiple: true,
                      renderValue: (value: any) => {
                        return value
                          .map((id: string) => {
                            return dataset?.app_type.find(
                              (option: any) => option.id === id
                            )?.label;
                          })
                          .join(", ");
                      },
                    }}
                    renderChildren={(value) => {
                      return dataset?.app_type.map((opt: any) => (
                        <MenuItem key={opt.id} value={opt.id}>
                          <Checkbox checked={value.includes(opt.id)} />
                          {opt.label}
                        </MenuItem>
                      ));
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <AppSelect
                    formApi={formApi}
                    name={"project_territories"}
                    required
                    selectProps={{
                      label: "Territoires",
                      multiple: true,
                      renderValue: (value: any) => {
                        return value
                          .map((id: string) => {
                            return dataset?.app_territory.find(
                              (option: any) => option.id === id
                            )?.label;
                          })
                          .join(", ");
                      },
                    }}
                    renderChildren={(value) => {
                      return dataset?.app_territory.map((opt: any) => (
                        <MenuItem key={opt.id} value={opt.id}>
                          <Checkbox checked={value.includes(opt.id)} />
                          {opt.label}
                        </MenuItem>
                      ));
                    }}
                  />
                </Grid>
              </Grid>

              <Grid container spacing={2} sx={{ p: 1.5 }}>
                <Grid item xs={3}>
                  <AppTextField
                    formApi={formApi}
                    name={"partner"}
                    textFieldProps={{
                      label: "Acteurs/partenaires impliqués",
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <AppTextField
                    formApi={formApi}
                    name={"contact_pnrfo"}
                    textFieldProps={{
                      label: "Personne contact au PNRFO",
                      required: true,
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <AppTextField
                    formApi={formApi}
                    name={"budget"}
                    textFieldProps={{
                      label: "Budget",
                      type: "number",
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <AppSelect
                    formApi={formApi}
                    name={"project_financings"}
                    selectProps={{
                      label: "Financement(s)",
                      multiple: true,
                      renderValue: (value: any) => {
                        return value
                          .map((id: string) => {
                            return dataset?.app_financing.find(
                              (option: any) => option.id === id
                            )?.label;
                          })
                          .join(", ");
                      },
                    }}
                    renderChildren={(value) => {
                      return dataset?.app_financing.map((opt: any) => (
                        <MenuItem key={opt.id} value={opt.id}>
                          <Checkbox checked={value.includes(opt.id)} />
                          {opt.label}
                        </MenuItem>
                      ));
                    }}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} sx={{ p: 1.5 }}>
                <Grid item xs={6}>
                  <AppTextField
                    formApi={formApi}
                    name={"label"}
                    textFieldProps={{
                      label: "Titre court",
                      required: true,
                      InputProps: {
                        startAdornment: (
                          <InputAdornment position="start">
                            <Tooltip
                              title="le titre court est utilisé dans la liste des projets et la popup projet."
                              placement="top-start"
                              arrow
                            >
                              <QuestionMark />
                            </Tooltip>
                          </InputAdornment>
                        ),
                      },
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <AppTextField
                    formApi={formApi}
                    name={"title"}
                    textFieldProps={{
                      label: "Titre long",
                      required: true,
                      InputProps: {
                        startAdornment: (
                          <InputAdornment position="start">
                            <Tooltip
                              title="le titre long est utilisé en titre de la page projet."
                              placement="top-start"
                              arrow
                            >
                              <QuestionMark />
                            </Tooltip>
                          </InputAdornment>
                        ),
                      },
                    }}
                  />
                </Grid>
              </Grid>
              <Box sx={{ p: { sm: 1.5, xs: 1 } }}>
                <Typography>Texte de la description*</Typography>
                <MDEditor name="public_description" formApi={formApi} />
              </Box>
            </StepContent>
          </Step>
          <Step active={true}>
            <StepLabel>Champs privés</StepLabel>
            <StepContent>
              <Stack>
                <Stack sx={isSM ? styles.stack.sm : styles.stack.xs}>
                  <AppSelect
                    formApi={formApi}
                    name={"step"}
                    selectProps={{
                      label: "État",
                    }}
                  >
                    {PROJECT_STEPS.map((item) => (
                      <MenuItem key={item} value={item}>
                        {t(`project.step.${item}`)}
                      </MenuItem>
                    ))}
                  </AppSelect>
                  <AppTextField
                    formApi={formApi}
                    name={"owner"}
                    textFieldProps={{
                      label:
                        "Propriétaire connu et/ou différent du porteur de projet",
                    }}
                  />
                </Stack>
                <Box sx={{ p: { sm: 1.5, xs: 1 } }}>
                  <Typography>Description privée</Typography>
                  <MDEditor name="private_description" formApi={formApi} />
                </Box>
              </Stack>
            </StepContent>
          </Step>
          <Step active={true}>
            <StepLabel>Photo *</StepLabel>
            <StepContent>
              {photoError && (
                <Box className="form-item-error">Une photo est requise</Box>
              )}
              <Stack direction={"row"} mt={2} spacing={1}>
                <Box
                  style={{
                    width: 300,
                    height: 225,
                    background: "#CCC",
                  }}
                >
                  {Boolean(photoUrl) && (
                    <img
                      src={photoUrl}
                      style={{
                        width: "100%",
                        height: "100%",
                        objectFit: "cover",
                      }}
                    />
                  )}
                </Box>
                <AppInputFile
                  files={editPhoto ? [editPhoto] : undefined}
                  accept={{
                    "image/*": [],
                  }}
                  stackProps={{ py: 1 }}
                  onChange={(files) => {
                    setEditPhoto(files?.[0]);
                  }}
                />
              </Stack>
            </StepContent>
          </Step>
          <Step active={true}>
            <StepLabel>Fichiers joints</StepLabel>
            <StepContent></StepContent>
          </Step>
        </Stepper>
        <Stack
          direction="row"
          justifyContent="space-between"
          sx={{
            background: "transparent",
            position: "fixed",
            zIndex: 2,
            left: 0,
            bottom: 16,
            width: "100%",
            px: 2,
          }}
        >
          <CancelBtn
            label="Retour à la liste"
            onClick={() =>
              router.push(
                `/admin${router.query.goback ? `?${router.query.goback}` : ""}`
              )
            }
          />
          {formData?.id && (
            <DeleteBtn onClick={() => setDeleteProject(!deleteProject)} />
          )}
          <Stack direction={"row"} spacing={2}>
            <Box sx={{ background: "white", minWidth: 150 }}>
              <AppSelect
                formApi={formApi}
                name={"status"}
                selectProps={{
                  label: "Publication",
                  variant: "filled",
                }}
                formControlProps={{
                  variant: "filled",
                }}
              >
                {PROJECT_STATUSES.map((item) => (
                  <MenuItem key={item} value={item}>
                    {t(`project.status.${item}`)}
                  </MenuItem>
                ))}
              </AppSelect>
            </Box>
            <SubmitBtn disabled={formApi.formState.isSubmitting} />
          </Stack>
        </Stack>
      </form>
      {formData?.id && (
        <Stack direction={"row"} spacing={2}>
          <Stack sx={{ flex: 1 }}>
            <Stack direction={"row"} justifyContent="center" mb={3}>
              <Button
                variant="contained"
                onClick={() => {
                  setCurFile({ project_id: formData?.id });
                }}
              >
                Ajouter un document
              </Button>
            </Stack>
            <TableContainer component={Paper}>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Nom</TableCell>
                    <TableCell>Label</TableCell>
                    <TableCell>URL</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {files?.map((file) => (
                    <TableRow key={file.id}>
                      <TableCell>
                        <FileIcon
                          src={
                            getProjectImgURL(
                              formData,
                              IMAGOR_PRESET_NAME["50x50"],
                              file.preview
                            ) ||
                            getProjectImgURL(
                              formData,
                              IMAGOR_PRESET_NAME["50x50"],
                              file.filename
                            )
                          }
                        />
                      </TableCell>
                      <TableCell>{file.filename}</TableCell>
                      <TableCell>{file.label}</TableCell>
                      <TableCell>{file.url}</TableCell>
                      <TableCell>
                        <IconButton
                          onClick={() => {
                            setCurFile({ ...file });
                          }}
                        >
                          <Edit />
                        </IconButton>
                        <IconButton
                          color="error"
                          onClick={() => setFileToDelete({ ...file })}
                        >
                          <Delete />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Stack>
          {curFile && (
            <Stack
              sx={{
                flex: {
                  xs: 1,
                  lg: 0.7,
                  xl: 0.4,
                },
                paddingTop: "60px",
              }}
            >
              <Paper
                sx={{
                  flex: {
                    xs: 1,
                  },
                }}
              >
                <Stack p={2}>
                  <ProjectFileForm
                    data={curFile}
                    onCancel={() => setCurFile(undefined)}
                    onClose={() => setCurFile(undefined)}
                    onComplete={(data) => {
                      const file = files?.find((file) => {
                        return file.id == data.id;
                      });
                      !file ? files.push(data) : Object.assign(file, data);
                      setFiles([...files]);
                      //setCurFile(undefined);
                    }}
                  />
                </Stack>
              </Paper>
            </Stack>
          )}
          <Dialog open={Boolean(fileToDelete)}>
            <DialogTitle>Supprimer le fichier</DialogTitle>
            <DialogContent>
              Voulez-vous vraiment supprimer ce fichier ?
            </DialogContent>
            <DialogActions>
              <Button autoFocus onClick={() => setFileToDelete(undefined)}>
                Non
              </Button>
              <Button
                color="error"
                onClick={() => {
                  deleteFile(fileToDelete!);
                  setFileToDelete(undefined);
                }}
              >
                Oui
              </Button>
            </DialogActions>
          </Dialog>
          <Dialog open={Boolean(deleteProject)}>
            <DialogTitle>Supprimer ce projet</DialogTitle>
            <DialogContent>
              Voulez-vous vraiment supprimer ce projet ?
            </DialogContent>
            <DialogActions>
              <Button autoFocus onClick={() => setDeleteProject(false)}>
                Non
              </Button>
              <Button
                color="error"
                onClick={async () => {
                  await deleteOneProject();
                }}
              >
                Oui
              </Button>
            </DialogActions>
          </Dialog>
        </Stack>
      )}
      <ErrorDialog
        isOpen={showErrorDialog}
        handleClose={() => setShowErrorDialog(false)}
        message="Une erreur est présente sur le formulaire."
      />
      <MessageDialog
        isOpen={showSuccessDialog}
        handleClose={() => setshowSuccessDialog(false)}
        title="Succès"
        message="Le projet est bien enregistré."
      />
    </Stack>
  );
};

const styles = {
  stack: {
    sm: {
      flexDirection: "row",
      flexWrap: "nowrap",
      p: 1.5,
      gap: "1em",
    },
    xs: {
      flexDirection: "column",
      flexWrap: "wrap",
      p: 1,
      gap: "1em",
    },
  },
};
