import { useApolloClient } from "@apollo/client";
import { Delete } from "@mui/icons-material";
import {
  Box,
  Button,
  IconButton,
  Stack,
  Step,
  StepContent,
  StepLabel,
  Stepper,
  Typography,
} from "@mui/material";
import { pick } from "lodash";
import React, { FC, ReactNode, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import {
  InsertProjectFileDocument,
  InsertProjectFileMutation,
  InsertProjectFileMutationVariables,
  UpdateProjectFileByPkMutation,
} from "../../generated/graphql";
import { useAppContext } from "../../lib/AppContext";
import { IMAGOR_PRESET_NAME } from "../../lib/constants";
import useHandleErrors from "../../lib/useHandleErrors";
import {
  getProjectImgURL,
  getProjectUploadURL,
  uploadFile,
} from "../../lib/utils";
import { TFile, TProjectFile } from "../../types/app-types";
import { ConfirmDialog, ConfirmDialogProps } from "../dialog/ConfirmDialog";
import { AppInputFile } from "../form-input/AppInputFile";
import { AppTextField } from "../form-input/AppTextField";

const EditFileComponent: FC<{
  label: string;
  file?: TFile | null;
  preset?: string;
  onChange: (file?: TFile | null) => void;
}> = (props) => {
  return (
    <Stack>
      <Typography>{props.label}</Typography>
      <Stack direction={"row"} spacing={1}>
        <Box
          sx={{
            width: 100,
            height: 100,
            background: "#CCC",
          }}
        >
          {Boolean(props.preset) && props.file !== null && (
            <>
              <IconButton
                sx={{ position: "absolute" }}
                size="small"
                color="error"
                onClick={() => props.onChange(null)}
              >
                <Delete />
              </IconButton>
              <img
                src={props.preset}
                style={{
                  width: "100%",
                  height: "100%",
                  objectFit: "cover",
                }}
              />
            </>
          )}
        </Box>
        <Box sx={{ flex: 1 }}>
          <AppInputFile
            files={props.file ? [props.file] : undefined}
            onChange={(files) => {
              props.onChange(files?.[0]);
            }}
          />
        </Box>
      </Stack>
    </Stack>
  );
};

interface IProps {
  data: TProjectFile;
  onComplete?: (data: TProjectFile) => void;
  onCancel?: () => void;
  onClose?: () => void;
}

export const ProjectFileForm: FC<IProps> = ({
  data,
  onComplete,
  onCancel,
  onClose,
}) => {
  const formApi = useForm();
  const client = useApolloClient();
  const appCtx = useAppContext();

  const [editFile, setEditFile] = useState<TFile | null>();
  const [editPreview, setEditPreview] = useState<TFile | null>();
  const [confirmDialogProps, setConfirmDialogProps] =
    useState<ConfirmDialogProps | null>(null);

  const { handleResponse } = useHandleErrors();

  useEffect(() => {
    window.scrollTo(0, document.body.scrollHeight);
  }, [editFile, editPreview]);

  useEffect(() => {
    formApi.reset(data);
    setEditFile(undefined);
    setEditPreview(undefined);
  }, [data]);

  const confirmRemove = async (message: ReactNode[]) => {
    return new Promise<void>((resolve, reject) => {
      setConfirmDialogProps({
        onConfirm: resolve,
        onCancel: reject,
        message,
      });
    });
  };

  const onSubmit = async () => {
    const formData = formApi.getValues();
    const newData: TProjectFile = pick(formData, ["label", "url"]);
    newData.project_id = data.project_id;
    const isInsert = !formData.id;

    if (!isInsert) {
      const confirms: {
        message: string;
        field: "filename" | "preview";
        setter: typeof setEditPreview;
      }[] = [];
      if (editFile === null && data.filename) {
        confirms.push({
          message: "Voulez-vous vraiment supprimer le document ?",
          field: "filename",
          setter: setEditFile,
        });
      }
      if (editPreview === null && data.preview) {
        confirms.push({
          message: "Voulez-vous vraiment supprimer l'aperçu ?",
          field: "preview",
          setter: setEditPreview,
        });
      }

      if (confirms.length) {
        try {
          await confirmRemove(
            confirms.map((confirm) => {
              return (
                <Typography key={confirm.field}>{confirm.message}</Typography>
              );
            })
          );
          setConfirmDialogProps(null);
          confirms.forEach((confirm) => {
            newData[confirm.field] = null;
          });
        } catch (error) {
          confirms.forEach((confirm) => {
            confirm.setter(undefined);
          });
          return setConfirmDialogProps(null);
        }
      }
    }

    appCtx.loader.show();
    try {
      let uploadFileUrl = "";
      if (editFile) {
        try {
          uploadFileUrl = await getProjectUploadURL(data.project_id, editFile);
        } catch (error) {
          appCtx.loader.hide();
          return handleResponse(error, "Erreur document");
        }
      }
      let uploadPreviewUrl = "";
      if (editPreview) {
        try {
          uploadPreviewUrl = await getProjectUploadURL(
            data.project_id,
            editPreview
          );
        } catch (error) {
          appCtx.loader.hide();
          return handleResponse(error, "Erreur aperçu");
        }
      }

      if (editFile && uploadFileUrl) {
        newData.filename = editFile.slugified;
      }
      if (editPreview && uploadPreviewUrl) {
        newData.preview = editPreview.slugified;
      }

      const response = isInsert
        ? await insert(newData)
        : await update(formData.id, newData);

      if (response.errors) {
        appCtx.loader.hide();
        return handleResponse(response);
      }

      const resultData: TProjectFile = isInsert
        ? (response.data as InsertProjectFileMutation)
            .insert_app_project_file_one!
        : (response.data as UpdateProjectFileByPkMutation)
            .update_app_project_file_by_pk!;

      if (editFile === null) {
        resultData.filename = null;
        setEditFile(undefined);
      } else if (editFile && uploadFileUrl) {
        try {
          await uploadFile(editFile, uploadFileUrl);
          setEditFile(undefined);
        } catch (uploadError: any) {
          formApi.reset(resultData);
          onComplete?.(resultData);
          appCtx.loader.hide();
          return handleResponse(uploadError, "Erreur document");
        }
      }

      if (editPreview === null) {
        resultData.preview = null;
        setEditPreview(undefined);
      } else if (editPreview && uploadPreviewUrl) {
        try {
          await uploadFile(editPreview, uploadPreviewUrl);
          setEditPreview(undefined);
        } catch (uploadError: any) {
          formApi.reset(resultData);
          onComplete?.(resultData);
          appCtx.loader.hide();
          return handleResponse(uploadError, "Erreur aperçu");
        }
      }

      formApi.reset(resultData);
      onComplete?.(resultData);
      onClose?.();
    } catch (error) {
      console.log(error);
    }
    appCtx.loader.hide();
  };

  const insert = async (variables: InsertProjectFileMutationVariables) => {
    return await client.query<
      InsertProjectFileMutation,
      InsertProjectFileMutationVariables
    >({
      query: InsertProjectFileDocument,
      variables,
    });
  };

  const update = async (id: string, data: TProjectFile) => {
    const response = await fetch(`/api/project_files/${id}`, {
      method: "PATCH",
      body: JSON.stringify(data),
    });
    return await response.json();
  };

  return (
    <form onSubmit={formApi.handleSubmit(onSubmit)}>
      <Stack spacing={2}>
        <Stepper orientation="vertical">
          <Step active={true}>
            <StepLabel>Choisir un document</StepLabel>
            <StepContent>
              <Stack spacing={2}>
                <EditFileComponent
                  label="Fichier"
                  file={editFile}
                  preset={getProjectImgURL(
                    data.project_id,
                    IMAGOR_PRESET_NAME["200x200"],
                    data.filename
                  )}
                  onChange={setEditFile}
                />
                <AppTextField
                  formApi={formApi}
                  name={"url"}
                  textFieldProps={{
                    label: "OU un lien vers un fichier",
                  }}
                />
              </Stack>
            </StepContent>
          </Step>
          <Step active={true}>
            <StepLabel>Ajouter des infos</StepLabel>
            <StepContent>
              <Stack spacing={2}>
                <EditFileComponent
                  label="Associer un aperçu au document"
                  file={editPreview}
                  preset={getProjectImgURL(
                    data.project_id,
                    IMAGOR_PRESET_NAME["200x200"],
                    data.preview
                  )}
                  onChange={setEditPreview}
                />
                <AppTextField
                  formApi={formApi}
                  name={"label"}
                  textFieldProps={{
                    label: "Titre du document",
                  }}
                />
              </Stack>
            </StepContent>
          </Step>
        </Stepper>
        <Stack
          direction={"row"}
          sx={{
            justifyContent: "space-between",
            mt: 2,
          }}
        >
          {Boolean(onCancel) && <Button onClick={onCancel}>Annuler</Button>}
          <Button color="primary" variant="contained" type="submit">
            OK
          </Button>
        </Stack>
      </Stack>
      {confirmDialogProps && (
        <ConfirmDialog {...confirmDialogProps} isOpen={true} />
      )}
    </form>
  );
};
