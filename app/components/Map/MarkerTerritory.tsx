import { BoxProps } from "@mui/system";
import L, { divIcon } from "leaflet";
import React, { FC, useMemo } from "react";
import { Marker } from "react-leaflet";
import { TTerritorySelection } from "../../types/app-types";
import { renderToStaticMarkup } from "react-dom/server";
import center from "@turf/center";

interface IProps {
  feature: GeoJSON.Feature<GeoJSON.Geometry, TTerritorySelection>;
  onClick: () => void;
  sxProps?: BoxProps;
}

const SIZE = 32;
const ARROW_H = 12;

export const MarkerTerritory: FC<IProps> = ({ feature, onClick }) => {
  const data = feature.properties;
  const icon = useMemo(() => {
    return divIcon({
      className: data.isHighlighted ? "selected" : "",
      iconSize: new L.Point(SIZE, SIZE),
      iconAnchor: new L.Point(SIZE / 2, SIZE + ARROW_H),
      html: renderToStaticMarkup(
        <>
          <span className="marker-content">
            <span>
              {data.filtered_projects_aggregate?.aggregate?.count || 0}
            </span>
          </span>
          <span className="marker-arrow"></span>
        </>
      ),
    });
  }, [data.isHighlighted, data.filtered_projects_aggregate]);

  const centered =
    feature.geometry.type == "Point"
      ? (feature as any)
      : center(feature.geometry as GeoJSON.MultiPolygon);

  return (
    <Marker
      icon={icon}
      eventHandlers={{
        click: onClick,
      }}
      position={[
        centered.geometry.coordinates[1],
        centered.geometry.coordinates[0],
      ]}
    />
  );
};

export default MarkerTerritory;
