// @ts-nocheck
import L from "leaflet";
import { createControlComponent } from "@react-leaflet/core";
import "leaflet.fullscreen";
import "leaflet.fullscreen/Control.FullScreen.css";

const FullscreenControl = createControlComponent((props) => {
  return L.control.fullscreen({
    ...props,
    forceSeparateButton: true,
  });
});

export default FullscreenControl;
