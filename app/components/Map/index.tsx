import { FC, useEffect, useRef, useState } from "react";
import {
  LayersControl,
  MapContainer,
  TileLayer,
  useMap,
  GeoJSON as GeoJSONLayer,
} from "react-leaflet";
import { TTerritorySelection } from "../../types/app-types";
import bbox from "@turf/bbox";
import center from "@turf/center";
import booleanEqual from "@turf/boolean-equal";
import { multiPoint, Feature, MultiPoint } from "@turf/helpers";
import { FitBoundsOptions, LatLngBoundsExpression, LatLngTuple } from "leaflet";
import { useTheme } from "@mui/material";
import FullscreenControl from "./controls/Fullscreen";

const { BaseLayer } = LayersControl;

export interface MapContentProps {
  geojson: GeoJSON.FeatureCollection<GeoJSON.Geometry, TTerritorySelection>;
  selection: GeoJSON.FeatureCollection<GeoJSON.Geometry, TTerritorySelection>;
  children?: JSX.Element | JSX.Element[];
}

const MapContent: FC<MapContentProps> = ({ children, geojson, selection }) => {
  const map = useMap();
  const bounds = bbox(selection.features.length === 0 ? geojson : selection);
  const prevBounds = useRef<Feature<MultiPoint>>();

  useEffect(() => {
    if (map) {
      const mapBounds: LatLngBoundsExpression = [
        [bounds[1], bounds[0]],
        [bounds[3], bounds[2]],
      ];
      const newBounds = multiPoint(mapBounds);

      const optsFly: FitBoundsOptions = {
        duration: 0.4,
        maxZoom: 13,
      };

      if (!map.getZoom()) {
        map.fitBounds(mapBounds);
      } else {
        const canFly = !prevBounds.current
          ? true
          : !booleanEqual(prevBounds.current, newBounds);
        canFly && map.flyToBounds(mapBounds, optsFly);
      }
      prevBounds.current = newBounds;
    }
  }, [geojson.features, map]);

  return (
    <>
      <LayersControl>
        <BaseLayer checked name="OSM">
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
        </BaseLayer>
        <BaseLayer name="IGN-F/Géoportail">
          <TileLayer
            attribution='&copy; <a href="https://www.ign.fr/">IGN</a> contributors'
            url="https://data.geopf.fr/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIXSET=PM&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=image/jpeg&STYLE=normal"
          />
        </BaseLayer>
      </LayersControl>
      {children}
    </>
  );
};

export interface MapProps {
  geojson: GeoJSON.FeatureCollection<GeoJSON.Geometry, TTerritorySelection>;
  children?: JSX.Element | JSX.Element[];
}

const Map: FC<MapProps> = ({ children, geojson }) => {
  const [pnrBounds, setPnrBounds] = useState<GeoJSON.FeatureCollection<
    GeoJSON.Geometry,
    GeoJSON.GeoJsonProperties
  > | null>(null);

  useEffect(() => {
    (async () => {
      const resp = await fetch("/assets/pnr-bounds.geojson");
      setPnrBounds(await resp.json());
    })();
  }, []);

  const theme = useTheme();

  const selected = geojson.features.filter(
    (feature) => feature.properties.isSelected
  );
  const geojsonSelected = { ...geojson, features: selected };
  const centerSelected = center(geojson as any);
  const pathes = {
    ...geojson,
    features: geojson.features.filter((feat) => feat.geometry.type != "Point"),
  };

  return (
    <MapContainer
      center={centerSelected.geometry.coordinates as LatLngTuple}
      style={{ width: "100%", height: "100%" }}
    >
      <GeoJSONLayer
        data={pathes}
        pathOptions={{
          fillColor: "transparent",
          color: "#000",
          weight: 2,
          dashArray: "5 10",
        }}
      />
      {pnrBounds && (
        <GeoJSONLayer
          data={pnrBounds}
          pathOptions={{
            weight: 3,
            color: theme.palette.primary.main,
            fillColor: "#transparent",
          }}
        />
      )}
      <MapContent geojson={geojson} selection={geojsonSelected}>
        {children}
      </MapContent>
      <FullscreenControl />
    </MapContainer>
  );
};

export default Map;
