import { FilterList } from "@mui/icons-material";
import { IconButton, Menu, MenuItem, TextField } from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { MRT_Column } from "material-react-table";
import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

interface IProps {
  column: MRT_Column;
  header: any;
  table: any;
}

export const GridFilterDate: FC<IProps> = (props) => {
  const column = props.column;

  const operators = [
    "equals",
    "notEquals",
    "between",
    "betweenInclusive",
    "greaterThan",
    "greaterThanOrEqualTo",
    "lessThan",
    "lessThanOrEqualTo",
    "empty",
    "notEmpty",
  ];
  const _filterFn: string = column?.columnDef?._filterFn;

  const [operator, setOperator] = useState<string>(
    operators.includes(_filterFn) ? _filterFn : operators[0]
  );
  const [value, setValue] = useState<any>(column.getFilterValue() || null);
  const [opsAnchorEl, setOpsAnchorEl] = useState<any>(null);
  const isOpsOpen = Boolean(opsAnchorEl);
  const onOpClick = (newOp: string) => {
    setOperator(newOp);
    setValue(null);
    closeOps();
    //apply({ newOp });
  };
  const closeOps = () => {
    setOpsAnchorEl(null);
  };
  /* const onValueChange = (newVal: any) => {
            setValue(newVal);
            apply({ newVal });
          }; */

  const { t } = useTranslation();

  useEffect(() => {
    column.columnDef._filterFn = operator;
    column.setFilterValue(
      value || ["empty", "noempty"].includes(operator) ? value : undefined
    );
  }, [value]);

  /* const apply = (params: { newOp?: string; newVal?: any }) => {
            column.setFilterValue({
              operator: params.newOp || operator,
              value: params.newVal || value,
            });
          }; */

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <IconButton
        onClick={(e) => {
          setOpsAnchorEl(e.currentTarget);
        }}
      >
        <FilterList />
      </IconButton>
      <Menu anchorEl={opsAnchorEl} open={isOpsOpen} onClose={closeOps}>
        {operators.map((opName) => {
          return (
            <MenuItem
              key={opName}
              onClick={() => {
                onOpClick(opName);
              }}
            >
              {t(`grid.filters.operators.${opName}`)}
            </MenuItem>
          );
        })}
      </Menu>
      <DatePicker
        onChange={(newVal) => setValue(newVal)}
        renderInput={(params) => (
          <TextField
            {...params}
            helperText={`Filter Mode: ${t(
              `grid.filters.operators.${operator}`
            )}`}
            sx={{ minWidth: "120px" }}
            variant="standard"
          />
        )}
        value={value}
      />
    </LocalizationProvider>
  );
};
