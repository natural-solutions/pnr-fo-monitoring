import { Box } from "@mui/material";
import { FC } from "react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";

interface LayoutAppProps {
  children: JSX.Element;
}

const LayoutApp: FC<LayoutAppProps> = ({ children }) => {
  return <Box height={"100%"}>{children}</Box>;
};

export default LayoutApp;
