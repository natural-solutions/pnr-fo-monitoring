import { AppBar, Button, Stack, Toolbar, Typography } from "@mui/material";
import { useSession } from "next-auth/react";
import Head from "next/head";
import Link from "next/link";
import { FC, useEffect, useState } from "react";
import { SessionBtn } from "../SessionBtn";
import { LoginPaper } from "../LoginPaper";

interface LayoutAdminProps {
  children: JSX.Element | JSX.Element[];
}

const LayoutAdmin: FC<LayoutAdminProps> = ({ children }) => {
  const session = useSession();

  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  return (
    <>
      <Head>
        <title>Admin PNR FO</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Stack height={"100%"} className="app-admin">
        <AppBar position="fixed" className="header-admin">
          <Toolbar variant="dense">
            <Stack
              direction={"row"}
              sx={{ flexGrow: 1, alignItems: "center" }}
              spacing={3}
            >
              <Typography variant="h6" component="div">
                Admin
              </Typography>
              <Link href="/" passHref>
                <Button
                  component="a"
                  color="inherit"
                  variant="outlined"
                  size="small"
                >
                  Portail grand public
                </Button>
              </Link>
            </Stack>
            <SessionBtn />
          </Toolbar>
        </AppBar>
        {session.status === "authenticated" && !Boolean(session.data.error) ? (
          children
        ) : (
          <>
            {!isLoading && (
              <Stack
                sx={{
                  height: "100%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <LoginPaper />
              </Stack>
            )}
          </>
        )}
      </Stack>
    </>
  );
};

export default LayoutAdmin;
