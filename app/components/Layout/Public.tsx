import {
  ArrowBackIosNew,
  ArrowCircleRightOutlined,
  Close,
  Email,
  Info,
  LocationOn,
  Logout,
  Menu,
  PhoneInTalk,
} from "@mui/icons-material";
import {
  AppBar,
  Box,
  Button,
  Drawer,
  Fab,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Stack,
  Tab,
  Tabs,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";
import { signOut, useSession } from "next-auth/react";
import Link from "next/link";
import { FC, useState } from "react";
import { SessionBtn } from "../SessionBtn";
import { useAppContext } from "../../lib/AppContext";

const Btns: FC = () => {
  const appContext = useAppContext();

  return (
    <>
      <Stack direction={"row"} spacing={0}>
        <Tooltip title="En savoir +">
          <IconButton
            color="primary"
            onClick={() => appContext.onBoarding.show()}
          >
            <Info />
          </IconButton>
        </Tooltip>
        <Tooltip title="Téléphone: 03 25 43 38 88">
          <IconButton color="primary" component="a" href="tel:03 25 43 38 88">
            <PhoneInTalk />
          </IconButton>
        </Tooltip>
        <Tooltip title="Nous contacter">
          <IconButton
            color="primary"
            component="a"
            target="_blank"
            href="https://www.pnr-foret-orient.fr/pratique/nous-contacter/"
          >
            <Email />
          </IconButton>
        </Tooltip>
      </Stack>
      <Tooltip title="Carte interactive">
        <Fab
          size="small"
          color="primary"
          sx={{
            width: 36,
            height: 36,
            display: {
              xs: "none",
              sm: "inline-flex",
            },
          }}
          component="a"
          target="_blank"
          href="https://www.pnr-foret-orient.fr/pratique/carte-interactive/"
        >
          <LocationOn />
        </Fab>
      </Tooltip>
    </>
  );
};

interface LayoutPublicProps {
  title?: string;
  backUrl?: string;
  children: JSX.Element | JSX.Element[];
}

const LayoutPublic: FC<LayoutPublicProps> = ({ title, backUrl, children }) => {
  const session = useSession();
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  const toggleDrawer = () => {
    setIsDrawerOpen((prevState) => !prevState);
  };

  const drawerListItemStyle = {
    fontSize: 24,
    fontWeight: "bold",
    color: "primary",
  };

  return (
    <Stack height={"100%"} className="app-public">
      <AppBar
        position="fixed"
        className="header-public"
        color="default"
        sx={{
          display: {
            md: "none",
          },
        }}
      >
        <Toolbar
          variant="dense"
          sx={{
            paddingRight: {
              xs: 0,
            },
            justifyContent: "space-between",
          }}
        >
          <Stack
            direction={"row"}
            sx={{
              alignItems: "center",
            }}
          >
            <Link href={backUrl || ""} passHref>
              <IconButton
                color="inherit"
                edge="start"
                style={{
                  display: Boolean(backUrl) ? "inline-flex" : "none",
                }}
              >
                <ArrowBackIosNew />
              </IconButton>
            </Link>
            {Boolean(title) && (
              <Typography
                component={"h1"}
                variant="h6"
                sx={{
                  textTransform: "uppercase",
                  fontSize: {
                    xs: "1rem",
                    sm: "1.25rem",
                  },
                }}
              >
                {title}
              </Typography>
            )}
          </Stack>
          <Stack
            direction={"row"}
            spacing={{
              xs: 0,
              sm: 1,
            }}
            sx={{ alignItems: "center" }}
          >
            <Btns />
            <IconButton
              color="inherit"
              edge="end"
              size="large"
              onClick={() => {
                setIsDrawerOpen(true);
              }}
            >
              <Menu fontSize="inherit" />
            </IconButton>
          </Stack>
        </Toolbar>
      </AppBar>
      <AppBar
        position="fixed"
        className="header-public"
        color="default"
        sx={{
          visibility: {
            xs: "hidden",
            md: "visible",
          },
          height: {
            xs: 0,
            md: "auto",
          },
        }}
      >
        <Toolbar
          variant="dense"
          sx={{
            paddingRight: "0px",
            paddingBlockEnd: "0px",
            justifyContent: "space-between",
          }}
        >
          <Box sx={{ height: "40px", alignSelf: "start", overflow: "visible" }}>
            <img
              src="/assets/logo.png"
              alt="PNR Forêt d'Orient"
              style={{
                width: 135,
                display: "block",
              }}
            />
          </Box>
          <Tabs
            value={0}
            centered
            sx={{
              position: {
                lg: "absolute",
              },
              width: {
                lg: "100%",
              },
            }}
          >
            <Tab component="a" href="/" label="Carte des projets" />
            <Tab
              component="a"
              href="https://www.pnr-foret-orient.fr/nos-actions"
              target="_blank"
              label="Nos actions"
            />
          </Tabs>
          <Stack direction={"row"} spacing={1}>
            <Btns />
            {session.status === "authenticated" &&
              !Boolean(session.data.error) && (
                <Link href="/admin" passHref>
                  <Button
                    variant="contained"
                    component="a"
                    sx={{
                      marginLeft: "26px !important",
                    }}
                  >
                    Admin
                  </Button>
                </Link>
              )}
            <SessionBtn />
          </Stack>
        </Toolbar>
      </AppBar>
      <Drawer
        anchor="right"
        variant="persistent"
        open={isDrawerOpen}
        onClose={toggleDrawer}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: "block", md: "none" },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: "100%",
            background: "#ECD9EB",
          },
        }}
      >
        <Stack sx={{ height: "100%", alignItems: "center" }}>
          <Stack
            direction={"row"}
            sx={{
              justifyContent: "space-between",
              alignItems: "start",
              pl: 3,
              width: "100%",
            }}
          >
            <img
              src="/assets/logo.png"
              alt="PNR Forêt d'Orient"
              style={{
                width: 135,
                display: "block",
              }}
            />
            <Stack direction={"row"} spacing={2} sx={{ alignItems: "center" }}>
              <Btns />
              <IconButton
                color="inherit"
                edge="end"
                size="large"
                onClick={() => {
                  setIsDrawerOpen(false);
                }}
              >
                <Close fontSize="inherit" />
              </IconButton>
            </Stack>
          </Stack>
          <List
            sx={{
              mt: 10,
              width: "100%",
              maxWidth: 350,
            }}
          >
            <ListItem
              disablePadding
              sx={{
                alignItems: "revert",
              }}
            >
              <Link href="/" passHref>
                <ListItemButton component="a">
                  <ListItemText
                    primary="Carte des projets"
                    primaryTypographyProps={drawerListItemStyle}
                  />
                  <ArrowCircleRightOutlined fontSize="large" color="primary" />
                </ListItemButton>
              </Link>
            </ListItem>
            <ListItem disablePadding>
              <ListItemButton
                component="a"
                href="https://www.pnr-foret-orient.fr/nos-actions"
                target="_blank"
              >
                <ListItemText
                  primary="Nos actions"
                  primaryTypographyProps={drawerListItemStyle}
                />
                <ArrowCircleRightOutlined fontSize="large" color="primary" />
              </ListItemButton>
            </ListItem>
          </List>
          {/* <Stack
            direction={"row-reverse"}
            sx={{ justifyContent: "flex-end", pl: 2 }}
          >
            <Btns />
          </Stack> */}
          {session.status === "authenticated" &&
            !Boolean(session.data.error) && (
              <Box sx={{ mt: 4, px: 2, pb: 2 }}>
                <Link href="/admin" passHref>
                  <Button variant="contained" component="a">
                    Admin
                  </Button>
                </Link>
                <Stack spacing={1} sx={{ mt: 3 }}>
                  <Typography>{session.data.user.name}</Typography>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={() => signOut()}
                    endIcon={<Logout />}
                  >
                    sign out
                  </Button>
                </Stack>
              </Box>
            )}
        </Stack>
      </Drawer>
      {children}
    </Stack>
  );
};

export default LayoutPublic;
