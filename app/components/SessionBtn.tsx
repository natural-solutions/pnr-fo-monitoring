import { AccountCircle, Logout } from "@mui/icons-material";
import {
  Button,
  ClickAwayListener,
  IconButton,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import { signIn, signOut, useSession } from "next-auth/react";
import React, { FC, useState } from "react";

export const SessionBtn: FC = () => {
  const [isTooltipOpen, setIsTooltipOpen] = useState(false);

  const session = useSession();
  const handleSignIn = () => {
    signIn("keycloak");
  };
  const handleSignOut = () => {
    signOut();
  };
  return (
    <>
      {session.status !== "authenticated" || Boolean(session.data.error) ? (
        <></>
      ) : (
        <ClickAwayListener onClickAway={() => setIsTooltipOpen(false)}>
          <div>
            <Tooltip
              onClose={() => setIsTooltipOpen(false)}
              open={isTooltipOpen}
              arrow
              placement="bottom-end"
              PopperProps={{
                className: "tooltip-light",
                disablePortal: true,
              }}
              disableFocusListener
              disableHoverListener
              disableTouchListener
              title={
                <Stack spacing={1} sx={{ p: 1 }}>
                  <Typography>{session.data.user.name}</Typography>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={handleSignOut}
                    endIcon={<Logout />}
                  >
                    sign out
                  </Button>
                </Stack>
              }
            >
              <IconButton
                size="medium"
                style={{ marginRight: "-12px" }}
                color="inherit"
                onClick={() => setIsTooltipOpen(true)}
              >
                <AccountCircle color="inherit" />
              </IconButton>
            </Tooltip>
          </div>
        </ClickAwayListener>
      )}
    </>
  );
};
