import { Typography } from "@mui/material";
import { Stack, StackProps } from "@mui/system";
import React, { FC, ImgHTMLAttributes, useEffect, useState } from "react";

interface IProps {
  src?: string;
  fallbackProps?: StackProps;
  imgProps?: ImgHTMLAttributes<HTMLImageElement>;
}

export const FileIcon: FC<IProps> = ({ src, fallbackProps, imgProps }) => {
  const [fallback, setFallback] = useState("");

  const onError = (e: any) => {
    console.log("img err", e);
    const ext = src?.split(".").pop();
    setFallback(ext || " ");
  };

  useEffect(() => {
    const ext = src?.split(".").pop();
    setFallback(src ? "" : ext || " ");
  }, [src]);

  return (
    <>
      {Boolean(fallback) ? (
        <Stack
          width={50}
          height={50}
          style={{
            background: "#CCC",
            ...fallbackProps?.style,
          }}
          alignItems="center"
          justifyContent="center"
          {...fallbackProps}
        >
          <Typography>{fallback}</Typography>
        </Stack>
      ) : (
        <img
          src={src}
          style={{ display: "block", ...imgProps?.style }}
          {...imgProps}
          onError={onError}
        />
      )}
    </>
  );
};
