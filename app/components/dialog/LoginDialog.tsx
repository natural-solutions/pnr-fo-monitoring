import { Button, Dialog, Stack, Typography } from "@mui/material";
import { signIn } from "next-auth/react";

export default function LoginDialog(props: {
  onClose: () => void;
  open: boolean;
}) {
  const { onClose, open } = props;

  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog onClose={handleClose} disableEscapeKeyDown open={open}>
      <Stack p={2} spacing={2}>
        <Typography>Veuillez vous connecter</Typography>
        <Stack direction={"row"}>
          <Button onClick={handleClose}>Plus tard</Button>
          <Button variant="contained" onClick={() => signIn("keycloak")}>
            Connexion
          </Button>
        </Stack>
      </Stack>
    </Dialog>
  );
}
