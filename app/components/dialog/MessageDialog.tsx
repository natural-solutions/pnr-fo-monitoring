import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { FC } from "react";

export type MessageDialogProps = {
  isOpen: boolean;
  title: string;
  message: string | React.ReactNode | React.ReactNode[];
  handleClose?: () => void;
};

const MessageDialog: FC<MessageDialogProps> = (props) => {
  return (
    <Dialog
      onClose={props.handleClose}
      aria-labelledby="simple-dialog-title"
      open={props.isOpen}
    >
      <DialogTitle id="simple-dialog-title">{props.title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{props.message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleClose}>OK</Button>
      </DialogActions>
    </Dialog>
  );
};

export default MessageDialog;
