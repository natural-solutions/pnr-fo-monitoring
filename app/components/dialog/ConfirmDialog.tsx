import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import React, { FC, ReactNode } from "react";

export interface ConfirmDialogProps {
  isOpen?: boolean;
  onConfirm?: () => void;
  onCancel?: () => void;
  title?: string;
  message?: string | ReactNode | ReactNode[];
}

export const ConfirmDialog: FC<ConfirmDialogProps> = (props) => {
  const onConfirm = () => {
    if (props.onConfirm) {
      return props.onConfirm();
    }
  };

  const onCancel = () => {
    if (props.onCancel) {
      return props.onCancel();
    }
  };

  return (
    <Dialog open={Boolean(props.isOpen)}>
      <DialogTitle>{props.title || "Action irréversible"}</DialogTitle>
      <DialogContent>
        {props.message || "Voulez-vous vraiment supprimer cet object ?"}
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onCancel}>
          Non
        </Button>
        <Button color="error" onClick={onConfirm}>
          Oui
        </Button>
      </DialogActions>
    </Dialog>
  );
};
