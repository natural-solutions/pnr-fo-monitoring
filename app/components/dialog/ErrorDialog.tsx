import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { FC } from "react";

export type ErrorDialogProps = {
  isOpen: boolean;
  title?: string;
  message: string | React.ReactNode | React.ReactNode[];
  handleClose?: () => void;
};

const ErrorDialog: FC<ErrorDialogProps> = (props) => {
  return (
    <Dialog
      onClose={props.handleClose}
      aria-labelledby="simple-dialog-title"
      open={props.isOpen}
    >
      <DialogTitle id="simple-dialog-title">
        {props.title || "Erreur de saisie"}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>{props.message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleClose}>OK</Button>
      </DialogActions>
    </Dialog>
  );
};

export default ErrorDialog;
