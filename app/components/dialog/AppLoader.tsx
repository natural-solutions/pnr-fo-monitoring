import { Backdrop, CircularProgress } from "@mui/material";

export default function AppLoader(props: { isOpen: boolean }) {
  const { isOpen } = props;

  return (
    <Backdrop
      open={isOpen}
      sx={{
        zIndex: 1101,
      }}
    >
      <CircularProgress color="primary" />
    </Backdrop>
  );
}
