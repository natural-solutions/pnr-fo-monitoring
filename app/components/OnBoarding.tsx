import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  FormControlLabel,
  Typography,
} from "@mui/material";
import Link from "next/link";
import React, { FC, useEffect, useState } from "react";

interface IProps {
  open: boolean;
  onClose: () => void;
}

/**
 * @author
 * @function @OnBoarding
 **/

export const OnBoarding: FC<IProps> = ({ open, onClose }) => {
  const [isOnBoardingAutoOpen, setIsOnBoardingAutoOpen] = useState(false);

  useEffect(() => {
    if (["1", null].includes(localStorage.getItem("isOnBoardingAutoOpen"))) {
      setIsOnBoardingAutoOpen(true);
    }
  }, []);

  return (
    <Dialog open={open} onClose={onClose} fullWidth={true} maxWidth="md">
      <DialogContent>
        <DialogContentText
          variant="subtitle1"
          sx={{
            lineHeight: 1.3,
            fontSize: {
              xs: "1.2rem",
              sm: "1.4rem",
            },
          }}
        >
          LE PARC NATUREL RÉGIONAL DE LA FORÊT D'ORIENT
        </DialogContentText>
        <DialogContentText
          variant="subtitle1"
          sx={{
            lineHeight: 1.3,
            textAlign: "right",
            fontSize: {
              xs: "1.2rem",
              sm: "1.4rem",
            },
          }}
        >
          TERRITOIRE D'EXCEPTION,
          <br />
          TERRE DE PROJETS !
        </DialogContentText>
        <DialogContentText sx={{ mt: 2 }}>
          Le Parc, c’est un service d’expertise, d’animation, de pédagogie,
          d’échanges et de médiation qui agit pour la protection, la
          valorisation des patrimoines et le développement du territoire.
        </DialogContentText>
        <DialogContentText
          variant="subtitle2"
          sx={{
            mt: 2,
            lineHeight: 1.3,
            fontSize: "1.2rem",
            textAlign: "center",
          }}
          color="primary"
        >
          « Terre de projets », c’est un outil qui vous donne accès à tous les
          projets et actions du Pnr de la Forêt d’Orient et de ses partenaires
          sur l’ensemble des communes du territoire.
        </DialogContentText>
        <DialogContentText sx={{ mt: 2 }}>
          Chaque projet possède une fiche descriptive avec : <br />
          <Typography
            color="primary"
            component="span"
            sx={{ mt: 1.5, mr: 1.5 }}
          >
            ●
          </Typography>
          L’année de mise en œuvre du projet
          <br />
          <Typography color="primary" component="span" sx={{ mr: 1.5 }}>
            ●
          </Typography>
          Le budget alloué <br />
          <Typography color="primary" component="span" sx={{ mr: 1.5 }}>
            ●
          </Typography>
          La source de financement <br />
          <Typography color="primary" component="span" sx={{ mr: 1.5 }}>
            ●
          </Typography>
          Le type de projet ou d’action <br />
          <Typography color="primary" component="span" sx={{ mr: 1.5 }}>
            ●
          </Typography>
          La ou les thématiques concernées par le projet <br />
          <Typography color="primary" component="span" sx={{ mr: 1.5 }}>
            ●
          </Typography>
          Les acteurs ou partenaires impliqués <br />
          <Typography color="primary" component="span" sx={{ mr: 1.5 }}>
            ●
          </Typography>
          La ou les communes concernées <br />
          <Typography color="primary" component="span" sx={{ mr: 1.5 }}>
            ●
          </Typography>
          La description du projet <br />
          <Typography color="primary" component="span" sx={{ mr: 1.5 }}>
            ●
          </Typography>
          Des informations complémentaires (compte-rendu, rapport, délibération,
          article de presse...)
        </DialogContentText>
        <DialogContentText sx={{ mt: 2, fontStyle: "italic" }}>
          Vous avez un projet ? Vous souhaitez être accompagnés ?{" "}
          <Link
            href="https://www.pnr-foret-orient.fr/pratique/nous-contacter/"
            target="_blank"
          >
            Contactez-nous !
          </Link>
        </DialogContentText>
        <Box
          sx={{
            width: "100%",
            mt: 2,
            display: {
              xs: "none",
              sm: "none",
              md: "block",
            },
          }}
        >
          <a href="/assets/tuto.jpg" target="_blank">
            <img
              src="/assets/tuto.jpg"
              alt="Tutoriel"
              style={{ width: "100%" }}
            />
          </a>
        </Box>
      </DialogContent>
      <DialogActions sx={{ justifyContent: "space-between" }}>
        <FormControlLabel
          label="Afficher à chaque ouverture"
          control={
            <Checkbox
              checked={isOnBoardingAutoOpen}
              onChange={(_, checked) => {
                setIsOnBoardingAutoOpen(checked);
                localStorage.setItem(
                  "isOnBoardingAutoOpen",
                  checked ? "1" : "0"
                );
              }}
            />
          }
        />
        <Button variant="contained" onClick={onClose} autoFocus>
          Fermer
        </Button>
      </DialogActions>
    </Dialog>
  );
};
