import { Button, Paper, Stack, Typography } from "@mui/material";
import { signIn } from "next-auth/react";
import React, { FC } from "react";

type LoginPaperProps = {
  callbackUrl?: string;
};

export const LoginPaper: FC<LoginPaperProps> = ({ callbackUrl }) => {
  return (
    <Paper sx={{ p: 2 }}>
      <Stack spacing={2}>
        <Typography>Veuillez vous connecter</Typography>
        <Button
          variant="contained"
          onClick={() =>
            signIn("keycloak", {
              callbackUrl,
            })
          }
        >
          Connexion
        </Button>
      </Stack>
    </Paper>
  );
};
