import { Box, Button, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React, { FC } from "react";
import { IMAGOR_PRESET_NAME } from "../lib/constants";
import { getProjectImgURL } from "../lib/utils";
import { TProject } from "../types/app-types";

interface IProps {
  project: TProject;
  link?: string;
}

export const ProjectTooltipContent: FC<IProps> = ({ project, link }) => {
  const router = useRouter();
  const photo = getProjectImgURL(
    project,
    IMAGOR_PRESET_NAME["300x225"],
    project.photo
  );
  return (
    <Box sx={{ width: 240, color: "#333" }}>
      {Boolean(photo) && (
        <Box sx={{ height: 180, background: "#EEE" }}>
          <img
            style={{ width: "100%", height: "auto" }}
            src={photo}
            alt={`photo ${project.label}`}
          />
        </Box>
      )}
      <Typography
        variant="h5"
        className="custom-title"
        sx={{ textTransform: "uppercase" }}
      >
        {project.label}
      </Typography>
      <Box>
        <Button
          component="a"
          href={`/projects/${project.id}`}
          onClick={(e) => {
            e.preventDefault();
            router.push(link || e.currentTarget.href);
          }}
        >
          En savoir plus
        </Button>
      </Box>
    </Box>
  );
};
