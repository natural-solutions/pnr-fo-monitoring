import { Clear } from "@mui/icons-material";
import { IconButton, InputAdornment } from "@mui/material";
import React, { FC } from "react";

interface IProps {
  onClick: () => void;
}

export const ClearInputBtn: FC<IProps> = (props) => {
  return (
    <InputAdornment position="end">
      <IconButton onClick={() => props.onClick()}>
        <Clear />
      </IconButton>
    </InputAdornment>
  );
};
