import {
  Box,
  IconButton,
  Stack,
  StackProps,
  TextFieldProps,
  Typography,
} from "@mui/material";
import React, { FC } from "react";
import { useDropzone } from "react-dropzone";
import { slugify } from "../../lib/utils";
import { Close } from "@mui/icons-material";
import { TFile } from "../../types/app-types";

interface IProps {
  value?: any;
  onChange?: (files?: TFile[]) => void;
  textFieldProps?: TextFieldProps;
  stackProps?: StackProps;
  files?: TFile[];
  accept?: any;
}

export const AppInputFile: FC<IProps> = (props) => {
  const { getRootProps, getInputProps } = useDropzone({
    multiple: false,
    accept: props.accept,
    onDrop: (acceptedFiles) => {
      props.onChange?.(
        acceptedFiles.map((file) => {
          return Object.assign(file, {
            preview: URL.createObjectURL(file),
            slugified: slugify(file.name),
          });
        })
      );
    },
  });

  const removeFile = (indexToremove: number) => {
    const newFiles = (props.files || []).filter((file, i) => {
      return i != indexToremove;
    });
    props.onChange?.(newFiles);
  };

  return (
    <Stack direction={"column"}>
      <Stack direction={"column"} spacing={1}>
        <Box
          {...getRootProps()}
          sx={{
            background: "#1565C0",
            borderRadius: "0.5em",
            cursor: "pointer",
            color: "#fff",
          }}
          p={2}
          textAlign="center"
        >
          <input {...getInputProps()} />
          <Typography>
            Glissez-déposez votre fichiers ici, ou cliquez pour le sélectionner.
          </Typography>
        </Box>
        <Stack direction={"column"}>
          {(props.files || []).map((file, index) => {
            return (
              <Stack
                key={index.toString()}
                direction="row"
                alignItems={"center"}
                spacing={1}
              >
                <Box
                  style={{
                    width: 80,
                    height: 80,
                    background: "#CCC",
                  }}
                >
                  {Boolean(file.preview) && (
                    <img
                      src={file.preview}
                      style={{
                        width: "100%",
                        height: "100%",
                        objectFit: "contain",
                      }}
                      onLoad={() => {
                        URL.revokeObjectURL(file.preview);
                      }}
                    />
                  )}
                </Box>
                <Typography>{file.slugified}</Typography>
                <IconButton onClick={() => removeFile(index)}>
                  <Close />
                </IconButton>
              </Stack>
            );
          })}
        </Stack>
      </Stack>
    </Stack>
  );
};
