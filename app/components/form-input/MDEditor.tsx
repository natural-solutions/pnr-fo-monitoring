import "@uiw/react-md-editor/markdown-editor.css";
import "@uiw/react-markdown-preview/markdown.css";
import {
  bold,
  italic,
  link,
  divider,
  quote,
  unorderedListCommand,
  orderedListCommand,
  checkedListCommand,
  title3,
} from "@uiw/react-md-editor/lib/commands";
import dynamic from "next/dynamic";
import { FC } from "react";
import { Controller } from "react-hook-form";
import { FormControl } from "@mui/material";
import { FormInputError } from "./FormInputError";

const MDEditor = dynamic(() => import("@uiw/react-md-editor"), { ssr: false });

type MDEditorProps = {
  name: string;
  formApi: any;
};

export const MDEditorComponent: FC<MDEditorProps> = (props) => {
  const control = props.formApi.control;
  return (
    <Controller
      control={control}
      name={props.name}
      render={({ field: { onChange, value } }) => {
        return (
          <FormControl variant="outlined" style={{ width: "100%" }}>
            {MDEditor && (
              <MDEditor
                value={value || ""}
                onChange={(newValue) => {
                  onChange(newValue);
                }}
                textareaProps={{
                  placeholder: "Veuillez écrire ici",
                }}
                commands={[
                  bold,
                  italic,
                  title3,
                  link,
                  divider,
                  quote,
                  unorderedListCommand,
                  orderedListCommand,
                  checkedListCommand,
                ]}
              />
            )}
            <FormInputError name={props.name} formApi={props.formApi} />
          </FormControl>
        );
      }}
    />
  );
};
