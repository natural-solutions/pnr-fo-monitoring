import { Button } from "@mui/material";
import React, { FC } from "react";

export const SubmitBtn: FC<{ disabled?: boolean }> = (props) => {
  return (
    <Button
      variant="contained"
      sx={styles.button}
      type="submit"
      disabled={props.disabled}
    >
      Enregistrer
    </Button>
  );
};

const styles = {
  button: {
    width: { sm: 250, md: 300 },
  },
};
