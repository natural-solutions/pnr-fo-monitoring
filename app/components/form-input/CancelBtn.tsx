import { Button } from "@mui/material";
import React, { FC } from "react";

interface CancelBtnProps {
  label?: string;
  onClick: () => void;
}

export const CancelBtn: FC<CancelBtnProps> = ({ label, onClick }) => {
  return (
    <Button
      variant="contained"
      onClick={onClick}
      color="neutral"
      sx={styles.button}
    >
      {label || "Annuler"}
    </Button>
  );
};

const styles = {
  button: {
    width: { sm: 250, md: 300 },
  },
};
