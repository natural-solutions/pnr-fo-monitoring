import { CheckBox, CheckBoxOutlineBlank } from "@mui/icons-material";
import { Autocomplete, Checkbox, TextField } from "@mui/material";
import React, { FC, ReactNode } from "react";

interface IProps {
  label?: string;
  valueKey?: string;
  labelKey?: string;
  value?: any[];
  options?: any[];
  getOptionLabel?: (option: any) => string;
  renderOptionLabel?: (option: any) => ReactNode;
  onChange: (value: any[]) => void;
}

export const SimpleAutocomplete: FC<IProps> = (props) => {
  const valueKey = props.valueKey || "id";
  const labelKey = props.labelKey || "label";

  const value = (props.value || [])
    .map((item: any) => {
      if (typeof item == "object") {
        return item;
      }
      return (props.options || []).find(
        (option) => String(option[valueKey]) == String(item)
      );
    })
    .filter((item) => item);

  return (
    <Autocomplete
      fullWidth
      multiple
      options={props.options || []}
      limitTags={1}
      disableCloseOnSelect
      //getOptionLabel={(option) => option.title}
      value={value}
      getOptionLabel={(option) =>
        props.getOptionLabel?.(option) || String(option[labelKey])
      }
      renderOption={(optionProps, option, { selected }) => (
        <li {...optionProps}>
          <Checkbox
            icon={<CheckBoxOutlineBlank fontSize="small" />}
            checkedIcon={<CheckBox fontSize="small" />}
            style={{ marginRight: 8 }}
            checked={selected}
          />
          {props.renderOptionLabel?.(option) || String(option[labelKey])}
        </li>
      )}
      renderInput={(params) => <TextField {...params} label={props.label} />}
      onChange={(e, value) => {
        props.onChange(value);
      }}
    />
  );
};
