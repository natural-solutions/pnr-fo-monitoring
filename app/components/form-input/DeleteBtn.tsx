import { Button } from "@mui/material";
import React, { FC } from "react";

interface DeleteBtnProps {
  onClick: () => void;
}

export const DeleteBtn: FC<DeleteBtnProps> = ({ onClick }) => {
  return (
    <Button
      variant="contained"
      color="warning"
      sx={styles.button}
      onClick={onClick}
    >
      Supprimer
    </Button>
  );
};

const styles = {
  button: {
    width: { sm: 250, md: 300 },
  },
};
