import { Clear } from "@mui/icons-material";
import {
  FormControl,
  FormControlProps,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Select,
  SelectProps,
  Stack,
} from "@mui/material";
import { isPlainObject } from "lodash";
import { FC } from "react";
import { Controller } from "react-hook-form";
import { FormInputError } from "./FormInputError";

type AppSelectProps = {
  name: string;
  formApi: any;
  hasClearButton?: boolean;
  formControlProps?: FormControlProps;
  selectProps?: SelectProps;
  renderChildren?: (value: any) => React.ReactNode[];
  children?: React.ReactNode;
  valueKey?: string;
  required?: boolean;
};

export const AppSelect: FC<AppSelectProps> = (props) => {
  const control = props.formApi.control;
  const selectProps: SelectProps = props.selectProps || {};
  selectProps.type = selectProps.type || "text";
  selectProps.variant = selectProps.variant || "outlined";
  selectProps.fullWidth = selectProps.fullWidth === false ? false : true;
  selectProps.inputProps = selectProps.inputProps || {};
  const required = props.required === true ? true : false;
  const valueKey = props.valueKey || "id";

  let hasClearButton = props.hasClearButton;

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  return (
    <Stack direction="column" width="100%">
      <Controller
        control={control}
        name={props.name}
        render={({ field: { onChange, value } }) => {
          const InputProps: any = {};
          if (hasClearButton) {
            InputProps.endAdornment = (
              <InputAdornment position="end">
                <IconButton onClick={() => onChange(null)}>
                  <Clear />
                </IconButton>
              </InputAdornment>
            );
          }
          if (typeof value === "undefined" || value === null) {
            value = "";
          }
          if (isPlainObject(value)) {
            value = value[valueKey];
          }
          if (selectProps.multiple && !Array.isArray(value)) {
            value = [];
          }

          return (
            <FormControl
              variant="outlined"
              style={{ width: "100%" }}
              required={required}
              {...props.formControlProps}
            >
              <InputLabel id="select-label">{selectProps.label}</InputLabel>
              <Select
                labelId="select-label"
                variant="outlined"
                value={value}
                onChange={(e) => {
                  onChange(e.target.value);
                }}
                {...selectProps}
                name={props.name}
                MenuProps={MenuProps}
              >
                {props.renderChildren?.(value) || props.children}
              </Select>
            </FormControl>
          );
        }}
      />
      <FormInputError name={props.name} formApi={props.formApi} />
    </Stack>
  );
};
