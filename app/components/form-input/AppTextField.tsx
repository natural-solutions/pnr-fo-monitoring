import { Stack, TextField, TextFieldProps } from "@mui/material";
import { FC } from "react";
import { Controller, UseFormReturn } from "react-hook-form";
import { ClearInputBtn } from "./ClearInputBtn";
import { FormInputError } from "./FormInputError";

type AppTextFieldProps = {
  name: string;
  formApi: UseFormReturn;
  hasClearButton?: boolean;
  textFieldProps?: TextFieldProps;
};

export const AppTextField: FC<AppTextFieldProps> = (props) => {
  const control = props.formApi.control;
  const textFieldProps: TextFieldProps = props.textFieldProps || {};
  textFieldProps.type = textFieldProps.type || "text";
  textFieldProps.required = textFieldProps.required === true ? true : false;
  textFieldProps.variant = textFieldProps.variant || "outlined";
  textFieldProps.fullWidth = textFieldProps.fullWidth === false ? false : true;
  textFieldProps.inputProps = textFieldProps.inputProps || {};
  textFieldProps.InputLabelProps = textFieldProps.InputLabelProps || {};
  //textFieldProps.InputLabelProps.shrink = true;
  let hasClearButton = props.hasClearButton;
  if (textFieldProps.type === "number") {
    textFieldProps.inputProps.step = "any";
    if (hasClearButton !== false && !textFieldProps.disabled) {
      hasClearButton = true;
    }
  }

  return (
    <Stack direction="column" width="100%">
      <Controller
        control={control}
        name={props.name}
        render={({ field: { onChange, value } }) => {
          if (value == null) {
            value = "";
          }
          const InputProps: any = textFieldProps.InputProps || {};
          if (hasClearButton) {
            InputProps.endAdornment = (
              <ClearInputBtn onClick={() => onChange(null)} />
            );
          }
          return (
            <TextField
              value={value}
              name={props.name}
              {...textFieldProps}
              InputProps={InputProps}
              onChange={(e) => {
                const value = e.target.value;
                if (textFieldProps.type === "number") {
                  if (typeof value === "string" && !value) {
                    onChange(null);
                  }
                  onChange(Number(value));
                } else {
                  onChange(value);
                }
              }}
            />
          );
        }}
      ></Controller>
      <FormInputError name={props.name} formApi={props.formApi} />
    </Stack>
  );
};
