import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  bigint: any;
  geography: any;
  geometry: any;
  numeric: number;
  timestamptz: string;
  uuid: any;
};

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['Boolean']>;
  _gt?: InputMaybe<Scalars['Boolean']>;
  _gte?: InputMaybe<Scalars['Boolean']>;
  _in?: InputMaybe<Array<Scalars['Boolean']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['Boolean']>;
  _lte?: InputMaybe<Scalars['Boolean']>;
  _neq?: InputMaybe<Scalars['Boolean']>;
  _nin?: InputMaybe<Array<Scalars['Boolean']>>;
};

/** Boolean expression to compare columns of type "Int". All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['Int']>;
  _gt?: InputMaybe<Scalars['Int']>;
  _gte?: InputMaybe<Scalars['Int']>;
  _in?: InputMaybe<Array<Scalars['Int']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['Int']>;
  _lte?: InputMaybe<Scalars['Int']>;
  _neq?: InputMaybe<Scalars['Int']>;
  _nin?: InputMaybe<Array<Scalars['Int']>>;
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['String']>;
  _gt?: InputMaybe<Scalars['String']>;
  _gte?: InputMaybe<Scalars['String']>;
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars['String']>;
  _in?: InputMaybe<Array<Scalars['String']>>;
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars['String']>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars['String']>;
  _lt?: InputMaybe<Scalars['String']>;
  _lte?: InputMaybe<Scalars['String']>;
  _neq?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars['String']>;
  _nin?: InputMaybe<Array<Scalars['String']>>;
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars['String']>;
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars['String']>;
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars['String']>;
};

/** columns and relationships of "app.financing" */
export type App_Financing = {
  __typename?: 'app_financing';
  id: Scalars['String'];
  label: Scalars['String'];
  /** An array relationship */
  project_financings: Array<App_Project_Financing>;
  /** An aggregate relationship */
  project_financings_aggregate: App_Project_Financing_Aggregate;
};


/** columns and relationships of "app.financing" */
export type App_FinancingProject_FinancingsArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Financing_Order_By>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};


/** columns and relationships of "app.financing" */
export type App_FinancingProject_Financings_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Financing_Order_By>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};

/** aggregated selection of "app.financing" */
export type App_Financing_Aggregate = {
  __typename?: 'app_financing_aggregate';
  aggregate?: Maybe<App_Financing_Aggregate_Fields>;
  nodes: Array<App_Financing>;
};

/** aggregate fields of "app.financing" */
export type App_Financing_Aggregate_Fields = {
  __typename?: 'app_financing_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Financing_Max_Fields>;
  min?: Maybe<App_Financing_Min_Fields>;
};


/** aggregate fields of "app.financing" */
export type App_Financing_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Financing_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "app.financing". All fields are combined with a logical 'AND'. */
export type App_Financing_Bool_Exp = {
  _and?: InputMaybe<Array<App_Financing_Bool_Exp>>;
  _not?: InputMaybe<App_Financing_Bool_Exp>;
  _or?: InputMaybe<Array<App_Financing_Bool_Exp>>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  project_financings?: InputMaybe<App_Project_Financing_Bool_Exp>;
};

/** unique or primary key constraints on table "app.financing" */
export enum App_Financing_Constraint {
  /** unique or primary key constraint on columns "id" */
  FinancingPkey = 'financing_pkey'
}

/** input type for inserting data into table "app.financing" */
export type App_Financing_Insert_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
  project_financings?: InputMaybe<App_Project_Financing_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type App_Financing_Max_Fields = {
  __typename?: 'app_financing_max_fields';
  id?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type App_Financing_Min_Fields = {
  __typename?: 'app_financing_min_fields';
  id?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "app.financing" */
export type App_Financing_Mutation_Response = {
  __typename?: 'app_financing_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Financing>;
};

/** input type for inserting object relation for remote table "app.financing" */
export type App_Financing_Obj_Rel_Insert_Input = {
  data: App_Financing_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Financing_On_Conflict>;
};

/** on_conflict condition type for table "app.financing" */
export type App_Financing_On_Conflict = {
  constraint: App_Financing_Constraint;
  update_columns?: Array<App_Financing_Update_Column>;
  where?: InputMaybe<App_Financing_Bool_Exp>;
};

/** Ordering options when selecting data from "app.financing". */
export type App_Financing_Order_By = {
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  project_financings_aggregate?: InputMaybe<App_Project_Financing_Aggregate_Order_By>;
};

/** primary key columns input for table: app.financing */
export type App_Financing_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "app.financing" */
export enum App_Financing_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label'
}

/** input type for updating data in table "app.financing" */
export type App_Financing_Set_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "app_financing" */
export type App_Financing_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Financing_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Financing_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.financing" */
export enum App_Financing_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label'
}

export type App_Financing_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Financing_Set_Input>;
  where: App_Financing_Bool_Exp;
};

/** columns and relationships of "app.project" */
export type App_Project = {
  __typename?: 'app_project';
  budget?: Maybe<Scalars['numeric']>;
  contact_pnrfo?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  description_short?: Maybe<Scalars['String']>;
  header?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  label: Scalars['String'];
  owner?: Maybe<Scalars['String']>;
  partner?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
  private_description?: Maybe<Scalars['String']>;
  /** An array relationship */
  project_files: Array<App_Project_File>;
  /** An aggregate relationship */
  project_files_aggregate: App_Project_File_Aggregate;
  /** An array relationship */
  project_financings: Array<App_Project_Financing>;
  /** An aggregate relationship */
  project_financings_aggregate: App_Project_Financing_Aggregate;
  /** An array relationship */
  project_territories: Array<App_Project_Territory>;
  /** An aggregate relationship */
  project_territories_aggregate: App_Project_Territory_Aggregate;
  /** An array relationship */
  project_themes: Array<App_Project_Theme>;
  /** An aggregate relationship */
  project_themes_aggregate: App_Project_Theme_Aggregate;
  /** An array relationship */
  project_types: Array<App_Project_Type>;
  /** An aggregate relationship */
  project_types_aggregate: App_Project_Type_Aggregate;
  public_description?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  step?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  year?: Maybe<Scalars['Int']>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_FilesArgs = {
  distinct_on?: InputMaybe<Array<App_Project_File_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_File_Order_By>>;
  where?: InputMaybe<App_Project_File_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_Files_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_File_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_File_Order_By>>;
  where?: InputMaybe<App_Project_File_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_FinancingsArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Financing_Order_By>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_Financings_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Financing_Order_By>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_TerritoriesArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Territory_Order_By>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_Territories_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Territory_Order_By>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_ThemesArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Theme_Order_By>>;
  where?: InputMaybe<App_Project_Theme_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_Themes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Theme_Order_By>>;
  where?: InputMaybe<App_Project_Theme_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_TypesArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Type_Order_By>>;
  where?: InputMaybe<App_Project_Type_Bool_Exp>;
};


/** columns and relationships of "app.project" */
export type App_ProjectProject_Types_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Type_Order_By>>;
  where?: InputMaybe<App_Project_Type_Bool_Exp>;
};

/** aggregated selection of "app.project" */
export type App_Project_Aggregate = {
  __typename?: 'app_project_aggregate';
  aggregate?: Maybe<App_Project_Aggregate_Fields>;
  nodes: Array<App_Project>;
};

/** aggregate fields of "app.project" */
export type App_Project_Aggregate_Fields = {
  __typename?: 'app_project_aggregate_fields';
  avg?: Maybe<App_Project_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<App_Project_Max_Fields>;
  min?: Maybe<App_Project_Min_Fields>;
  stddev?: Maybe<App_Project_Stddev_Fields>;
  stddev_pop?: Maybe<App_Project_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<App_Project_Stddev_Samp_Fields>;
  sum?: Maybe<App_Project_Sum_Fields>;
  var_pop?: Maybe<App_Project_Var_Pop_Fields>;
  var_samp?: Maybe<App_Project_Var_Samp_Fields>;
  variance?: Maybe<App_Project_Variance_Fields>;
};


/** aggregate fields of "app.project" */
export type App_Project_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Project_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type App_Project_Avg_Fields = {
  __typename?: 'app_project_avg_fields';
  budget?: Maybe<Scalars['Float']>;
  year?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "app.project". All fields are combined with a logical 'AND'. */
export type App_Project_Bool_Exp = {
  _and?: InputMaybe<Array<App_Project_Bool_Exp>>;
  _not?: InputMaybe<App_Project_Bool_Exp>;
  _or?: InputMaybe<Array<App_Project_Bool_Exp>>;
  budget?: InputMaybe<Numeric_Comparison_Exp>;
  contact_pnrfo?: InputMaybe<String_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  deleted_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  description_short?: InputMaybe<String_Comparison_Exp>;
  header?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  owner?: InputMaybe<String_Comparison_Exp>;
  partner?: InputMaybe<String_Comparison_Exp>;
  photo?: InputMaybe<String_Comparison_Exp>;
  private_description?: InputMaybe<String_Comparison_Exp>;
  project_files?: InputMaybe<App_Project_File_Bool_Exp>;
  project_financings?: InputMaybe<App_Project_Financing_Bool_Exp>;
  project_territories?: InputMaybe<App_Project_Territory_Bool_Exp>;
  project_themes?: InputMaybe<App_Project_Theme_Bool_Exp>;
  project_types?: InputMaybe<App_Project_Type_Bool_Exp>;
  public_description?: InputMaybe<String_Comparison_Exp>;
  status?: InputMaybe<String_Comparison_Exp>;
  step?: InputMaybe<String_Comparison_Exp>;
  title?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  year?: InputMaybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.project" */
export enum App_Project_Constraint {
  /** unique or primary key constraint on columns "id" */
  ProjectPkey = 'project_pkey'
}

/** columns and relationships of "app.project_file" */
export type App_Project_File = {
  __typename?: 'app_project_file';
  created_at: Scalars['timestamptz'];
  deleted_at?: Maybe<Scalars['timestamptz']>;
  filename?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  label?: Maybe<Scalars['String']>;
  preview?: Maybe<Scalars['String']>;
  project_id: Scalars['uuid'];
  updated_at: Scalars['timestamptz'];
  url?: Maybe<Scalars['String']>;
};

/** aggregated selection of "app.project_file" */
export type App_Project_File_Aggregate = {
  __typename?: 'app_project_file_aggregate';
  aggregate?: Maybe<App_Project_File_Aggregate_Fields>;
  nodes: Array<App_Project_File>;
};

/** aggregate fields of "app.project_file" */
export type App_Project_File_Aggregate_Fields = {
  __typename?: 'app_project_file_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Project_File_Max_Fields>;
  min?: Maybe<App_Project_File_Min_Fields>;
};


/** aggregate fields of "app.project_file" */
export type App_Project_File_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Project_File_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "app.project_file" */
export type App_Project_File_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<App_Project_File_Max_Order_By>;
  min?: InputMaybe<App_Project_File_Min_Order_By>;
};

/** input type for inserting array relation for remote table "app.project_file" */
export type App_Project_File_Arr_Rel_Insert_Input = {
  data: Array<App_Project_File_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Project_File_On_Conflict>;
};

/** Boolean expression to filter rows from the table "app.project_file". All fields are combined with a logical 'AND'. */
export type App_Project_File_Bool_Exp = {
  _and?: InputMaybe<Array<App_Project_File_Bool_Exp>>;
  _not?: InputMaybe<App_Project_File_Bool_Exp>;
  _or?: InputMaybe<Array<App_Project_File_Bool_Exp>>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  deleted_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  filename?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  preview?: InputMaybe<String_Comparison_Exp>;
  project_id?: InputMaybe<Uuid_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  url?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.project_file" */
export enum App_Project_File_Constraint {
  /** unique or primary key constraint on columns "id" */
  ProjectFilePkey = 'project_file_pkey'
}

/** input type for inserting data into table "app.project_file" */
export type App_Project_File_Insert_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']>;
  deleted_at?: InputMaybe<Scalars['timestamptz']>;
  filename?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  label?: InputMaybe<Scalars['String']>;
  preview?: InputMaybe<Scalars['String']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
  url?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type App_Project_File_Max_Fields = {
  __typename?: 'app_project_file_max_fields';
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  filename?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  label?: Maybe<Scalars['String']>;
  preview?: Maybe<Scalars['String']>;
  project_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  url?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "app.project_file" */
export type App_Project_File_Max_Order_By = {
  created_at?: InputMaybe<Order_By>;
  deleted_at?: InputMaybe<Order_By>;
  filename?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  preview?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  url?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type App_Project_File_Min_Fields = {
  __typename?: 'app_project_file_min_fields';
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  filename?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  label?: Maybe<Scalars['String']>;
  preview?: Maybe<Scalars['String']>;
  project_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  url?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "app.project_file" */
export type App_Project_File_Min_Order_By = {
  created_at?: InputMaybe<Order_By>;
  deleted_at?: InputMaybe<Order_By>;
  filename?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  preview?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  url?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "app.project_file" */
export type App_Project_File_Mutation_Response = {
  __typename?: 'app_project_file_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Project_File>;
};

/** on_conflict condition type for table "app.project_file" */
export type App_Project_File_On_Conflict = {
  constraint: App_Project_File_Constraint;
  update_columns?: Array<App_Project_File_Update_Column>;
  where?: InputMaybe<App_Project_File_Bool_Exp>;
};

/** Ordering options when selecting data from "app.project_file". */
export type App_Project_File_Order_By = {
  created_at?: InputMaybe<Order_By>;
  deleted_at?: InputMaybe<Order_By>;
  filename?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  preview?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  url?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app.project_file */
export type App_Project_File_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "app.project_file" */
export enum App_Project_File_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  Filename = 'filename',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Preview = 'preview',
  /** column name */
  ProjectId = 'project_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Url = 'url'
}

/** input type for updating data in table "app.project_file" */
export type App_Project_File_Set_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']>;
  deleted_at?: InputMaybe<Scalars['timestamptz']>;
  filename?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  label?: InputMaybe<Scalars['String']>;
  preview?: InputMaybe<Scalars['String']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
  url?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "app_project_file" */
export type App_Project_File_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Project_File_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Project_File_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']>;
  deleted_at?: InputMaybe<Scalars['timestamptz']>;
  filename?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  label?: InputMaybe<Scalars['String']>;
  preview?: InputMaybe<Scalars['String']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
  url?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.project_file" */
export enum App_Project_File_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  Filename = 'filename',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Preview = 'preview',
  /** column name */
  ProjectId = 'project_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Url = 'url'
}

export type App_Project_File_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Project_File_Set_Input>;
  where: App_Project_File_Bool_Exp;
};

/** columns and relationships of "app.project_financing" */
export type App_Project_Financing = {
  __typename?: 'app_project_financing';
  /** An object relationship */
  financing: App_Financing;
  financing_id: Scalars['String'];
  id: Scalars['uuid'];
  /** An object relationship */
  project: App_Project;
  project_id: Scalars['uuid'];
};

/** aggregated selection of "app.project_financing" */
export type App_Project_Financing_Aggregate = {
  __typename?: 'app_project_financing_aggregate';
  aggregate?: Maybe<App_Project_Financing_Aggregate_Fields>;
  nodes: Array<App_Project_Financing>;
};

/** aggregate fields of "app.project_financing" */
export type App_Project_Financing_Aggregate_Fields = {
  __typename?: 'app_project_financing_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Project_Financing_Max_Fields>;
  min?: Maybe<App_Project_Financing_Min_Fields>;
};


/** aggregate fields of "app.project_financing" */
export type App_Project_Financing_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "app.project_financing" */
export type App_Project_Financing_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<App_Project_Financing_Max_Order_By>;
  min?: InputMaybe<App_Project_Financing_Min_Order_By>;
};

/** input type for inserting array relation for remote table "app.project_financing" */
export type App_Project_Financing_Arr_Rel_Insert_Input = {
  data: Array<App_Project_Financing_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Project_Financing_On_Conflict>;
};

/** Boolean expression to filter rows from the table "app.project_financing". All fields are combined with a logical 'AND'. */
export type App_Project_Financing_Bool_Exp = {
  _and?: InputMaybe<Array<App_Project_Financing_Bool_Exp>>;
  _not?: InputMaybe<App_Project_Financing_Bool_Exp>;
  _or?: InputMaybe<Array<App_Project_Financing_Bool_Exp>>;
  financing?: InputMaybe<App_Financing_Bool_Exp>;
  financing_id?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  project?: InputMaybe<App_Project_Bool_Exp>;
  project_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.project_financing" */
export enum App_Project_Financing_Constraint {
  /** unique or primary key constraint on columns "id" */
  ProjectFinancingPkey = 'project_financing_pkey'
}

/** input type for inserting data into table "app.project_financing" */
export type App_Project_Financing_Insert_Input = {
  financing?: InputMaybe<App_Financing_Obj_Rel_Insert_Input>;
  financing_id?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  project?: InputMaybe<App_Project_Obj_Rel_Insert_Input>;
  project_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type App_Project_Financing_Max_Fields = {
  __typename?: 'app_project_financing_max_fields';
  financing_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  project_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "app.project_financing" */
export type App_Project_Financing_Max_Order_By = {
  financing_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type App_Project_Financing_Min_Fields = {
  __typename?: 'app_project_financing_min_fields';
  financing_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  project_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "app.project_financing" */
export type App_Project_Financing_Min_Order_By = {
  financing_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "app.project_financing" */
export type App_Project_Financing_Mutation_Response = {
  __typename?: 'app_project_financing_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Project_Financing>;
};

/** on_conflict condition type for table "app.project_financing" */
export type App_Project_Financing_On_Conflict = {
  constraint: App_Project_Financing_Constraint;
  update_columns?: Array<App_Project_Financing_Update_Column>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};

/** Ordering options when selecting data from "app.project_financing". */
export type App_Project_Financing_Order_By = {
  financing?: InputMaybe<App_Financing_Order_By>;
  financing_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  project?: InputMaybe<App_Project_Order_By>;
  project_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app.project_financing */
export type App_Project_Financing_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "app.project_financing" */
export enum App_Project_Financing_Select_Column {
  /** column name */
  FinancingId = 'financing_id',
  /** column name */
  Id = 'id',
  /** column name */
  ProjectId = 'project_id'
}

/** input type for updating data in table "app.project_financing" */
export type App_Project_Financing_Set_Input = {
  financing_id?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  project_id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "app_project_financing" */
export type App_Project_Financing_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Project_Financing_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Project_Financing_Stream_Cursor_Value_Input = {
  financing_id?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  project_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "app.project_financing" */
export enum App_Project_Financing_Update_Column {
  /** column name */
  FinancingId = 'financing_id',
  /** column name */
  Id = 'id',
  /** column name */
  ProjectId = 'project_id'
}

export type App_Project_Financing_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Project_Financing_Set_Input>;
  where: App_Project_Financing_Bool_Exp;
};

/** input type for incrementing numeric columns in table "app.project" */
export type App_Project_Inc_Input = {
  budget?: InputMaybe<Scalars['numeric']>;
  year?: InputMaybe<Scalars['Int']>;
};

/** input type for inserting data into table "app.project" */
export type App_Project_Insert_Input = {
  budget?: InputMaybe<Scalars['numeric']>;
  contact_pnrfo?: InputMaybe<Scalars['String']>;
  created_at?: InputMaybe<Scalars['timestamptz']>;
  deleted_at?: InputMaybe<Scalars['timestamptz']>;
  description_short?: InputMaybe<Scalars['String']>;
  header?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  label?: InputMaybe<Scalars['String']>;
  owner?: InputMaybe<Scalars['String']>;
  partner?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
  private_description?: InputMaybe<Scalars['String']>;
  project_files?: InputMaybe<App_Project_File_Arr_Rel_Insert_Input>;
  project_financings?: InputMaybe<App_Project_Financing_Arr_Rel_Insert_Input>;
  project_territories?: InputMaybe<App_Project_Territory_Arr_Rel_Insert_Input>;
  project_themes?: InputMaybe<App_Project_Theme_Arr_Rel_Insert_Input>;
  project_types?: InputMaybe<App_Project_Type_Arr_Rel_Insert_Input>;
  public_description?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  step?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
  year?: InputMaybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type App_Project_Max_Fields = {
  __typename?: 'app_project_max_fields';
  budget?: Maybe<Scalars['numeric']>;
  contact_pnrfo?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  description_short?: Maybe<Scalars['String']>;
  header?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  label?: Maybe<Scalars['String']>;
  owner?: Maybe<Scalars['String']>;
  partner?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
  private_description?: Maybe<Scalars['String']>;
  public_description?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  step?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  year?: Maybe<Scalars['Int']>;
};

/** aggregate min on columns */
export type App_Project_Min_Fields = {
  __typename?: 'app_project_min_fields';
  budget?: Maybe<Scalars['numeric']>;
  contact_pnrfo?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  description_short?: Maybe<Scalars['String']>;
  header?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  label?: Maybe<Scalars['String']>;
  owner?: Maybe<Scalars['String']>;
  partner?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
  private_description?: Maybe<Scalars['String']>;
  public_description?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  step?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  year?: Maybe<Scalars['Int']>;
};

/** response of any mutation on the table "app.project" */
export type App_Project_Mutation_Response = {
  __typename?: 'app_project_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Project>;
};

/** input type for inserting object relation for remote table "app.project" */
export type App_Project_Obj_Rel_Insert_Input = {
  data: App_Project_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Project_On_Conflict>;
};

/** on_conflict condition type for table "app.project" */
export type App_Project_On_Conflict = {
  constraint: App_Project_Constraint;
  update_columns?: Array<App_Project_Update_Column>;
  where?: InputMaybe<App_Project_Bool_Exp>;
};

/** Ordering options when selecting data from "app.project". */
export type App_Project_Order_By = {
  budget?: InputMaybe<Order_By>;
  contact_pnrfo?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  deleted_at?: InputMaybe<Order_By>;
  description_short?: InputMaybe<Order_By>;
  header?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  owner?: InputMaybe<Order_By>;
  partner?: InputMaybe<Order_By>;
  photo?: InputMaybe<Order_By>;
  private_description?: InputMaybe<Order_By>;
  project_files_aggregate?: InputMaybe<App_Project_File_Aggregate_Order_By>;
  project_financings_aggregate?: InputMaybe<App_Project_Financing_Aggregate_Order_By>;
  project_territories_aggregate?: InputMaybe<App_Project_Territory_Aggregate_Order_By>;
  project_themes_aggregate?: InputMaybe<App_Project_Theme_Aggregate_Order_By>;
  project_types_aggregate?: InputMaybe<App_Project_Type_Aggregate_Order_By>;
  public_description?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  step?: InputMaybe<Order_By>;
  title?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  year?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app.project */
export type App_Project_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "app.project" */
export enum App_Project_Select_Column {
  /** column name */
  Budget = 'budget',
  /** column name */
  ContactPnrfo = 'contact_pnrfo',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DescriptionShort = 'description_short',
  /** column name */
  Header = 'header',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Owner = 'owner',
  /** column name */
  Partner = 'partner',
  /** column name */
  Photo = 'photo',
  /** column name */
  PrivateDescription = 'private_description',
  /** column name */
  PublicDescription = 'public_description',
  /** column name */
  Status = 'status',
  /** column name */
  Step = 'step',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Year = 'year'
}

/** input type for updating data in table "app.project" */
export type App_Project_Set_Input = {
  budget?: InputMaybe<Scalars['numeric']>;
  contact_pnrfo?: InputMaybe<Scalars['String']>;
  created_at?: InputMaybe<Scalars['timestamptz']>;
  deleted_at?: InputMaybe<Scalars['timestamptz']>;
  description_short?: InputMaybe<Scalars['String']>;
  header?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  label?: InputMaybe<Scalars['String']>;
  owner?: InputMaybe<Scalars['String']>;
  partner?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
  private_description?: InputMaybe<Scalars['String']>;
  public_description?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  step?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
  year?: InputMaybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type App_Project_Stddev_Fields = {
  __typename?: 'app_project_stddev_fields';
  budget?: Maybe<Scalars['Float']>;
  year?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type App_Project_Stddev_Pop_Fields = {
  __typename?: 'app_project_stddev_pop_fields';
  budget?: Maybe<Scalars['Float']>;
  year?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type App_Project_Stddev_Samp_Fields = {
  __typename?: 'app_project_stddev_samp_fields';
  budget?: Maybe<Scalars['Float']>;
  year?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "app_project" */
export type App_Project_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Project_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Project_Stream_Cursor_Value_Input = {
  budget?: InputMaybe<Scalars['numeric']>;
  contact_pnrfo?: InputMaybe<Scalars['String']>;
  created_at?: InputMaybe<Scalars['timestamptz']>;
  deleted_at?: InputMaybe<Scalars['timestamptz']>;
  description_short?: InputMaybe<Scalars['String']>;
  header?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  label?: InputMaybe<Scalars['String']>;
  owner?: InputMaybe<Scalars['String']>;
  partner?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
  private_description?: InputMaybe<Scalars['String']>;
  public_description?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  step?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
  year?: InputMaybe<Scalars['Int']>;
};

/** aggregate sum on columns */
export type App_Project_Sum_Fields = {
  __typename?: 'app_project_sum_fields';
  budget?: Maybe<Scalars['numeric']>;
  year?: Maybe<Scalars['Int']>;
};

/** columns and relationships of "app.project_territory" */
export type App_Project_Territory = {
  __typename?: 'app_project_territory';
  id: Scalars['uuid'];
  /** An object relationship */
  project: App_Project;
  project_id: Scalars['uuid'];
  /** An object relationship */
  territory: App_Territory;
  territory_id: Scalars['String'];
};

/** aggregated selection of "app.project_territory" */
export type App_Project_Territory_Aggregate = {
  __typename?: 'app_project_territory_aggregate';
  aggregate?: Maybe<App_Project_Territory_Aggregate_Fields>;
  nodes: Array<App_Project_Territory>;
};

/** aggregate fields of "app.project_territory" */
export type App_Project_Territory_Aggregate_Fields = {
  __typename?: 'app_project_territory_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Project_Territory_Max_Fields>;
  min?: Maybe<App_Project_Territory_Min_Fields>;
};


/** aggregate fields of "app.project_territory" */
export type App_Project_Territory_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "app.project_territory" */
export type App_Project_Territory_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<App_Project_Territory_Max_Order_By>;
  min?: InputMaybe<App_Project_Territory_Min_Order_By>;
};

/** input type for inserting array relation for remote table "app.project_territory" */
export type App_Project_Territory_Arr_Rel_Insert_Input = {
  data: Array<App_Project_Territory_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Project_Territory_On_Conflict>;
};

/** Boolean expression to filter rows from the table "app.project_territory". All fields are combined with a logical 'AND'. */
export type App_Project_Territory_Bool_Exp = {
  _and?: InputMaybe<Array<App_Project_Territory_Bool_Exp>>;
  _not?: InputMaybe<App_Project_Territory_Bool_Exp>;
  _or?: InputMaybe<Array<App_Project_Territory_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  project?: InputMaybe<App_Project_Bool_Exp>;
  project_id?: InputMaybe<Uuid_Comparison_Exp>;
  territory?: InputMaybe<App_Territory_Bool_Exp>;
  territory_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.project_territory" */
export enum App_Project_Territory_Constraint {
  /** unique or primary key constraint on columns "id" */
  ProjectTerritoryPkey = 'project_territory_pkey'
}

/** input type for inserting data into table "app.project_territory" */
export type App_Project_Territory_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project?: InputMaybe<App_Project_Obj_Rel_Insert_Input>;
  project_id?: InputMaybe<Scalars['uuid']>;
  territory?: InputMaybe<App_Territory_Obj_Rel_Insert_Input>;
  territory_id?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type App_Project_Territory_Max_Fields = {
  __typename?: 'app_project_territory_max_fields';
  id?: Maybe<Scalars['uuid']>;
  project_id?: Maybe<Scalars['uuid']>;
  territory_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "app.project_territory" */
export type App_Project_Territory_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  territory_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type App_Project_Territory_Min_Fields = {
  __typename?: 'app_project_territory_min_fields';
  id?: Maybe<Scalars['uuid']>;
  project_id?: Maybe<Scalars['uuid']>;
  territory_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "app.project_territory" */
export type App_Project_Territory_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  territory_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "app.project_territory" */
export type App_Project_Territory_Mutation_Response = {
  __typename?: 'app_project_territory_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Project_Territory>;
};

/** on_conflict condition type for table "app.project_territory" */
export type App_Project_Territory_On_Conflict = {
  constraint: App_Project_Territory_Constraint;
  update_columns?: Array<App_Project_Territory_Update_Column>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};

/** Ordering options when selecting data from "app.project_territory". */
export type App_Project_Territory_Order_By = {
  id?: InputMaybe<Order_By>;
  project?: InputMaybe<App_Project_Order_By>;
  project_id?: InputMaybe<Order_By>;
  territory?: InputMaybe<App_Territory_Order_By>;
  territory_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app.project_territory */
export type App_Project_Territory_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "app.project_territory" */
export enum App_Project_Territory_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  ProjectId = 'project_id',
  /** column name */
  TerritoryId = 'territory_id'
}

/** input type for updating data in table "app.project_territory" */
export type App_Project_Territory_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  territory_id?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "app_project_territory" */
export type App_Project_Territory_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Project_Territory_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Project_Territory_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  territory_id?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.project_territory" */
export enum App_Project_Territory_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  ProjectId = 'project_id',
  /** column name */
  TerritoryId = 'territory_id'
}

export type App_Project_Territory_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Project_Territory_Set_Input>;
  where: App_Project_Territory_Bool_Exp;
};

/** columns and relationships of "app.project_theme" */
export type App_Project_Theme = {
  __typename?: 'app_project_theme';
  id: Scalars['uuid'];
  /** An object relationship */
  project: App_Project;
  project_id: Scalars['uuid'];
  /** An object relationship */
  theme: App_Theme;
  theme_id: Scalars['String'];
};

/** aggregated selection of "app.project_theme" */
export type App_Project_Theme_Aggregate = {
  __typename?: 'app_project_theme_aggregate';
  aggregate?: Maybe<App_Project_Theme_Aggregate_Fields>;
  nodes: Array<App_Project_Theme>;
};

/** aggregate fields of "app.project_theme" */
export type App_Project_Theme_Aggregate_Fields = {
  __typename?: 'app_project_theme_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Project_Theme_Max_Fields>;
  min?: Maybe<App_Project_Theme_Min_Fields>;
};


/** aggregate fields of "app.project_theme" */
export type App_Project_Theme_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Project_Theme_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "app.project_theme" */
export type App_Project_Theme_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<App_Project_Theme_Max_Order_By>;
  min?: InputMaybe<App_Project_Theme_Min_Order_By>;
};

/** input type for inserting array relation for remote table "app.project_theme" */
export type App_Project_Theme_Arr_Rel_Insert_Input = {
  data: Array<App_Project_Theme_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Project_Theme_On_Conflict>;
};

/** Boolean expression to filter rows from the table "app.project_theme". All fields are combined with a logical 'AND'. */
export type App_Project_Theme_Bool_Exp = {
  _and?: InputMaybe<Array<App_Project_Theme_Bool_Exp>>;
  _not?: InputMaybe<App_Project_Theme_Bool_Exp>;
  _or?: InputMaybe<Array<App_Project_Theme_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  project?: InputMaybe<App_Project_Bool_Exp>;
  project_id?: InputMaybe<Uuid_Comparison_Exp>;
  theme?: InputMaybe<App_Theme_Bool_Exp>;
  theme_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.project_theme" */
export enum App_Project_Theme_Constraint {
  /** unique or primary key constraint on columns "id" */
  ProjectThemePkey = 'project_theme_pkey'
}

/** input type for inserting data into table "app.project_theme" */
export type App_Project_Theme_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project?: InputMaybe<App_Project_Obj_Rel_Insert_Input>;
  project_id?: InputMaybe<Scalars['uuid']>;
  theme?: InputMaybe<App_Theme_Obj_Rel_Insert_Input>;
  theme_id?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type App_Project_Theme_Max_Fields = {
  __typename?: 'app_project_theme_max_fields';
  id?: Maybe<Scalars['uuid']>;
  project_id?: Maybe<Scalars['uuid']>;
  theme_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "app.project_theme" */
export type App_Project_Theme_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  theme_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type App_Project_Theme_Min_Fields = {
  __typename?: 'app_project_theme_min_fields';
  id?: Maybe<Scalars['uuid']>;
  project_id?: Maybe<Scalars['uuid']>;
  theme_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "app.project_theme" */
export type App_Project_Theme_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  theme_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "app.project_theme" */
export type App_Project_Theme_Mutation_Response = {
  __typename?: 'app_project_theme_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Project_Theme>;
};

/** on_conflict condition type for table "app.project_theme" */
export type App_Project_Theme_On_Conflict = {
  constraint: App_Project_Theme_Constraint;
  update_columns?: Array<App_Project_Theme_Update_Column>;
  where?: InputMaybe<App_Project_Theme_Bool_Exp>;
};

/** Ordering options when selecting data from "app.project_theme". */
export type App_Project_Theme_Order_By = {
  id?: InputMaybe<Order_By>;
  project?: InputMaybe<App_Project_Order_By>;
  project_id?: InputMaybe<Order_By>;
  theme?: InputMaybe<App_Theme_Order_By>;
  theme_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app.project_theme */
export type App_Project_Theme_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "app.project_theme" */
export enum App_Project_Theme_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  ProjectId = 'project_id',
  /** column name */
  ThemeId = 'theme_id'
}

/** input type for updating data in table "app.project_theme" */
export type App_Project_Theme_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  theme_id?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "app_project_theme" */
export type App_Project_Theme_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Project_Theme_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Project_Theme_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  theme_id?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.project_theme" */
export enum App_Project_Theme_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  ProjectId = 'project_id',
  /** column name */
  ThemeId = 'theme_id'
}

export type App_Project_Theme_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Project_Theme_Set_Input>;
  where: App_Project_Theme_Bool_Exp;
};

/** columns and relationships of "app.project_type" */
export type App_Project_Type = {
  __typename?: 'app_project_type';
  id: Scalars['uuid'];
  /** An object relationship */
  project: App_Project;
  project_id: Scalars['uuid'];
  /** An object relationship */
  type: App_Type;
  type_id: Scalars['String'];
};

/** aggregated selection of "app.project_type" */
export type App_Project_Type_Aggregate = {
  __typename?: 'app_project_type_aggregate';
  aggregate?: Maybe<App_Project_Type_Aggregate_Fields>;
  nodes: Array<App_Project_Type>;
};

/** aggregate fields of "app.project_type" */
export type App_Project_Type_Aggregate_Fields = {
  __typename?: 'app_project_type_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Project_Type_Max_Fields>;
  min?: Maybe<App_Project_Type_Min_Fields>;
};


/** aggregate fields of "app.project_type" */
export type App_Project_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Project_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "app.project_type" */
export type App_Project_Type_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<App_Project_Type_Max_Order_By>;
  min?: InputMaybe<App_Project_Type_Min_Order_By>;
};

/** input type for inserting array relation for remote table "app.project_type" */
export type App_Project_Type_Arr_Rel_Insert_Input = {
  data: Array<App_Project_Type_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Project_Type_On_Conflict>;
};

/** Boolean expression to filter rows from the table "app.project_type". All fields are combined with a logical 'AND'. */
export type App_Project_Type_Bool_Exp = {
  _and?: InputMaybe<Array<App_Project_Type_Bool_Exp>>;
  _not?: InputMaybe<App_Project_Type_Bool_Exp>;
  _or?: InputMaybe<Array<App_Project_Type_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  project?: InputMaybe<App_Project_Bool_Exp>;
  project_id?: InputMaybe<Uuid_Comparison_Exp>;
  type?: InputMaybe<App_Type_Bool_Exp>;
  type_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.project_type" */
export enum App_Project_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  ProjectTypePkey = 'project_type_pkey'
}

/** input type for inserting data into table "app.project_type" */
export type App_Project_Type_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project?: InputMaybe<App_Project_Obj_Rel_Insert_Input>;
  project_id?: InputMaybe<Scalars['uuid']>;
  type?: InputMaybe<App_Type_Obj_Rel_Insert_Input>;
  type_id?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type App_Project_Type_Max_Fields = {
  __typename?: 'app_project_type_max_fields';
  id?: Maybe<Scalars['uuid']>;
  project_id?: Maybe<Scalars['uuid']>;
  type_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "app.project_type" */
export type App_Project_Type_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  type_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type App_Project_Type_Min_Fields = {
  __typename?: 'app_project_type_min_fields';
  id?: Maybe<Scalars['uuid']>;
  project_id?: Maybe<Scalars['uuid']>;
  type_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "app.project_type" */
export type App_Project_Type_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  project_id?: InputMaybe<Order_By>;
  type_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "app.project_type" */
export type App_Project_Type_Mutation_Response = {
  __typename?: 'app_project_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Project_Type>;
};

/** on_conflict condition type for table "app.project_type" */
export type App_Project_Type_On_Conflict = {
  constraint: App_Project_Type_Constraint;
  update_columns?: Array<App_Project_Type_Update_Column>;
  where?: InputMaybe<App_Project_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "app.project_type". */
export type App_Project_Type_Order_By = {
  id?: InputMaybe<Order_By>;
  project?: InputMaybe<App_Project_Order_By>;
  project_id?: InputMaybe<Order_By>;
  type?: InputMaybe<App_Type_Order_By>;
  type_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app.project_type */
export type App_Project_Type_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "app.project_type" */
export enum App_Project_Type_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  ProjectId = 'project_id',
  /** column name */
  TypeId = 'type_id'
}

/** input type for updating data in table "app.project_type" */
export type App_Project_Type_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  type_id?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "app_project_type" */
export type App_Project_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Project_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Project_Type_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  project_id?: InputMaybe<Scalars['uuid']>;
  type_id?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.project_type" */
export enum App_Project_Type_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  ProjectId = 'project_id',
  /** column name */
  TypeId = 'type_id'
}

export type App_Project_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Project_Type_Set_Input>;
  where: App_Project_Type_Bool_Exp;
};

/** update columns of table "app.project" */
export enum App_Project_Update_Column {
  /** column name */
  Budget = 'budget',
  /** column name */
  ContactPnrfo = 'contact_pnrfo',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DescriptionShort = 'description_short',
  /** column name */
  Header = 'header',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Owner = 'owner',
  /** column name */
  Partner = 'partner',
  /** column name */
  Photo = 'photo',
  /** column name */
  PrivateDescription = 'private_description',
  /** column name */
  PublicDescription = 'public_description',
  /** column name */
  Status = 'status',
  /** column name */
  Step = 'step',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Year = 'year'
}

export type App_Project_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<App_Project_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Project_Set_Input>;
  where: App_Project_Bool_Exp;
};

/** aggregate var_pop on columns */
export type App_Project_Var_Pop_Fields = {
  __typename?: 'app_project_var_pop_fields';
  budget?: Maybe<Scalars['Float']>;
  year?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type App_Project_Var_Samp_Fields = {
  __typename?: 'app_project_var_samp_fields';
  budget?: Maybe<Scalars['Float']>;
  year?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type App_Project_Variance_Fields = {
  __typename?: 'app_project_variance_fields';
  budget?: Maybe<Scalars['Float']>;
  year?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "app.territory" */
export type App_Territory = {
  __typename?: 'app_territory';
  coords?: Maybe<Scalars['geometry']>;
  id: Scalars['String'];
  label: Scalars['String'];
  /** An array relationship */
  territory_projects: Array<App_Project_Territory>;
  /** An aggregate relationship */
  territory_projects_aggregate: App_Project_Territory_Aggregate;
};


/** columns and relationships of "app.territory" */
export type App_TerritoryTerritory_ProjectsArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Territory_Order_By>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};


/** columns and relationships of "app.territory" */
export type App_TerritoryTerritory_Projects_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Territory_Order_By>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};

/** aggregated selection of "app.territory" */
export type App_Territory_Aggregate = {
  __typename?: 'app_territory_aggregate';
  aggregate?: Maybe<App_Territory_Aggregate_Fields>;
  nodes: Array<App_Territory>;
};

/** aggregate fields of "app.territory" */
export type App_Territory_Aggregate_Fields = {
  __typename?: 'app_territory_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Territory_Max_Fields>;
  min?: Maybe<App_Territory_Min_Fields>;
};


/** aggregate fields of "app.territory" */
export type App_Territory_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Territory_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "app.territory". All fields are combined with a logical 'AND'. */
export type App_Territory_Bool_Exp = {
  _and?: InputMaybe<Array<App_Territory_Bool_Exp>>;
  _not?: InputMaybe<App_Territory_Bool_Exp>;
  _or?: InputMaybe<Array<App_Territory_Bool_Exp>>;
  coords?: InputMaybe<Geometry_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  territory_projects?: InputMaybe<App_Project_Territory_Bool_Exp>;
};

/** unique or primary key constraints on table "app.territory" */
export enum App_Territory_Constraint {
  /** unique or primary key constraint on columns "id" */
  TerritoryPkey = 'territory_pkey'
}

/** input type for inserting data into table "app.territory" */
export type App_Territory_Insert_Input = {
  coords?: InputMaybe<Scalars['geometry']>;
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
  territory_projects?: InputMaybe<App_Project_Territory_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type App_Territory_Max_Fields = {
  __typename?: 'app_territory_max_fields';
  id?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type App_Territory_Min_Fields = {
  __typename?: 'app_territory_min_fields';
  id?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "app.territory" */
export type App_Territory_Mutation_Response = {
  __typename?: 'app_territory_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Territory>;
};

/** input type for inserting object relation for remote table "app.territory" */
export type App_Territory_Obj_Rel_Insert_Input = {
  data: App_Territory_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Territory_On_Conflict>;
};

/** on_conflict condition type for table "app.territory" */
export type App_Territory_On_Conflict = {
  constraint: App_Territory_Constraint;
  update_columns?: Array<App_Territory_Update_Column>;
  where?: InputMaybe<App_Territory_Bool_Exp>;
};

/** Ordering options when selecting data from "app.territory". */
export type App_Territory_Order_By = {
  coords?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  territory_projects_aggregate?: InputMaybe<App_Project_Territory_Aggregate_Order_By>;
};

/** primary key columns input for table: app.territory */
export type App_Territory_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "app.territory" */
export enum App_Territory_Select_Column {
  /** column name */
  Coords = 'coords',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label'
}

/** input type for updating data in table "app.territory" */
export type App_Territory_Set_Input = {
  coords?: InputMaybe<Scalars['geometry']>;
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "app_territory" */
export type App_Territory_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Territory_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Territory_Stream_Cursor_Value_Input = {
  coords?: InputMaybe<Scalars['geometry']>;
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.territory" */
export enum App_Territory_Update_Column {
  /** column name */
  Coords = 'coords',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label'
}

export type App_Territory_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Territory_Set_Input>;
  where: App_Territory_Bool_Exp;
};

/** columns and relationships of "app.theme" */
export type App_Theme = {
  __typename?: 'app_theme';
  id: Scalars['String'];
  label: Scalars['String'];
};

/** aggregated selection of "app.theme" */
export type App_Theme_Aggregate = {
  __typename?: 'app_theme_aggregate';
  aggregate?: Maybe<App_Theme_Aggregate_Fields>;
  nodes: Array<App_Theme>;
};

/** aggregate fields of "app.theme" */
export type App_Theme_Aggregate_Fields = {
  __typename?: 'app_theme_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Theme_Max_Fields>;
  min?: Maybe<App_Theme_Min_Fields>;
};


/** aggregate fields of "app.theme" */
export type App_Theme_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Theme_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "app.theme". All fields are combined with a logical 'AND'. */
export type App_Theme_Bool_Exp = {
  _and?: InputMaybe<Array<App_Theme_Bool_Exp>>;
  _not?: InputMaybe<App_Theme_Bool_Exp>;
  _or?: InputMaybe<Array<App_Theme_Bool_Exp>>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.theme" */
export enum App_Theme_Constraint {
  /** unique or primary key constraint on columns "id" */
  ThemePkey = 'theme_pkey'
}

/** input type for inserting data into table "app.theme" */
export type App_Theme_Insert_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type App_Theme_Max_Fields = {
  __typename?: 'app_theme_max_fields';
  id?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type App_Theme_Min_Fields = {
  __typename?: 'app_theme_min_fields';
  id?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "app.theme" */
export type App_Theme_Mutation_Response = {
  __typename?: 'app_theme_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Theme>;
};

/** input type for inserting object relation for remote table "app.theme" */
export type App_Theme_Obj_Rel_Insert_Input = {
  data: App_Theme_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Theme_On_Conflict>;
};

/** on_conflict condition type for table "app.theme" */
export type App_Theme_On_Conflict = {
  constraint: App_Theme_Constraint;
  update_columns?: Array<App_Theme_Update_Column>;
  where?: InputMaybe<App_Theme_Bool_Exp>;
};

/** Ordering options when selecting data from "app.theme". */
export type App_Theme_Order_By = {
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app.theme */
export type App_Theme_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "app.theme" */
export enum App_Theme_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label'
}

/** input type for updating data in table "app.theme" */
export type App_Theme_Set_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "app_theme" */
export type App_Theme_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Theme_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Theme_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.theme" */
export enum App_Theme_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label'
}

export type App_Theme_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Theme_Set_Input>;
  where: App_Theme_Bool_Exp;
};

/** columns and relationships of "app.type" */
export type App_Type = {
  __typename?: 'app_type';
  id: Scalars['String'];
  label: Scalars['String'];
};

/** aggregated selection of "app.type" */
export type App_Type_Aggregate = {
  __typename?: 'app_type_aggregate';
  aggregate?: Maybe<App_Type_Aggregate_Fields>;
  nodes: Array<App_Type>;
};

/** aggregate fields of "app.type" */
export type App_Type_Aggregate_Fields = {
  __typename?: 'app_type_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<App_Type_Max_Fields>;
  min?: Maybe<App_Type_Min_Fields>;
};


/** aggregate fields of "app.type" */
export type App_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<App_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "app.type". All fields are combined with a logical 'AND'. */
export type App_Type_Bool_Exp = {
  _and?: InputMaybe<Array<App_Type_Bool_Exp>>;
  _not?: InputMaybe<App_Type_Bool_Exp>;
  _or?: InputMaybe<Array<App_Type_Bool_Exp>>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.type" */
export enum App_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  TypePkey = 'type_pkey'
}

/** input type for inserting data into table "app.type" */
export type App_Type_Insert_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type App_Type_Max_Fields = {
  __typename?: 'app_type_max_fields';
  id?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type App_Type_Min_Fields = {
  __typename?: 'app_type_min_fields';
  id?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "app.type" */
export type App_Type_Mutation_Response = {
  __typename?: 'app_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<App_Type>;
};

/** input type for inserting object relation for remote table "app.type" */
export type App_Type_Obj_Rel_Insert_Input = {
  data: App_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<App_Type_On_Conflict>;
};

/** on_conflict condition type for table "app.type" */
export type App_Type_On_Conflict = {
  constraint: App_Type_Constraint;
  update_columns?: Array<App_Type_Update_Column>;
  where?: InputMaybe<App_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "app.type". */
export type App_Type_Order_By = {
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app.type */
export type App_Type_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "app.type" */
export enum App_Type_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label'
}

/** input type for updating data in table "app.type" */
export type App_Type_Set_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "app_type" */
export type App_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: App_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type App_Type_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.type" */
export enum App_Type_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label'
}

export type App_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<App_Type_Set_Input>;
  where: App_Type_Bool_Exp;
};

/** Boolean expression to compare columns of type "bigint". All fields are combined with logical 'AND'. */
export type Bigint_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['bigint']>;
  _gt?: InputMaybe<Scalars['bigint']>;
  _gte?: InputMaybe<Scalars['bigint']>;
  _in?: InputMaybe<Array<Scalars['bigint']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['bigint']>;
  _lte?: InputMaybe<Scalars['bigint']>;
  _neq?: InputMaybe<Scalars['bigint']>;
  _nin?: InputMaybe<Array<Scalars['bigint']>>;
};

/** ordering argument of a cursor */
export enum Cursor_Ordering {
  /** ascending ordering of the cursor */
  Asc = 'ASC',
  /** descending ordering of the cursor */
  Desc = 'DESC'
}

export type Geography_Cast_Exp = {
  geometry?: InputMaybe<Geometry_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "geography". All fields are combined with logical 'AND'. */
export type Geography_Comparison_Exp = {
  _cast?: InputMaybe<Geography_Cast_Exp>;
  _eq?: InputMaybe<Scalars['geography']>;
  _gt?: InputMaybe<Scalars['geography']>;
  _gte?: InputMaybe<Scalars['geography']>;
  _in?: InputMaybe<Array<Scalars['geography']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['geography']>;
  _lte?: InputMaybe<Scalars['geography']>;
  _neq?: InputMaybe<Scalars['geography']>;
  _nin?: InputMaybe<Array<Scalars['geography']>>;
  /** is the column within a given distance from the given geography value */
  _st_d_within?: InputMaybe<St_D_Within_Geography_Input>;
  /** does the column spatially intersect the given geography value */
  _st_intersects?: InputMaybe<Scalars['geography']>;
};

export type Geometry_Cast_Exp = {
  geography?: InputMaybe<Geography_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "geometry". All fields are combined with logical 'AND'. */
export type Geometry_Comparison_Exp = {
  _cast?: InputMaybe<Geometry_Cast_Exp>;
  _eq?: InputMaybe<Scalars['geometry']>;
  _gt?: InputMaybe<Scalars['geometry']>;
  _gte?: InputMaybe<Scalars['geometry']>;
  _in?: InputMaybe<Array<Scalars['geometry']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['geometry']>;
  _lte?: InputMaybe<Scalars['geometry']>;
  _neq?: InputMaybe<Scalars['geometry']>;
  _nin?: InputMaybe<Array<Scalars['geometry']>>;
  /** is the column within a given 3D distance from the given geometry value */
  _st_3d_d_within?: InputMaybe<St_D_Within_Input>;
  /** does the column spatially intersect the given geometry value in 3D */
  _st_3d_intersects?: InputMaybe<Scalars['geometry']>;
  /** does the column contain the given geometry value */
  _st_contains?: InputMaybe<Scalars['geometry']>;
  /** does the column cross the given geometry value */
  _st_crosses?: InputMaybe<Scalars['geometry']>;
  /** is the column within a given distance from the given geometry value */
  _st_d_within?: InputMaybe<St_D_Within_Input>;
  /** is the column equal to given geometry value (directionality is ignored) */
  _st_equals?: InputMaybe<Scalars['geometry']>;
  /** does the column spatially intersect the given geometry value */
  _st_intersects?: InputMaybe<Scalars['geometry']>;
  /** does the column 'spatially overlap' (intersect but not completely contain) the given geometry value */
  _st_overlaps?: InputMaybe<Scalars['geometry']>;
  /** does the column have atleast one point in common with the given geometry value */
  _st_touches?: InputMaybe<Scalars['geometry']>;
  /** is the column contained in the given geometry value */
  _st_within?: InputMaybe<Scalars['geometry']>;
};

/** columns and relationships of "keycloak_group" */
export type Keycloak_Group = {
  __typename?: 'keycloak_group';
  id: Scalars['String'];
  /** An array relationship */
  keycloak__top_gp: Array<User_Group_Membership>;
  /** An aggregate relationship */
  keycloak__top_gp_aggregate: User_Group_Membership_Aggregate;
  name?: Maybe<Scalars['String']>;
  parent_group: Scalars['String'];
  realm_id?: Maybe<Scalars['String']>;
};


/** columns and relationships of "keycloak_group" */
export type Keycloak_GroupKeycloak__Top_GpArgs = {
  distinct_on?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Group_Membership_Order_By>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};


/** columns and relationships of "keycloak_group" */
export type Keycloak_GroupKeycloak__Top_Gp_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Group_Membership_Order_By>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};

/** aggregated selection of "keycloak_group" */
export type Keycloak_Group_Aggregate = {
  __typename?: 'keycloak_group_aggregate';
  aggregate?: Maybe<Keycloak_Group_Aggregate_Fields>;
  nodes: Array<Keycloak_Group>;
};

/** aggregate fields of "keycloak_group" */
export type Keycloak_Group_Aggregate_Fields = {
  __typename?: 'keycloak_group_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Keycloak_Group_Max_Fields>;
  min?: Maybe<Keycloak_Group_Min_Fields>;
};


/** aggregate fields of "keycloak_group" */
export type Keycloak_Group_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Keycloak_Group_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "keycloak_group". All fields are combined with a logical 'AND'. */
export type Keycloak_Group_Bool_Exp = {
  _and?: InputMaybe<Array<Keycloak_Group_Bool_Exp>>;
  _not?: InputMaybe<Keycloak_Group_Bool_Exp>;
  _or?: InputMaybe<Array<Keycloak_Group_Bool_Exp>>;
  id?: InputMaybe<String_Comparison_Exp>;
  keycloak__top_gp?: InputMaybe<User_Group_Membership_Bool_Exp>;
  name?: InputMaybe<String_Comparison_Exp>;
  parent_group?: InputMaybe<String_Comparison_Exp>;
  realm_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "keycloak_group" */
export enum Keycloak_Group_Constraint {
  /** unique or primary key constraint on columns "id" */
  ConstraintGroup = 'constraint_group',
  /** unique or primary key constraint on columns "realm_id", "name", "parent_group" */
  SiblingNames = 'sibling_names'
}

/** input type for inserting data into table "keycloak_group" */
export type Keycloak_Group_Insert_Input = {
  id?: InputMaybe<Scalars['String']>;
  keycloak__top_gp?: InputMaybe<User_Group_Membership_Arr_Rel_Insert_Input>;
  name?: InputMaybe<Scalars['String']>;
  parent_group?: InputMaybe<Scalars['String']>;
  realm_id?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Keycloak_Group_Max_Fields = {
  __typename?: 'keycloak_group_max_fields';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  parent_group?: Maybe<Scalars['String']>;
  realm_id?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Keycloak_Group_Min_Fields = {
  __typename?: 'keycloak_group_min_fields';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  parent_group?: Maybe<Scalars['String']>;
  realm_id?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "keycloak_group" */
export type Keycloak_Group_Mutation_Response = {
  __typename?: 'keycloak_group_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Keycloak_Group>;
};

/** input type for inserting object relation for remote table "keycloak_group" */
export type Keycloak_Group_Obj_Rel_Insert_Input = {
  data: Keycloak_Group_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Keycloak_Group_On_Conflict>;
};

/** on_conflict condition type for table "keycloak_group" */
export type Keycloak_Group_On_Conflict = {
  constraint: Keycloak_Group_Constraint;
  update_columns?: Array<Keycloak_Group_Update_Column>;
  where?: InputMaybe<Keycloak_Group_Bool_Exp>;
};

/** Ordering options when selecting data from "keycloak_group". */
export type Keycloak_Group_Order_By = {
  id?: InputMaybe<Order_By>;
  keycloak__top_gp_aggregate?: InputMaybe<User_Group_Membership_Aggregate_Order_By>;
  name?: InputMaybe<Order_By>;
  parent_group?: InputMaybe<Order_By>;
  realm_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: keycloak_group */
export type Keycloak_Group_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "keycloak_group" */
export enum Keycloak_Group_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  ParentGroup = 'parent_group',
  /** column name */
  RealmId = 'realm_id'
}

/** input type for updating data in table "keycloak_group" */
export type Keycloak_Group_Set_Input = {
  id?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  parent_group?: InputMaybe<Scalars['String']>;
  realm_id?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "keycloak_group" */
export type Keycloak_Group_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Keycloak_Group_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Keycloak_Group_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  parent_group?: InputMaybe<Scalars['String']>;
  realm_id?: InputMaybe<Scalars['String']>;
};

/** update columns of table "keycloak_group" */
export enum Keycloak_Group_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  ParentGroup = 'parent_group',
  /** column name */
  RealmId = 'realm_id'
}

export type Keycloak_Group_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Keycloak_Group_Set_Input>;
  where: Keycloak_Group_Bool_Exp;
};

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root';
  /** delete data from the table: "app.financing" */
  delete_app_financing?: Maybe<App_Financing_Mutation_Response>;
  /** delete single row from the table: "app.financing" */
  delete_app_financing_by_pk?: Maybe<App_Financing>;
  /** delete data from the table: "app.project" */
  delete_app_project?: Maybe<App_Project_Mutation_Response>;
  /** delete single row from the table: "app.project" */
  delete_app_project_by_pk?: Maybe<App_Project>;
  /** delete data from the table: "app.project_file" */
  delete_app_project_file?: Maybe<App_Project_File_Mutation_Response>;
  /** delete single row from the table: "app.project_file" */
  delete_app_project_file_by_pk?: Maybe<App_Project_File>;
  /** delete data from the table: "app.project_financing" */
  delete_app_project_financing?: Maybe<App_Project_Financing_Mutation_Response>;
  /** delete single row from the table: "app.project_financing" */
  delete_app_project_financing_by_pk?: Maybe<App_Project_Financing>;
  /** delete data from the table: "app.project_territory" */
  delete_app_project_territory?: Maybe<App_Project_Territory_Mutation_Response>;
  /** delete single row from the table: "app.project_territory" */
  delete_app_project_territory_by_pk?: Maybe<App_Project_Territory>;
  /** delete data from the table: "app.project_theme" */
  delete_app_project_theme?: Maybe<App_Project_Theme_Mutation_Response>;
  /** delete single row from the table: "app.project_theme" */
  delete_app_project_theme_by_pk?: Maybe<App_Project_Theme>;
  /** delete data from the table: "app.project_type" */
  delete_app_project_type?: Maybe<App_Project_Type_Mutation_Response>;
  /** delete single row from the table: "app.project_type" */
  delete_app_project_type_by_pk?: Maybe<App_Project_Type>;
  /** delete data from the table: "app.territory" */
  delete_app_territory?: Maybe<App_Territory_Mutation_Response>;
  /** delete single row from the table: "app.territory" */
  delete_app_territory_by_pk?: Maybe<App_Territory>;
  /** delete data from the table: "app.theme" */
  delete_app_theme?: Maybe<App_Theme_Mutation_Response>;
  /** delete single row from the table: "app.theme" */
  delete_app_theme_by_pk?: Maybe<App_Theme>;
  /** delete data from the table: "app.type" */
  delete_app_type?: Maybe<App_Type_Mutation_Response>;
  /** delete single row from the table: "app.type" */
  delete_app_type_by_pk?: Maybe<App_Type>;
  /** delete data from the table: "keycloak_group" */
  delete_keycloak_group?: Maybe<Keycloak_Group_Mutation_Response>;
  /** delete single row from the table: "keycloak_group" */
  delete_keycloak_group_by_pk?: Maybe<Keycloak_Group>;
  /** delete data from the table: "test" */
  delete_test?: Maybe<Test_Mutation_Response>;
  /** delete single row from the table: "test" */
  delete_test_by_pk?: Maybe<Test>;
  /** delete data from the table: "user_entity" */
  delete_user_entity?: Maybe<User_Entity_Mutation_Response>;
  /** delete single row from the table: "user_entity" */
  delete_user_entity_by_pk?: Maybe<User_Entity>;
  /** delete data from the table: "user_group_membership" */
  delete_user_group_membership?: Maybe<User_Group_Membership_Mutation_Response>;
  /** delete single row from the table: "user_group_membership" */
  delete_user_group_membership_by_pk?: Maybe<User_Group_Membership>;
  /** insert data into the table: "app.financing" */
  insert_app_financing?: Maybe<App_Financing_Mutation_Response>;
  /** insert a single row into the table: "app.financing" */
  insert_app_financing_one?: Maybe<App_Financing>;
  /** insert data into the table: "app.project" */
  insert_app_project?: Maybe<App_Project_Mutation_Response>;
  /** insert data into the table: "app.project_file" */
  insert_app_project_file?: Maybe<App_Project_File_Mutation_Response>;
  /** insert a single row into the table: "app.project_file" */
  insert_app_project_file_one?: Maybe<App_Project_File>;
  /** insert data into the table: "app.project_financing" */
  insert_app_project_financing?: Maybe<App_Project_Financing_Mutation_Response>;
  /** insert a single row into the table: "app.project_financing" */
  insert_app_project_financing_one?: Maybe<App_Project_Financing>;
  /** insert a single row into the table: "app.project" */
  insert_app_project_one?: Maybe<App_Project>;
  /** insert data into the table: "app.project_territory" */
  insert_app_project_territory?: Maybe<App_Project_Territory_Mutation_Response>;
  /** insert a single row into the table: "app.project_territory" */
  insert_app_project_territory_one?: Maybe<App_Project_Territory>;
  /** insert data into the table: "app.project_theme" */
  insert_app_project_theme?: Maybe<App_Project_Theme_Mutation_Response>;
  /** insert a single row into the table: "app.project_theme" */
  insert_app_project_theme_one?: Maybe<App_Project_Theme>;
  /** insert data into the table: "app.project_type" */
  insert_app_project_type?: Maybe<App_Project_Type_Mutation_Response>;
  /** insert a single row into the table: "app.project_type" */
  insert_app_project_type_one?: Maybe<App_Project_Type>;
  /** insert data into the table: "app.territory" */
  insert_app_territory?: Maybe<App_Territory_Mutation_Response>;
  /** insert a single row into the table: "app.territory" */
  insert_app_territory_one?: Maybe<App_Territory>;
  /** insert data into the table: "app.theme" */
  insert_app_theme?: Maybe<App_Theme_Mutation_Response>;
  /** insert a single row into the table: "app.theme" */
  insert_app_theme_one?: Maybe<App_Theme>;
  /** insert data into the table: "app.type" */
  insert_app_type?: Maybe<App_Type_Mutation_Response>;
  /** insert a single row into the table: "app.type" */
  insert_app_type_one?: Maybe<App_Type>;
  /** insert data into the table: "keycloak_group" */
  insert_keycloak_group?: Maybe<Keycloak_Group_Mutation_Response>;
  /** insert a single row into the table: "keycloak_group" */
  insert_keycloak_group_one?: Maybe<Keycloak_Group>;
  /** insert data into the table: "test" */
  insert_test?: Maybe<Test_Mutation_Response>;
  /** insert a single row into the table: "test" */
  insert_test_one?: Maybe<Test>;
  /** insert data into the table: "user_entity" */
  insert_user_entity?: Maybe<User_Entity_Mutation_Response>;
  /** insert a single row into the table: "user_entity" */
  insert_user_entity_one?: Maybe<User_Entity>;
  /** insert data into the table: "user_group_membership" */
  insert_user_group_membership?: Maybe<User_Group_Membership_Mutation_Response>;
  /** insert a single row into the table: "user_group_membership" */
  insert_user_group_membership_one?: Maybe<User_Group_Membership>;
  /** update data of the table: "app.financing" */
  update_app_financing?: Maybe<App_Financing_Mutation_Response>;
  /** update single row of the table: "app.financing" */
  update_app_financing_by_pk?: Maybe<App_Financing>;
  /** update multiples rows of table: "app.financing" */
  update_app_financing_many?: Maybe<Array<Maybe<App_Financing_Mutation_Response>>>;
  /** update data of the table: "app.project" */
  update_app_project?: Maybe<App_Project_Mutation_Response>;
  /** update single row of the table: "app.project" */
  update_app_project_by_pk?: Maybe<App_Project>;
  /** update data of the table: "app.project_file" */
  update_app_project_file?: Maybe<App_Project_File_Mutation_Response>;
  /** update single row of the table: "app.project_file" */
  update_app_project_file_by_pk?: Maybe<App_Project_File>;
  /** update multiples rows of table: "app.project_file" */
  update_app_project_file_many?: Maybe<Array<Maybe<App_Project_File_Mutation_Response>>>;
  /** update data of the table: "app.project_financing" */
  update_app_project_financing?: Maybe<App_Project_Financing_Mutation_Response>;
  /** update single row of the table: "app.project_financing" */
  update_app_project_financing_by_pk?: Maybe<App_Project_Financing>;
  /** update multiples rows of table: "app.project_financing" */
  update_app_project_financing_many?: Maybe<Array<Maybe<App_Project_Financing_Mutation_Response>>>;
  /** update multiples rows of table: "app.project" */
  update_app_project_many?: Maybe<Array<Maybe<App_Project_Mutation_Response>>>;
  /** update data of the table: "app.project_territory" */
  update_app_project_territory?: Maybe<App_Project_Territory_Mutation_Response>;
  /** update single row of the table: "app.project_territory" */
  update_app_project_territory_by_pk?: Maybe<App_Project_Territory>;
  /** update multiples rows of table: "app.project_territory" */
  update_app_project_territory_many?: Maybe<Array<Maybe<App_Project_Territory_Mutation_Response>>>;
  /** update data of the table: "app.project_theme" */
  update_app_project_theme?: Maybe<App_Project_Theme_Mutation_Response>;
  /** update single row of the table: "app.project_theme" */
  update_app_project_theme_by_pk?: Maybe<App_Project_Theme>;
  /** update multiples rows of table: "app.project_theme" */
  update_app_project_theme_many?: Maybe<Array<Maybe<App_Project_Theme_Mutation_Response>>>;
  /** update data of the table: "app.project_type" */
  update_app_project_type?: Maybe<App_Project_Type_Mutation_Response>;
  /** update single row of the table: "app.project_type" */
  update_app_project_type_by_pk?: Maybe<App_Project_Type>;
  /** update multiples rows of table: "app.project_type" */
  update_app_project_type_many?: Maybe<Array<Maybe<App_Project_Type_Mutation_Response>>>;
  /** update data of the table: "app.territory" */
  update_app_territory?: Maybe<App_Territory_Mutation_Response>;
  /** update single row of the table: "app.territory" */
  update_app_territory_by_pk?: Maybe<App_Territory>;
  /** update multiples rows of table: "app.territory" */
  update_app_territory_many?: Maybe<Array<Maybe<App_Territory_Mutation_Response>>>;
  /** update data of the table: "app.theme" */
  update_app_theme?: Maybe<App_Theme_Mutation_Response>;
  /** update single row of the table: "app.theme" */
  update_app_theme_by_pk?: Maybe<App_Theme>;
  /** update multiples rows of table: "app.theme" */
  update_app_theme_many?: Maybe<Array<Maybe<App_Theme_Mutation_Response>>>;
  /** update data of the table: "app.type" */
  update_app_type?: Maybe<App_Type_Mutation_Response>;
  /** update single row of the table: "app.type" */
  update_app_type_by_pk?: Maybe<App_Type>;
  /** update multiples rows of table: "app.type" */
  update_app_type_many?: Maybe<Array<Maybe<App_Type_Mutation_Response>>>;
  /** update data of the table: "keycloak_group" */
  update_keycloak_group?: Maybe<Keycloak_Group_Mutation_Response>;
  /** update single row of the table: "keycloak_group" */
  update_keycloak_group_by_pk?: Maybe<Keycloak_Group>;
  /** update multiples rows of table: "keycloak_group" */
  update_keycloak_group_many?: Maybe<Array<Maybe<Keycloak_Group_Mutation_Response>>>;
  /** update data of the table: "test" */
  update_test?: Maybe<Test_Mutation_Response>;
  /** update single row of the table: "test" */
  update_test_by_pk?: Maybe<Test>;
  /** update multiples rows of table: "test" */
  update_test_many?: Maybe<Array<Maybe<Test_Mutation_Response>>>;
  /** update data of the table: "user_entity" */
  update_user_entity?: Maybe<User_Entity_Mutation_Response>;
  /** update single row of the table: "user_entity" */
  update_user_entity_by_pk?: Maybe<User_Entity>;
  /** update multiples rows of table: "user_entity" */
  update_user_entity_many?: Maybe<Array<Maybe<User_Entity_Mutation_Response>>>;
  /** update data of the table: "user_group_membership" */
  update_user_group_membership?: Maybe<User_Group_Membership_Mutation_Response>;
  /** update single row of the table: "user_group_membership" */
  update_user_group_membership_by_pk?: Maybe<User_Group_Membership>;
  /** update multiples rows of table: "user_group_membership" */
  update_user_group_membership_many?: Maybe<Array<Maybe<User_Group_Membership_Mutation_Response>>>;
};


/** mutation root */
export type Mutation_RootDelete_App_FinancingArgs = {
  where: App_Financing_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Financing_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_App_ProjectArgs = {
  where: App_Project_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Project_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_App_Project_FileArgs = {
  where: App_Project_File_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Project_File_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_App_Project_FinancingArgs = {
  where: App_Project_Financing_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Project_Financing_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_App_Project_TerritoryArgs = {
  where: App_Project_Territory_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Project_Territory_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_App_Project_ThemeArgs = {
  where: App_Project_Theme_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Project_Theme_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_App_Project_TypeArgs = {
  where: App_Project_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Project_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_App_TerritoryArgs = {
  where: App_Territory_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Territory_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_App_ThemeArgs = {
  where: App_Theme_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Theme_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_App_TypeArgs = {
  where: App_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_App_Type_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_Keycloak_GroupArgs = {
  where: Keycloak_Group_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Keycloak_Group_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_TestArgs = {
  where: Test_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Test_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_User_EntityArgs = {
  where: User_Entity_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_User_Entity_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_User_Group_MembershipArgs = {
  where: User_Group_Membership_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_User_Group_Membership_By_PkArgs = {
  group_id: Scalars['String'];
  user_id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootInsert_App_FinancingArgs = {
  objects: Array<App_Financing_Insert_Input>;
  on_conflict?: InputMaybe<App_Financing_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Financing_OneArgs = {
  object: App_Financing_Insert_Input;
  on_conflict?: InputMaybe<App_Financing_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_ProjectArgs = {
  objects: Array<App_Project_Insert_Input>;
  on_conflict?: InputMaybe<App_Project_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_FileArgs = {
  objects: Array<App_Project_File_Insert_Input>;
  on_conflict?: InputMaybe<App_Project_File_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_File_OneArgs = {
  object: App_Project_File_Insert_Input;
  on_conflict?: InputMaybe<App_Project_File_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_FinancingArgs = {
  objects: Array<App_Project_Financing_Insert_Input>;
  on_conflict?: InputMaybe<App_Project_Financing_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_Financing_OneArgs = {
  object: App_Project_Financing_Insert_Input;
  on_conflict?: InputMaybe<App_Project_Financing_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_OneArgs = {
  object: App_Project_Insert_Input;
  on_conflict?: InputMaybe<App_Project_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_TerritoryArgs = {
  objects: Array<App_Project_Territory_Insert_Input>;
  on_conflict?: InputMaybe<App_Project_Territory_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_Territory_OneArgs = {
  object: App_Project_Territory_Insert_Input;
  on_conflict?: InputMaybe<App_Project_Territory_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_ThemeArgs = {
  objects: Array<App_Project_Theme_Insert_Input>;
  on_conflict?: InputMaybe<App_Project_Theme_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_Theme_OneArgs = {
  object: App_Project_Theme_Insert_Input;
  on_conflict?: InputMaybe<App_Project_Theme_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_TypeArgs = {
  objects: Array<App_Project_Type_Insert_Input>;
  on_conflict?: InputMaybe<App_Project_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Project_Type_OneArgs = {
  object: App_Project_Type_Insert_Input;
  on_conflict?: InputMaybe<App_Project_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_TerritoryArgs = {
  objects: Array<App_Territory_Insert_Input>;
  on_conflict?: InputMaybe<App_Territory_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Territory_OneArgs = {
  object: App_Territory_Insert_Input;
  on_conflict?: InputMaybe<App_Territory_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_ThemeArgs = {
  objects: Array<App_Theme_Insert_Input>;
  on_conflict?: InputMaybe<App_Theme_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Theme_OneArgs = {
  object: App_Theme_Insert_Input;
  on_conflict?: InputMaybe<App_Theme_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_TypeArgs = {
  objects: Array<App_Type_Insert_Input>;
  on_conflict?: InputMaybe<App_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_App_Type_OneArgs = {
  object: App_Type_Insert_Input;
  on_conflict?: InputMaybe<App_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Keycloak_GroupArgs = {
  objects: Array<Keycloak_Group_Insert_Input>;
  on_conflict?: InputMaybe<Keycloak_Group_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Keycloak_Group_OneArgs = {
  object: Keycloak_Group_Insert_Input;
  on_conflict?: InputMaybe<Keycloak_Group_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_TestArgs = {
  objects: Array<Test_Insert_Input>;
  on_conflict?: InputMaybe<Test_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Test_OneArgs = {
  object: Test_Insert_Input;
  on_conflict?: InputMaybe<Test_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_EntityArgs = {
  objects: Array<User_Entity_Insert_Input>;
  on_conflict?: InputMaybe<User_Entity_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_Entity_OneArgs = {
  object: User_Entity_Insert_Input;
  on_conflict?: InputMaybe<User_Entity_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_Group_MembershipArgs = {
  objects: Array<User_Group_Membership_Insert_Input>;
  on_conflict?: InputMaybe<User_Group_Membership_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_Group_Membership_OneArgs = {
  object: User_Group_Membership_Insert_Input;
  on_conflict?: InputMaybe<User_Group_Membership_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_App_FinancingArgs = {
  _set?: InputMaybe<App_Financing_Set_Input>;
  where: App_Financing_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Financing_By_PkArgs = {
  _set?: InputMaybe<App_Financing_Set_Input>;
  pk_columns: App_Financing_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Financing_ManyArgs = {
  updates: Array<App_Financing_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_ProjectArgs = {
  _inc?: InputMaybe<App_Project_Inc_Input>;
  _set?: InputMaybe<App_Project_Set_Input>;
  where: App_Project_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_By_PkArgs = {
  _inc?: InputMaybe<App_Project_Inc_Input>;
  _set?: InputMaybe<App_Project_Set_Input>;
  pk_columns: App_Project_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_FileArgs = {
  _set?: InputMaybe<App_Project_File_Set_Input>;
  where: App_Project_File_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_File_By_PkArgs = {
  _set?: InputMaybe<App_Project_File_Set_Input>;
  pk_columns: App_Project_File_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_File_ManyArgs = {
  updates: Array<App_Project_File_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_FinancingArgs = {
  _set?: InputMaybe<App_Project_Financing_Set_Input>;
  where: App_Project_Financing_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_Financing_By_PkArgs = {
  _set?: InputMaybe<App_Project_Financing_Set_Input>;
  pk_columns: App_Project_Financing_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_Financing_ManyArgs = {
  updates: Array<App_Project_Financing_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_ManyArgs = {
  updates: Array<App_Project_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_TerritoryArgs = {
  _set?: InputMaybe<App_Project_Territory_Set_Input>;
  where: App_Project_Territory_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_Territory_By_PkArgs = {
  _set?: InputMaybe<App_Project_Territory_Set_Input>;
  pk_columns: App_Project_Territory_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_Territory_ManyArgs = {
  updates: Array<App_Project_Territory_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_ThemeArgs = {
  _set?: InputMaybe<App_Project_Theme_Set_Input>;
  where: App_Project_Theme_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_Theme_By_PkArgs = {
  _set?: InputMaybe<App_Project_Theme_Set_Input>;
  pk_columns: App_Project_Theme_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_Theme_ManyArgs = {
  updates: Array<App_Project_Theme_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_TypeArgs = {
  _set?: InputMaybe<App_Project_Type_Set_Input>;
  where: App_Project_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_Type_By_PkArgs = {
  _set?: InputMaybe<App_Project_Type_Set_Input>;
  pk_columns: App_Project_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Project_Type_ManyArgs = {
  updates: Array<App_Project_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_TerritoryArgs = {
  _set?: InputMaybe<App_Territory_Set_Input>;
  where: App_Territory_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Territory_By_PkArgs = {
  _set?: InputMaybe<App_Territory_Set_Input>;
  pk_columns: App_Territory_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Territory_ManyArgs = {
  updates: Array<App_Territory_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_ThemeArgs = {
  _set?: InputMaybe<App_Theme_Set_Input>;
  where: App_Theme_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Theme_By_PkArgs = {
  _set?: InputMaybe<App_Theme_Set_Input>;
  pk_columns: App_Theme_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Theme_ManyArgs = {
  updates: Array<App_Theme_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_App_TypeArgs = {
  _set?: InputMaybe<App_Type_Set_Input>;
  where: App_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_App_Type_By_PkArgs = {
  _set?: InputMaybe<App_Type_Set_Input>;
  pk_columns: App_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_App_Type_ManyArgs = {
  updates: Array<App_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Keycloak_GroupArgs = {
  _set?: InputMaybe<Keycloak_Group_Set_Input>;
  where: Keycloak_Group_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Keycloak_Group_By_PkArgs = {
  _set?: InputMaybe<Keycloak_Group_Set_Input>;
  pk_columns: Keycloak_Group_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Keycloak_Group_ManyArgs = {
  updates: Array<Keycloak_Group_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_TestArgs = {
  _set?: InputMaybe<Test_Set_Input>;
  where: Test_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Test_By_PkArgs = {
  _set?: InputMaybe<Test_Set_Input>;
  pk_columns: Test_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Test_ManyArgs = {
  updates: Array<Test_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_User_EntityArgs = {
  _inc?: InputMaybe<User_Entity_Inc_Input>;
  _set?: InputMaybe<User_Entity_Set_Input>;
  where: User_Entity_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_User_Entity_By_PkArgs = {
  _inc?: InputMaybe<User_Entity_Inc_Input>;
  _set?: InputMaybe<User_Entity_Set_Input>;
  pk_columns: User_Entity_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_User_Entity_ManyArgs = {
  updates: Array<User_Entity_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_User_Group_MembershipArgs = {
  _set?: InputMaybe<User_Group_Membership_Set_Input>;
  where: User_Group_Membership_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_User_Group_Membership_By_PkArgs = {
  _set?: InputMaybe<User_Group_Membership_Set_Input>;
  pk_columns: User_Group_Membership_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_User_Group_Membership_ManyArgs = {
  updates: Array<User_Group_Membership_Updates>;
};

/** Boolean expression to compare columns of type "numeric". All fields are combined with logical 'AND'. */
export type Numeric_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['numeric']>;
  _gt?: InputMaybe<Scalars['numeric']>;
  _gte?: InputMaybe<Scalars['numeric']>;
  _in?: InputMaybe<Array<Scalars['numeric']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['numeric']>;
  _lte?: InputMaybe<Scalars['numeric']>;
  _neq?: InputMaybe<Scalars['numeric']>;
  _nin?: InputMaybe<Array<Scalars['numeric']>>;
};

/** column ordering options */
export enum Order_By {
  /** in ascending order, nulls last */
  Asc = 'asc',
  /** in ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in descending order, nulls first */
  Desc = 'desc',
  /** in descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "app.financing" */
  app_financing: Array<App_Financing>;
  /** fetch aggregated fields from the table: "app.financing" */
  app_financing_aggregate: App_Financing_Aggregate;
  /** fetch data from the table: "app.financing" using primary key columns */
  app_financing_by_pk?: Maybe<App_Financing>;
  /** fetch data from the table: "app.project" */
  app_project: Array<App_Project>;
  /** fetch aggregated fields from the table: "app.project" */
  app_project_aggregate: App_Project_Aggregate;
  /** fetch data from the table: "app.project" using primary key columns */
  app_project_by_pk?: Maybe<App_Project>;
  /** fetch data from the table: "app.project_file" */
  app_project_file: Array<App_Project_File>;
  /** fetch aggregated fields from the table: "app.project_file" */
  app_project_file_aggregate: App_Project_File_Aggregate;
  /** fetch data from the table: "app.project_file" using primary key columns */
  app_project_file_by_pk?: Maybe<App_Project_File>;
  /** fetch data from the table: "app.project_financing" */
  app_project_financing: Array<App_Project_Financing>;
  /** fetch aggregated fields from the table: "app.project_financing" */
  app_project_financing_aggregate: App_Project_Financing_Aggregate;
  /** fetch data from the table: "app.project_financing" using primary key columns */
  app_project_financing_by_pk?: Maybe<App_Project_Financing>;
  /** fetch data from the table: "app.project_territory" */
  app_project_territory: Array<App_Project_Territory>;
  /** fetch aggregated fields from the table: "app.project_territory" */
  app_project_territory_aggregate: App_Project_Territory_Aggregate;
  /** fetch data from the table: "app.project_territory" using primary key columns */
  app_project_territory_by_pk?: Maybe<App_Project_Territory>;
  /** fetch data from the table: "app.project_theme" */
  app_project_theme: Array<App_Project_Theme>;
  /** fetch aggregated fields from the table: "app.project_theme" */
  app_project_theme_aggregate: App_Project_Theme_Aggregate;
  /** fetch data from the table: "app.project_theme" using primary key columns */
  app_project_theme_by_pk?: Maybe<App_Project_Theme>;
  /** fetch data from the table: "app.project_type" */
  app_project_type: Array<App_Project_Type>;
  /** fetch aggregated fields from the table: "app.project_type" */
  app_project_type_aggregate: App_Project_Type_Aggregate;
  /** fetch data from the table: "app.project_type" using primary key columns */
  app_project_type_by_pk?: Maybe<App_Project_Type>;
  /** fetch data from the table: "app.territory" */
  app_territory: Array<App_Territory>;
  /** fetch aggregated fields from the table: "app.territory" */
  app_territory_aggregate: App_Territory_Aggregate;
  /** fetch data from the table: "app.territory" using primary key columns */
  app_territory_by_pk?: Maybe<App_Territory>;
  /** fetch data from the table: "app.theme" */
  app_theme: Array<App_Theme>;
  /** fetch aggregated fields from the table: "app.theme" */
  app_theme_aggregate: App_Theme_Aggregate;
  /** fetch data from the table: "app.theme" using primary key columns */
  app_theme_by_pk?: Maybe<App_Theme>;
  /** fetch data from the table: "app.type" */
  app_type: Array<App_Type>;
  /** fetch aggregated fields from the table: "app.type" */
  app_type_aggregate: App_Type_Aggregate;
  /** fetch data from the table: "app.type" using primary key columns */
  app_type_by_pk?: Maybe<App_Type>;
  /** fetch data from the table: "keycloak_group" */
  keycloak_group: Array<Keycloak_Group>;
  /** fetch aggregated fields from the table: "keycloak_group" */
  keycloak_group_aggregate: Keycloak_Group_Aggregate;
  /** fetch data from the table: "keycloak_group" using primary key columns */
  keycloak_group_by_pk?: Maybe<Keycloak_Group>;
  /** fetch data from the table: "test" */
  test: Array<Test>;
  /** fetch aggregated fields from the table: "test" */
  test_aggregate: Test_Aggregate;
  /** fetch data from the table: "test" using primary key columns */
  test_by_pk?: Maybe<Test>;
  /** fetch data from the table: "user_entity" */
  user_entity: Array<User_Entity>;
  /** fetch aggregated fields from the table: "user_entity" */
  user_entity_aggregate: User_Entity_Aggregate;
  /** fetch data from the table: "user_entity" using primary key columns */
  user_entity_by_pk?: Maybe<User_Entity>;
  /** fetch data from the table: "user_group_membership" */
  user_group_membership: Array<User_Group_Membership>;
  /** fetch aggregated fields from the table: "user_group_membership" */
  user_group_membership_aggregate: User_Group_Membership_Aggregate;
  /** fetch data from the table: "user_group_membership" using primary key columns */
  user_group_membership_by_pk?: Maybe<User_Group_Membership>;
};


export type Query_RootApp_FinancingArgs = {
  distinct_on?: InputMaybe<Array<App_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Financing_Order_By>>;
  where?: InputMaybe<App_Financing_Bool_Exp>;
};


export type Query_RootApp_Financing_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Financing_Order_By>>;
  where?: InputMaybe<App_Financing_Bool_Exp>;
};


export type Query_RootApp_Financing_By_PkArgs = {
  id: Scalars['String'];
};


export type Query_RootApp_ProjectArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Order_By>>;
  where?: InputMaybe<App_Project_Bool_Exp>;
};


export type Query_RootApp_Project_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Order_By>>;
  where?: InputMaybe<App_Project_Bool_Exp>;
};


export type Query_RootApp_Project_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootApp_Project_FileArgs = {
  distinct_on?: InputMaybe<Array<App_Project_File_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_File_Order_By>>;
  where?: InputMaybe<App_Project_File_Bool_Exp>;
};


export type Query_RootApp_Project_File_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_File_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_File_Order_By>>;
  where?: InputMaybe<App_Project_File_Bool_Exp>;
};


export type Query_RootApp_Project_File_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootApp_Project_FinancingArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Financing_Order_By>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};


export type Query_RootApp_Project_Financing_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Financing_Order_By>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};


export type Query_RootApp_Project_Financing_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootApp_Project_TerritoryArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Territory_Order_By>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};


export type Query_RootApp_Project_Territory_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Territory_Order_By>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};


export type Query_RootApp_Project_Territory_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootApp_Project_ThemeArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Theme_Order_By>>;
  where?: InputMaybe<App_Project_Theme_Bool_Exp>;
};


export type Query_RootApp_Project_Theme_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Theme_Order_By>>;
  where?: InputMaybe<App_Project_Theme_Bool_Exp>;
};


export type Query_RootApp_Project_Theme_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootApp_Project_TypeArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Type_Order_By>>;
  where?: InputMaybe<App_Project_Type_Bool_Exp>;
};


export type Query_RootApp_Project_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Type_Order_By>>;
  where?: InputMaybe<App_Project_Type_Bool_Exp>;
};


export type Query_RootApp_Project_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootApp_TerritoryArgs = {
  distinct_on?: InputMaybe<Array<App_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Territory_Order_By>>;
  where?: InputMaybe<App_Territory_Bool_Exp>;
};


export type Query_RootApp_Territory_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Territory_Order_By>>;
  where?: InputMaybe<App_Territory_Bool_Exp>;
};


export type Query_RootApp_Territory_By_PkArgs = {
  id: Scalars['String'];
};


export type Query_RootApp_ThemeArgs = {
  distinct_on?: InputMaybe<Array<App_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Theme_Order_By>>;
  where?: InputMaybe<App_Theme_Bool_Exp>;
};


export type Query_RootApp_Theme_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Theme_Order_By>>;
  where?: InputMaybe<App_Theme_Bool_Exp>;
};


export type Query_RootApp_Theme_By_PkArgs = {
  id: Scalars['String'];
};


export type Query_RootApp_TypeArgs = {
  distinct_on?: InputMaybe<Array<App_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Type_Order_By>>;
  where?: InputMaybe<App_Type_Bool_Exp>;
};


export type Query_RootApp_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Type_Order_By>>;
  where?: InputMaybe<App_Type_Bool_Exp>;
};


export type Query_RootApp_Type_By_PkArgs = {
  id: Scalars['String'];
};


export type Query_RootKeycloak_GroupArgs = {
  distinct_on?: InputMaybe<Array<Keycloak_Group_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Keycloak_Group_Order_By>>;
  where?: InputMaybe<Keycloak_Group_Bool_Exp>;
};


export type Query_RootKeycloak_Group_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Keycloak_Group_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Keycloak_Group_Order_By>>;
  where?: InputMaybe<Keycloak_Group_Bool_Exp>;
};


export type Query_RootKeycloak_Group_By_PkArgs = {
  id: Scalars['String'];
};


export type Query_RootTestArgs = {
  distinct_on?: InputMaybe<Array<Test_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Test_Order_By>>;
  where?: InputMaybe<Test_Bool_Exp>;
};


export type Query_RootTest_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Test_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Test_Order_By>>;
  where?: InputMaybe<Test_Bool_Exp>;
};


export type Query_RootTest_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootUser_EntityArgs = {
  distinct_on?: InputMaybe<Array<User_Entity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Entity_Order_By>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Query_RootUser_Entity_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Entity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Entity_Order_By>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Query_RootUser_Entity_By_PkArgs = {
  id: Scalars['String'];
};


export type Query_RootUser_Group_MembershipArgs = {
  distinct_on?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Group_Membership_Order_By>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};


export type Query_RootUser_Group_Membership_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Group_Membership_Order_By>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};


export type Query_RootUser_Group_Membership_By_PkArgs = {
  group_id: Scalars['String'];
  user_id: Scalars['String'];
};

export type St_D_Within_Geography_Input = {
  distance: Scalars['Float'];
  from: Scalars['geography'];
  use_spheroid?: InputMaybe<Scalars['Boolean']>;
};

export type St_D_Within_Input = {
  distance: Scalars['Float'];
  from: Scalars['geometry'];
};

export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "app.financing" */
  app_financing: Array<App_Financing>;
  /** fetch aggregated fields from the table: "app.financing" */
  app_financing_aggregate: App_Financing_Aggregate;
  /** fetch data from the table: "app.financing" using primary key columns */
  app_financing_by_pk?: Maybe<App_Financing>;
  /** fetch data from the table in a streaming manner : "app.financing" */
  app_financing_stream: Array<App_Financing>;
  /** fetch data from the table: "app.project" */
  app_project: Array<App_Project>;
  /** fetch aggregated fields from the table: "app.project" */
  app_project_aggregate: App_Project_Aggregate;
  /** fetch data from the table: "app.project" using primary key columns */
  app_project_by_pk?: Maybe<App_Project>;
  /** fetch data from the table: "app.project_file" */
  app_project_file: Array<App_Project_File>;
  /** fetch aggregated fields from the table: "app.project_file" */
  app_project_file_aggregate: App_Project_File_Aggregate;
  /** fetch data from the table: "app.project_file" using primary key columns */
  app_project_file_by_pk?: Maybe<App_Project_File>;
  /** fetch data from the table in a streaming manner : "app.project_file" */
  app_project_file_stream: Array<App_Project_File>;
  /** fetch data from the table: "app.project_financing" */
  app_project_financing: Array<App_Project_Financing>;
  /** fetch aggregated fields from the table: "app.project_financing" */
  app_project_financing_aggregate: App_Project_Financing_Aggregate;
  /** fetch data from the table: "app.project_financing" using primary key columns */
  app_project_financing_by_pk?: Maybe<App_Project_Financing>;
  /** fetch data from the table in a streaming manner : "app.project_financing" */
  app_project_financing_stream: Array<App_Project_Financing>;
  /** fetch data from the table in a streaming manner : "app.project" */
  app_project_stream: Array<App_Project>;
  /** fetch data from the table: "app.project_territory" */
  app_project_territory: Array<App_Project_Territory>;
  /** fetch aggregated fields from the table: "app.project_territory" */
  app_project_territory_aggregate: App_Project_Territory_Aggregate;
  /** fetch data from the table: "app.project_territory" using primary key columns */
  app_project_territory_by_pk?: Maybe<App_Project_Territory>;
  /** fetch data from the table in a streaming manner : "app.project_territory" */
  app_project_territory_stream: Array<App_Project_Territory>;
  /** fetch data from the table: "app.project_theme" */
  app_project_theme: Array<App_Project_Theme>;
  /** fetch aggregated fields from the table: "app.project_theme" */
  app_project_theme_aggregate: App_Project_Theme_Aggregate;
  /** fetch data from the table: "app.project_theme" using primary key columns */
  app_project_theme_by_pk?: Maybe<App_Project_Theme>;
  /** fetch data from the table in a streaming manner : "app.project_theme" */
  app_project_theme_stream: Array<App_Project_Theme>;
  /** fetch data from the table: "app.project_type" */
  app_project_type: Array<App_Project_Type>;
  /** fetch aggregated fields from the table: "app.project_type" */
  app_project_type_aggregate: App_Project_Type_Aggregate;
  /** fetch data from the table: "app.project_type" using primary key columns */
  app_project_type_by_pk?: Maybe<App_Project_Type>;
  /** fetch data from the table in a streaming manner : "app.project_type" */
  app_project_type_stream: Array<App_Project_Type>;
  /** fetch data from the table: "app.territory" */
  app_territory: Array<App_Territory>;
  /** fetch aggregated fields from the table: "app.territory" */
  app_territory_aggregate: App_Territory_Aggregate;
  /** fetch data from the table: "app.territory" using primary key columns */
  app_territory_by_pk?: Maybe<App_Territory>;
  /** fetch data from the table in a streaming manner : "app.territory" */
  app_territory_stream: Array<App_Territory>;
  /** fetch data from the table: "app.theme" */
  app_theme: Array<App_Theme>;
  /** fetch aggregated fields from the table: "app.theme" */
  app_theme_aggregate: App_Theme_Aggregate;
  /** fetch data from the table: "app.theme" using primary key columns */
  app_theme_by_pk?: Maybe<App_Theme>;
  /** fetch data from the table in a streaming manner : "app.theme" */
  app_theme_stream: Array<App_Theme>;
  /** fetch data from the table: "app.type" */
  app_type: Array<App_Type>;
  /** fetch aggregated fields from the table: "app.type" */
  app_type_aggregate: App_Type_Aggregate;
  /** fetch data from the table: "app.type" using primary key columns */
  app_type_by_pk?: Maybe<App_Type>;
  /** fetch data from the table in a streaming manner : "app.type" */
  app_type_stream: Array<App_Type>;
  /** fetch data from the table: "keycloak_group" */
  keycloak_group: Array<Keycloak_Group>;
  /** fetch aggregated fields from the table: "keycloak_group" */
  keycloak_group_aggregate: Keycloak_Group_Aggregate;
  /** fetch data from the table: "keycloak_group" using primary key columns */
  keycloak_group_by_pk?: Maybe<Keycloak_Group>;
  /** fetch data from the table in a streaming manner : "keycloak_group" */
  keycloak_group_stream: Array<Keycloak_Group>;
  /** fetch data from the table: "test" */
  test: Array<Test>;
  /** fetch aggregated fields from the table: "test" */
  test_aggregate: Test_Aggregate;
  /** fetch data from the table: "test" using primary key columns */
  test_by_pk?: Maybe<Test>;
  /** fetch data from the table in a streaming manner : "test" */
  test_stream: Array<Test>;
  /** fetch data from the table: "user_entity" */
  user_entity: Array<User_Entity>;
  /** fetch aggregated fields from the table: "user_entity" */
  user_entity_aggregate: User_Entity_Aggregate;
  /** fetch data from the table: "user_entity" using primary key columns */
  user_entity_by_pk?: Maybe<User_Entity>;
  /** fetch data from the table in a streaming manner : "user_entity" */
  user_entity_stream: Array<User_Entity>;
  /** fetch data from the table: "user_group_membership" */
  user_group_membership: Array<User_Group_Membership>;
  /** fetch aggregated fields from the table: "user_group_membership" */
  user_group_membership_aggregate: User_Group_Membership_Aggregate;
  /** fetch data from the table: "user_group_membership" using primary key columns */
  user_group_membership_by_pk?: Maybe<User_Group_Membership>;
  /** fetch data from the table in a streaming manner : "user_group_membership" */
  user_group_membership_stream: Array<User_Group_Membership>;
};


export type Subscription_RootApp_FinancingArgs = {
  distinct_on?: InputMaybe<Array<App_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Financing_Order_By>>;
  where?: InputMaybe<App_Financing_Bool_Exp>;
};


export type Subscription_RootApp_Financing_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Financing_Order_By>>;
  where?: InputMaybe<App_Financing_Bool_Exp>;
};


export type Subscription_RootApp_Financing_By_PkArgs = {
  id: Scalars['String'];
};


export type Subscription_RootApp_Financing_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Financing_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Financing_Bool_Exp>;
};


export type Subscription_RootApp_ProjectArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Order_By>>;
  where?: InputMaybe<App_Project_Bool_Exp>;
};


export type Subscription_RootApp_Project_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Order_By>>;
  where?: InputMaybe<App_Project_Bool_Exp>;
};


export type Subscription_RootApp_Project_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootApp_Project_FileArgs = {
  distinct_on?: InputMaybe<Array<App_Project_File_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_File_Order_By>>;
  where?: InputMaybe<App_Project_File_Bool_Exp>;
};


export type Subscription_RootApp_Project_File_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_File_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_File_Order_By>>;
  where?: InputMaybe<App_Project_File_Bool_Exp>;
};


export type Subscription_RootApp_Project_File_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootApp_Project_File_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Project_File_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Project_File_Bool_Exp>;
};


export type Subscription_RootApp_Project_FinancingArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Financing_Order_By>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};


export type Subscription_RootApp_Project_Financing_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Financing_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Financing_Order_By>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};


export type Subscription_RootApp_Project_Financing_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootApp_Project_Financing_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Project_Financing_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Project_Financing_Bool_Exp>;
};


export type Subscription_RootApp_Project_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Project_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Project_Bool_Exp>;
};


export type Subscription_RootApp_Project_TerritoryArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Territory_Order_By>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};


export type Subscription_RootApp_Project_Territory_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Territory_Order_By>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};


export type Subscription_RootApp_Project_Territory_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootApp_Project_Territory_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Project_Territory_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Project_Territory_Bool_Exp>;
};


export type Subscription_RootApp_Project_ThemeArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Theme_Order_By>>;
  where?: InputMaybe<App_Project_Theme_Bool_Exp>;
};


export type Subscription_RootApp_Project_Theme_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Theme_Order_By>>;
  where?: InputMaybe<App_Project_Theme_Bool_Exp>;
};


export type Subscription_RootApp_Project_Theme_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootApp_Project_Theme_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Project_Theme_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Project_Theme_Bool_Exp>;
};


export type Subscription_RootApp_Project_TypeArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Type_Order_By>>;
  where?: InputMaybe<App_Project_Type_Bool_Exp>;
};


export type Subscription_RootApp_Project_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Project_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Project_Type_Order_By>>;
  where?: InputMaybe<App_Project_Type_Bool_Exp>;
};


export type Subscription_RootApp_Project_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootApp_Project_Type_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Project_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Project_Type_Bool_Exp>;
};


export type Subscription_RootApp_TerritoryArgs = {
  distinct_on?: InputMaybe<Array<App_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Territory_Order_By>>;
  where?: InputMaybe<App_Territory_Bool_Exp>;
};


export type Subscription_RootApp_Territory_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Territory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Territory_Order_By>>;
  where?: InputMaybe<App_Territory_Bool_Exp>;
};


export type Subscription_RootApp_Territory_By_PkArgs = {
  id: Scalars['String'];
};


export type Subscription_RootApp_Territory_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Territory_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Territory_Bool_Exp>;
};


export type Subscription_RootApp_ThemeArgs = {
  distinct_on?: InputMaybe<Array<App_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Theme_Order_By>>;
  where?: InputMaybe<App_Theme_Bool_Exp>;
};


export type Subscription_RootApp_Theme_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Theme_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Theme_Order_By>>;
  where?: InputMaybe<App_Theme_Bool_Exp>;
};


export type Subscription_RootApp_Theme_By_PkArgs = {
  id: Scalars['String'];
};


export type Subscription_RootApp_Theme_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Theme_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Theme_Bool_Exp>;
};


export type Subscription_RootApp_TypeArgs = {
  distinct_on?: InputMaybe<Array<App_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Type_Order_By>>;
  where?: InputMaybe<App_Type_Bool_Exp>;
};


export type Subscription_RootApp_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<App_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<App_Type_Order_By>>;
  where?: InputMaybe<App_Type_Bool_Exp>;
};


export type Subscription_RootApp_Type_By_PkArgs = {
  id: Scalars['String'];
};


export type Subscription_RootApp_Type_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<App_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<App_Type_Bool_Exp>;
};


export type Subscription_RootKeycloak_GroupArgs = {
  distinct_on?: InputMaybe<Array<Keycloak_Group_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Keycloak_Group_Order_By>>;
  where?: InputMaybe<Keycloak_Group_Bool_Exp>;
};


export type Subscription_RootKeycloak_Group_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Keycloak_Group_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Keycloak_Group_Order_By>>;
  where?: InputMaybe<Keycloak_Group_Bool_Exp>;
};


export type Subscription_RootKeycloak_Group_By_PkArgs = {
  id: Scalars['String'];
};


export type Subscription_RootKeycloak_Group_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Keycloak_Group_Stream_Cursor_Input>>;
  where?: InputMaybe<Keycloak_Group_Bool_Exp>;
};


export type Subscription_RootTestArgs = {
  distinct_on?: InputMaybe<Array<Test_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Test_Order_By>>;
  where?: InputMaybe<Test_Bool_Exp>;
};


export type Subscription_RootTest_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Test_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Test_Order_By>>;
  where?: InputMaybe<Test_Bool_Exp>;
};


export type Subscription_RootTest_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootTest_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Test_Stream_Cursor_Input>>;
  where?: InputMaybe<Test_Bool_Exp>;
};


export type Subscription_RootUser_EntityArgs = {
  distinct_on?: InputMaybe<Array<User_Entity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Entity_Order_By>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Subscription_RootUser_Entity_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Entity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Entity_Order_By>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Subscription_RootUser_Entity_By_PkArgs = {
  id: Scalars['String'];
};


export type Subscription_RootUser_Entity_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<User_Entity_Stream_Cursor_Input>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Subscription_RootUser_Group_MembershipArgs = {
  distinct_on?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Group_Membership_Order_By>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};


export type Subscription_RootUser_Group_Membership_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Group_Membership_Order_By>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};


export type Subscription_RootUser_Group_Membership_By_PkArgs = {
  group_id: Scalars['String'];
  user_id: Scalars['String'];
};


export type Subscription_RootUser_Group_Membership_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<User_Group_Membership_Stream_Cursor_Input>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};

/** columns and relationships of "test" */
export type Test = {
  __typename?: 'test';
  id: Scalars['uuid'];
};

/** aggregated selection of "test" */
export type Test_Aggregate = {
  __typename?: 'test_aggregate';
  aggregate?: Maybe<Test_Aggregate_Fields>;
  nodes: Array<Test>;
};

/** aggregate fields of "test" */
export type Test_Aggregate_Fields = {
  __typename?: 'test_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Test_Max_Fields>;
  min?: Maybe<Test_Min_Fields>;
};


/** aggregate fields of "test" */
export type Test_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Test_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "test". All fields are combined with a logical 'AND'. */
export type Test_Bool_Exp = {
  _and?: InputMaybe<Array<Test_Bool_Exp>>;
  _not?: InputMaybe<Test_Bool_Exp>;
  _or?: InputMaybe<Array<Test_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "test" */
export enum Test_Constraint {
  /** unique or primary key constraint on columns "id" */
  TestPkey = 'test_pkey'
}

/** input type for inserting data into table "test" */
export type Test_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Test_Max_Fields = {
  __typename?: 'test_max_fields';
  id?: Maybe<Scalars['uuid']>;
};

/** aggregate min on columns */
export type Test_Min_Fields = {
  __typename?: 'test_min_fields';
  id?: Maybe<Scalars['uuid']>;
};

/** response of any mutation on the table "test" */
export type Test_Mutation_Response = {
  __typename?: 'test_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Test>;
};

/** on_conflict condition type for table "test" */
export type Test_On_Conflict = {
  constraint: Test_Constraint;
  update_columns?: Array<Test_Update_Column>;
  where?: InputMaybe<Test_Bool_Exp>;
};

/** Ordering options when selecting data from "test". */
export type Test_Order_By = {
  id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: test */
export type Test_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "test" */
export enum Test_Select_Column {
  /** column name */
  Id = 'id'
}

/** input type for updating data in table "test" */
export type Test_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "test" */
export type Test_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Test_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Test_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "test" */
export enum Test_Update_Column {
  /** column name */
  Id = 'id'
}

export type Test_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Test_Set_Input>;
  where: Test_Bool_Exp;
};

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['timestamptz']>;
  _gt?: InputMaybe<Scalars['timestamptz']>;
  _gte?: InputMaybe<Scalars['timestamptz']>;
  _in?: InputMaybe<Array<Scalars['timestamptz']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['timestamptz']>;
  _lte?: InputMaybe<Scalars['timestamptz']>;
  _neq?: InputMaybe<Scalars['timestamptz']>;
  _nin?: InputMaybe<Array<Scalars['timestamptz']>>;
};

/** columns and relationships of "user_entity" */
export type User_Entity = {
  __typename?: 'user_entity';
  created_timestamp?: Maybe<Scalars['bigint']>;
  email?: Maybe<Scalars['String']>;
  email_constraint?: Maybe<Scalars['String']>;
  email_verified: Scalars['Boolean'];
  enabled: Scalars['Boolean'];
  federation_link?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  last_name?: Maybe<Scalars['String']>;
  not_before: Scalars['Int'];
  realm_id?: Maybe<Scalars['String']>;
  service_account_client_link?: Maybe<Scalars['String']>;
  /** An array relationship */
  user_group_memberships: Array<User_Group_Membership>;
  /** An aggregate relationship */
  user_group_memberships_aggregate: User_Group_Membership_Aggregate;
  username?: Maybe<Scalars['String']>;
};


/** columns and relationships of "user_entity" */
export type User_EntityUser_Group_MembershipsArgs = {
  distinct_on?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Group_Membership_Order_By>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};


/** columns and relationships of "user_entity" */
export type User_EntityUser_Group_Memberships_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Group_Membership_Order_By>>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};

/** aggregated selection of "user_entity" */
export type User_Entity_Aggregate = {
  __typename?: 'user_entity_aggregate';
  aggregate?: Maybe<User_Entity_Aggregate_Fields>;
  nodes: Array<User_Entity>;
};

/** aggregate fields of "user_entity" */
export type User_Entity_Aggregate_Fields = {
  __typename?: 'user_entity_aggregate_fields';
  avg?: Maybe<User_Entity_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<User_Entity_Max_Fields>;
  min?: Maybe<User_Entity_Min_Fields>;
  stddev?: Maybe<User_Entity_Stddev_Fields>;
  stddev_pop?: Maybe<User_Entity_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<User_Entity_Stddev_Samp_Fields>;
  sum?: Maybe<User_Entity_Sum_Fields>;
  var_pop?: Maybe<User_Entity_Var_Pop_Fields>;
  var_samp?: Maybe<User_Entity_Var_Samp_Fields>;
  variance?: Maybe<User_Entity_Variance_Fields>;
};


/** aggregate fields of "user_entity" */
export type User_Entity_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<User_Entity_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type User_Entity_Avg_Fields = {
  __typename?: 'user_entity_avg_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "user_entity". All fields are combined with a logical 'AND'. */
export type User_Entity_Bool_Exp = {
  _and?: InputMaybe<Array<User_Entity_Bool_Exp>>;
  _not?: InputMaybe<User_Entity_Bool_Exp>;
  _or?: InputMaybe<Array<User_Entity_Bool_Exp>>;
  created_timestamp?: InputMaybe<Bigint_Comparison_Exp>;
  email?: InputMaybe<String_Comparison_Exp>;
  email_constraint?: InputMaybe<String_Comparison_Exp>;
  email_verified?: InputMaybe<Boolean_Comparison_Exp>;
  enabled?: InputMaybe<Boolean_Comparison_Exp>;
  federation_link?: InputMaybe<String_Comparison_Exp>;
  first_name?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  last_name?: InputMaybe<String_Comparison_Exp>;
  not_before?: InputMaybe<Int_Comparison_Exp>;
  realm_id?: InputMaybe<String_Comparison_Exp>;
  service_account_client_link?: InputMaybe<String_Comparison_Exp>;
  user_group_memberships?: InputMaybe<User_Group_Membership_Bool_Exp>;
  username?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "user_entity" */
export enum User_Entity_Constraint {
  /** unique or primary key constraint on columns "id" */
  ConstraintFb = 'constraint_fb',
  /** unique or primary key constraint on columns "realm_id", "email_constraint" */
  UkDykn684sl8up1crfei6eckhd7 = 'uk_dykn684sl8up1crfei6eckhd7',
  /** unique or primary key constraint on columns "realm_id", "username" */
  UkRu8tt6t700s9v50bu18ws5ha6 = 'uk_ru8tt6t700s9v50bu18ws5ha6'
}

/** input type for incrementing numeric columns in table "user_entity" */
export type User_Entity_Inc_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']>;
  not_before?: InputMaybe<Scalars['Int']>;
};

/** input type for inserting data into table "user_entity" */
export type User_Entity_Insert_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']>;
  email?: InputMaybe<Scalars['String']>;
  email_constraint?: InputMaybe<Scalars['String']>;
  email_verified?: InputMaybe<Scalars['Boolean']>;
  enabled?: InputMaybe<Scalars['Boolean']>;
  federation_link?: InputMaybe<Scalars['String']>;
  first_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  not_before?: InputMaybe<Scalars['Int']>;
  realm_id?: InputMaybe<Scalars['String']>;
  service_account_client_link?: InputMaybe<Scalars['String']>;
  user_group_memberships?: InputMaybe<User_Group_Membership_Arr_Rel_Insert_Input>;
  username?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type User_Entity_Max_Fields = {
  __typename?: 'user_entity_max_fields';
  created_timestamp?: Maybe<Scalars['bigint']>;
  email?: Maybe<Scalars['String']>;
  email_constraint?: Maybe<Scalars['String']>;
  federation_link?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  last_name?: Maybe<Scalars['String']>;
  not_before?: Maybe<Scalars['Int']>;
  realm_id?: Maybe<Scalars['String']>;
  service_account_client_link?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type User_Entity_Min_Fields = {
  __typename?: 'user_entity_min_fields';
  created_timestamp?: Maybe<Scalars['bigint']>;
  email?: Maybe<Scalars['String']>;
  email_constraint?: Maybe<Scalars['String']>;
  federation_link?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  last_name?: Maybe<Scalars['String']>;
  not_before?: Maybe<Scalars['Int']>;
  realm_id?: Maybe<Scalars['String']>;
  service_account_client_link?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "user_entity" */
export type User_Entity_Mutation_Response = {
  __typename?: 'user_entity_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<User_Entity>;
};

/** input type for inserting object relation for remote table "user_entity" */
export type User_Entity_Obj_Rel_Insert_Input = {
  data: User_Entity_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<User_Entity_On_Conflict>;
};

/** on_conflict condition type for table "user_entity" */
export type User_Entity_On_Conflict = {
  constraint: User_Entity_Constraint;
  update_columns?: Array<User_Entity_Update_Column>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};

/** Ordering options when selecting data from "user_entity". */
export type User_Entity_Order_By = {
  created_timestamp?: InputMaybe<Order_By>;
  email?: InputMaybe<Order_By>;
  email_constraint?: InputMaybe<Order_By>;
  email_verified?: InputMaybe<Order_By>;
  enabled?: InputMaybe<Order_By>;
  federation_link?: InputMaybe<Order_By>;
  first_name?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  last_name?: InputMaybe<Order_By>;
  not_before?: InputMaybe<Order_By>;
  realm_id?: InputMaybe<Order_By>;
  service_account_client_link?: InputMaybe<Order_By>;
  user_group_memberships_aggregate?: InputMaybe<User_Group_Membership_Aggregate_Order_By>;
  username?: InputMaybe<Order_By>;
};

/** primary key columns input for table: user_entity */
export type User_Entity_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "user_entity" */
export enum User_Entity_Select_Column {
  /** column name */
  CreatedTimestamp = 'created_timestamp',
  /** column name */
  Email = 'email',
  /** column name */
  EmailConstraint = 'email_constraint',
  /** column name */
  EmailVerified = 'email_verified',
  /** column name */
  Enabled = 'enabled',
  /** column name */
  FederationLink = 'federation_link',
  /** column name */
  FirstName = 'first_name',
  /** column name */
  Id = 'id',
  /** column name */
  LastName = 'last_name',
  /** column name */
  NotBefore = 'not_before',
  /** column name */
  RealmId = 'realm_id',
  /** column name */
  ServiceAccountClientLink = 'service_account_client_link',
  /** column name */
  Username = 'username'
}

/** input type for updating data in table "user_entity" */
export type User_Entity_Set_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']>;
  email?: InputMaybe<Scalars['String']>;
  email_constraint?: InputMaybe<Scalars['String']>;
  email_verified?: InputMaybe<Scalars['Boolean']>;
  enabled?: InputMaybe<Scalars['Boolean']>;
  federation_link?: InputMaybe<Scalars['String']>;
  first_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  not_before?: InputMaybe<Scalars['Int']>;
  realm_id?: InputMaybe<Scalars['String']>;
  service_account_client_link?: InputMaybe<Scalars['String']>;
  username?: InputMaybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type User_Entity_Stddev_Fields = {
  __typename?: 'user_entity_stddev_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type User_Entity_Stddev_Pop_Fields = {
  __typename?: 'user_entity_stddev_pop_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type User_Entity_Stddev_Samp_Fields = {
  __typename?: 'user_entity_stddev_samp_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "user_entity" */
export type User_Entity_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: User_Entity_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type User_Entity_Stream_Cursor_Value_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']>;
  email?: InputMaybe<Scalars['String']>;
  email_constraint?: InputMaybe<Scalars['String']>;
  email_verified?: InputMaybe<Scalars['Boolean']>;
  enabled?: InputMaybe<Scalars['Boolean']>;
  federation_link?: InputMaybe<Scalars['String']>;
  first_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  not_before?: InputMaybe<Scalars['Int']>;
  realm_id?: InputMaybe<Scalars['String']>;
  service_account_client_link?: InputMaybe<Scalars['String']>;
  username?: InputMaybe<Scalars['String']>;
};

/** aggregate sum on columns */
export type User_Entity_Sum_Fields = {
  __typename?: 'user_entity_sum_fields';
  created_timestamp?: Maybe<Scalars['bigint']>;
  not_before?: Maybe<Scalars['Int']>;
};

/** update columns of table "user_entity" */
export enum User_Entity_Update_Column {
  /** column name */
  CreatedTimestamp = 'created_timestamp',
  /** column name */
  Email = 'email',
  /** column name */
  EmailConstraint = 'email_constraint',
  /** column name */
  EmailVerified = 'email_verified',
  /** column name */
  Enabled = 'enabled',
  /** column name */
  FederationLink = 'federation_link',
  /** column name */
  FirstName = 'first_name',
  /** column name */
  Id = 'id',
  /** column name */
  LastName = 'last_name',
  /** column name */
  NotBefore = 'not_before',
  /** column name */
  RealmId = 'realm_id',
  /** column name */
  ServiceAccountClientLink = 'service_account_client_link',
  /** column name */
  Username = 'username'
}

export type User_Entity_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<User_Entity_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<User_Entity_Set_Input>;
  where: User_Entity_Bool_Exp;
};

/** aggregate var_pop on columns */
export type User_Entity_Var_Pop_Fields = {
  __typename?: 'user_entity_var_pop_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type User_Entity_Var_Samp_Fields = {
  __typename?: 'user_entity_var_samp_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type User_Entity_Variance_Fields = {
  __typename?: 'user_entity_variance_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "user_group_membership" */
export type User_Group_Membership = {
  __typename?: 'user_group_membership';
  group_id: Scalars['String'];
  /** An object relationship */
  group_to_keycloak?: Maybe<Keycloak_Group>;
  /** An object relationship */
  user_entity: User_Entity;
  user_id: Scalars['String'];
};

/** aggregated selection of "user_group_membership" */
export type User_Group_Membership_Aggregate = {
  __typename?: 'user_group_membership_aggregate';
  aggregate?: Maybe<User_Group_Membership_Aggregate_Fields>;
  nodes: Array<User_Group_Membership>;
};

/** aggregate fields of "user_group_membership" */
export type User_Group_Membership_Aggregate_Fields = {
  __typename?: 'user_group_membership_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<User_Group_Membership_Max_Fields>;
  min?: Maybe<User_Group_Membership_Min_Fields>;
};


/** aggregate fields of "user_group_membership" */
export type User_Group_Membership_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<User_Group_Membership_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "user_group_membership" */
export type User_Group_Membership_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<User_Group_Membership_Max_Order_By>;
  min?: InputMaybe<User_Group_Membership_Min_Order_By>;
};

/** input type for inserting array relation for remote table "user_group_membership" */
export type User_Group_Membership_Arr_Rel_Insert_Input = {
  data: Array<User_Group_Membership_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<User_Group_Membership_On_Conflict>;
};

/** Boolean expression to filter rows from the table "user_group_membership". All fields are combined with a logical 'AND'. */
export type User_Group_Membership_Bool_Exp = {
  _and?: InputMaybe<Array<User_Group_Membership_Bool_Exp>>;
  _not?: InputMaybe<User_Group_Membership_Bool_Exp>;
  _or?: InputMaybe<Array<User_Group_Membership_Bool_Exp>>;
  group_id?: InputMaybe<String_Comparison_Exp>;
  group_to_keycloak?: InputMaybe<Keycloak_Group_Bool_Exp>;
  user_entity?: InputMaybe<User_Entity_Bool_Exp>;
  user_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "user_group_membership" */
export enum User_Group_Membership_Constraint {
  /** unique or primary key constraint on columns "group_id", "user_id" */
  ConstraintUserGroup = 'constraint_user_group'
}

/** input type for inserting data into table "user_group_membership" */
export type User_Group_Membership_Insert_Input = {
  group_id?: InputMaybe<Scalars['String']>;
  group_to_keycloak?: InputMaybe<Keycloak_Group_Obj_Rel_Insert_Input>;
  user_entity?: InputMaybe<User_Entity_Obj_Rel_Insert_Input>;
  user_id?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type User_Group_Membership_Max_Fields = {
  __typename?: 'user_group_membership_max_fields';
  group_id?: Maybe<Scalars['String']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "user_group_membership" */
export type User_Group_Membership_Max_Order_By = {
  group_id?: InputMaybe<Order_By>;
  user_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type User_Group_Membership_Min_Fields = {
  __typename?: 'user_group_membership_min_fields';
  group_id?: Maybe<Scalars['String']>;
  user_id?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "user_group_membership" */
export type User_Group_Membership_Min_Order_By = {
  group_id?: InputMaybe<Order_By>;
  user_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "user_group_membership" */
export type User_Group_Membership_Mutation_Response = {
  __typename?: 'user_group_membership_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<User_Group_Membership>;
};

/** on_conflict condition type for table "user_group_membership" */
export type User_Group_Membership_On_Conflict = {
  constraint: User_Group_Membership_Constraint;
  update_columns?: Array<User_Group_Membership_Update_Column>;
  where?: InputMaybe<User_Group_Membership_Bool_Exp>;
};

/** Ordering options when selecting data from "user_group_membership". */
export type User_Group_Membership_Order_By = {
  group_id?: InputMaybe<Order_By>;
  group_to_keycloak?: InputMaybe<Keycloak_Group_Order_By>;
  user_entity?: InputMaybe<User_Entity_Order_By>;
  user_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: user_group_membership */
export type User_Group_Membership_Pk_Columns_Input = {
  group_id: Scalars['String'];
  user_id: Scalars['String'];
};

/** select columns of table "user_group_membership" */
export enum User_Group_Membership_Select_Column {
  /** column name */
  GroupId = 'group_id',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "user_group_membership" */
export type User_Group_Membership_Set_Input = {
  group_id?: InputMaybe<Scalars['String']>;
  user_id?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "user_group_membership" */
export type User_Group_Membership_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: User_Group_Membership_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type User_Group_Membership_Stream_Cursor_Value_Input = {
  group_id?: InputMaybe<Scalars['String']>;
  user_id?: InputMaybe<Scalars['String']>;
};

/** update columns of table "user_group_membership" */
export enum User_Group_Membership_Update_Column {
  /** column name */
  GroupId = 'group_id',
  /** column name */
  UserId = 'user_id'
}

export type User_Group_Membership_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<User_Group_Membership_Set_Input>;
  where: User_Group_Membership_Bool_Exp;
};

/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['uuid']>;
  _gt?: InputMaybe<Scalars['uuid']>;
  _gte?: InputMaybe<Scalars['uuid']>;
  _in?: InputMaybe<Array<Scalars['uuid']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['uuid']>;
  _lte?: InputMaybe<Scalars['uuid']>;
  _neq?: InputMaybe<Scalars['uuid']>;
  _nin?: InputMaybe<Array<Scalars['uuid']>>;
};

export type UpdateProjectByPkMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  _set?: InputMaybe<App_Project_Set_Input>;
}>;


export type UpdateProjectByPkMutation = { __typename?: 'mutation_root', update_app_project_by_pk?: { __typename?: 'app_project', header?: string | null, label: string, photo?: string | null, status?: string | null, step?: string | null, title?: string | null, created_at?: string | null, id: any, updated_at?: string | null, public_description?: string | null, private_description?: string | null, year?: number | null, owner?: string | null, description_short?: string | null, contact_pnrfo?: string | null, budget?: number | null, partner?: string | null } | null };

export type UpdateFullProjectByPkMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  _set?: InputMaybe<App_Project_Set_Input>;
  project_themes?: InputMaybe<Array<App_Project_Theme_Insert_Input> | App_Project_Theme_Insert_Input>;
  project_territories?: InputMaybe<Array<App_Project_Territory_Insert_Input> | App_Project_Territory_Insert_Input>;
  project_financings?: InputMaybe<Array<App_Project_Financing_Insert_Input> | App_Project_Financing_Insert_Input>;
  project_types?: InputMaybe<Array<App_Project_Type_Insert_Input> | App_Project_Type_Insert_Input>;
}>;


export type UpdateFullProjectByPkMutation = { __typename?: 'mutation_root', update_app_project_by_pk?: { __typename?: 'app_project', header?: string | null, label: string, photo?: string | null, status?: string | null, step?: string | null, title?: string | null, created_at?: string | null, id: any, updated_at?: string | null, public_description?: string | null, private_description?: string | null, year?: number | null, owner?: string | null, description_short?: string | null, contact_pnrfo?: string | null, budget?: number | null, partner?: string | null } | null, delete_app_project_theme?: { __typename?: 'app_project_theme_mutation_response', affected_rows: number } | null, insert_app_project_theme?: { __typename?: 'app_project_theme_mutation_response', affected_rows: number } | null, delete_app_project_territory?: { __typename?: 'app_project_territory_mutation_response', affected_rows: number } | null, insert_app_project_territory?: { __typename?: 'app_project_territory_mutation_response', affected_rows: number } | null, delete_app_project_financing?: { __typename?: 'app_project_financing_mutation_response', affected_rows: number } | null, insert_app_project_financing?: { __typename?: 'app_project_financing_mutation_response', affected_rows: number } | null, delete_app_project_type?: { __typename?: 'app_project_type_mutation_response', affected_rows: number } | null, insert_app_project_type?: { __typename?: 'app_project_type_mutation_response', affected_rows: number } | null };

export type InsertProjectMutationVariables = Exact<{
  budget?: InputMaybe<Scalars['numeric']>;
  contact_pnrfo?: InputMaybe<Scalars['String']>;
  description_short?: InputMaybe<Scalars['String']>;
  header?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
  owner?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
  private_description?: InputMaybe<Scalars['String']>;
  public_description?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  step?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  partner?: InputMaybe<Scalars['String']>;
  year?: InputMaybe<Scalars['Int']>;
  project_themes?: InputMaybe<Array<App_Project_Theme_Insert_Input> | App_Project_Theme_Insert_Input>;
  project_types?: InputMaybe<Array<App_Project_Type_Insert_Input> | App_Project_Type_Insert_Input>;
  project_territories?: InputMaybe<Array<App_Project_Territory_Insert_Input> | App_Project_Territory_Insert_Input>;
  project_financings?: InputMaybe<Array<App_Project_Financing_Insert_Input> | App_Project_Financing_Insert_Input>;
}>;


export type InsertProjectMutation = { __typename?: 'mutation_root', insert_app_project_one?: { __typename?: 'app_project', created_at?: string | null, header?: string | null, id: any, label: string, photo?: string | null, status?: string | null, step?: string | null, title?: string | null, updated_at?: string | null, year?: number | null, owner?: string | null, public_description?: string | null, private_description?: string | null, description_short?: string | null, budget?: number | null, contact_pnrfo?: string | null, partner?: string | null, project_financings: Array<{ __typename?: 'app_project_financing', financing: { __typename?: 'app_financing', label: string, id: string } }>, project_territories: Array<{ __typename?: 'app_project_territory', territory: { __typename?: 'app_territory', label: string, id: string } }>, project_themes: Array<{ __typename?: 'app_project_theme', theme: { __typename?: 'app_theme', label: string, id: string } }>, project_types: Array<{ __typename?: 'app_project_type', type: { __typename?: 'app_type', label: string, id: string } }> } | null };

export type SoftDeleteOneProjectMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  _set?: InputMaybe<App_Project_Set_Input>;
}>;


export type SoftDeleteOneProjectMutation = { __typename?: 'mutation_root', update_app_project_by_pk?: { __typename?: 'app_project', deleted_at?: string | null } | null };

export type UpdateProjectFileByPkMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  _set?: InputMaybe<App_Project_File_Set_Input>;
}>;


export type UpdateProjectFileByPkMutation = { __typename?: 'mutation_root', update_app_project_file_by_pk?: { __typename?: 'app_project_file', created_at: string, filename?: string | null, preview?: string | null, url?: string | null, label?: string | null, id: any, project_id: any, updated_at: string, deleted_at?: string | null } | null };

export type InsertProjectFileMutationVariables = Exact<{
  url?: InputMaybe<Scalars['String']>;
  filename?: InputMaybe<Scalars['String']>;
  preview?: InputMaybe<Scalars['String']>;
  label?: InputMaybe<Scalars['String']>;
  project_id?: InputMaybe<Scalars['uuid']>;
}>;


export type InsertProjectFileMutation = { __typename?: 'mutation_root', insert_app_project_file_one?: { __typename?: 'app_project_file', created_at: string, filename?: string | null, preview?: string | null, url?: string | null, id: any, label?: string | null, project_id: any, updated_at: string, deleted_at?: string | null } | null };

export type DeleteProjectFileByPkMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteProjectFileByPkMutation = { __typename?: 'mutation_root', delete_app_project_file_by_pk?: { __typename?: 'app_project_file', id: any } | null };

export type FetchProjectsLightQueryVariables = Exact<{
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<App_Project_Bool_Exp>;
  order_by?: InputMaybe<Array<App_Project_Order_By> | App_Project_Order_By>;
}>;


export type FetchProjectsLightQuery = { __typename?: 'query_root', app_project: Array<{ __typename?: 'app_project', header?: string | null, id: any, label: string, photo?: string | null, title?: string | null, project_territories: Array<{ __typename?: 'app_project_territory', id: any, territory: { __typename?: 'app_territory', id: string, label: string, coords?: any | null } }>, project_themes: Array<{ __typename?: 'app_project_theme', id: any, theme: { __typename?: 'app_theme', id: string, label: string } }> }>, app_project_aggregate: { __typename?: 'app_project_aggregate', aggregate?: { __typename?: 'app_project_aggregate_fields', count: number } | null } };

export type FetchProjectsQueryVariables = Exact<{
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<App_Project_Bool_Exp>;
  order_by?: InputMaybe<Array<App_Project_Order_By> | App_Project_Order_By>;
}>;


export type FetchProjectsQuery = { __typename?: 'query_root', app_project: Array<{ __typename?: 'app_project', created_at?: string | null, header?: string | null, id: any, label: string, photo?: string | null, status?: string | null, step?: string | null, title?: string | null, updated_at?: string | null, budget?: number | null, contact_pnrfo?: string | null, deleted_at?: string | null, description_short?: string | null, owner?: string | null, partner?: string | null, private_description?: string | null, public_description?: string | null, year?: number | null, project_territories: Array<{ __typename?: 'app_project_territory', id: any, territory: { __typename?: 'app_territory', id: string, label: string, coords?: any | null } }>, project_financings: Array<{ __typename?: 'app_project_financing', id: any, financing: { __typename?: 'app_financing', id: string, label: string } }>, project_themes: Array<{ __typename?: 'app_project_theme', id: any, theme: { __typename?: 'app_theme', id: string, label: string } }>, project_types: Array<{ __typename?: 'app_project_type', id: any, type: { __typename?: 'app_type', id: string, label: string } }> }>, app_project_aggregate: { __typename?: 'app_project_aggregate', aggregate?: { __typename?: 'app_project_aggregate_fields', count: number } | null } };

export type FetchProjectByPkQueryVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchProjectByPkQuery = { __typename?: 'query_root', app_project_by_pk?: { __typename?: 'app_project', created_at?: string | null, header?: string | null, id: any, label: string, photo?: string | null, status?: string | null, step?: string | null, title?: string | null, updated_at?: string | null, budget?: number | null, contact_pnrfo?: string | null, deleted_at?: string | null, description_short?: string | null, year?: number | null, public_description?: string | null, private_description?: string | null, partner?: string | null, owner?: string | null, project_files: Array<{ __typename?: 'app_project_file', created_at: string, filename?: string | null, id: any, label?: string | null, url?: string | null, preview?: string | null, project_id: any, updated_at: string, deleted_at?: string | null }>, project_themes: Array<{ __typename?: 'app_project_theme', id: any, theme: { __typename?: 'app_theme', id: string, label: string } }>, project_territories: Array<{ __typename?: 'app_project_territory', id: any, territory: { __typename?: 'app_territory', id: string, label: string, coords?: any | null } }>, project_types: Array<{ __typename?: 'app_project_type', id: any, type: { __typename?: 'app_type', id: string, label: string } }>, project_financings: Array<{ __typename?: 'app_project_financing', id: any, financing: { __typename?: 'app_financing', id: string, label: string } }> } | null };

export type FetchDataForProjectFormQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchDataForProjectFormQuery = { __typename?: 'query_root', app_theme: Array<{ __typename?: 'app_theme', label: string, id: string }>, app_type: Array<{ __typename?: 'app_type', id: string, label: string }>, app_territory: Array<{ __typename?: 'app_territory', id: string, label: string, coords?: any | null }>, app_financing: Array<{ __typename?: 'app_financing', id: string, label: string }> };

export type FetchDataForProjectsListQueryVariables = Exact<{
  where_territory_projects_aggregate?: InputMaybe<App_Project_Territory_Bool_Exp>;
}>;


export type FetchDataForProjectsListQuery = { __typename?: 'query_root', app_project_year: Array<{ __typename?: 'app_project', year?: number | null }>, app_territory: Array<{ __typename?: 'app_territory', coords?: any | null, id: string, label: string, projects_aggregate: { __typename?: 'app_project_territory_aggregate', aggregate?: { __typename?: 'app_project_territory_aggregate_fields', count: number } | null }, filtered_projects_aggregate: { __typename?: 'app_project_territory_aggregate', aggregate?: { __typename?: 'app_project_territory_aggregate_fields', count: number } | null } }>, app_theme: Array<{ __typename?: 'app_theme', id: string, label: string }>, app_type: Array<{ __typename?: 'app_type', id: string, label: string }> };

export type FetchProjectFileByPkQueryVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchProjectFileByPkQuery = { __typename?: 'query_root', app_project_file_by_pk?: { __typename?: 'app_project_file', created_at: string, deleted_at?: string | null, filename?: string | null, id: any, label?: string | null, preview?: string | null, project_id: any, updated_at: string, url?: string | null } | null };


export const UpdateProjectByPkDocument = gql`
    mutation updateProjectByPk($id: uuid = "", $_set: app_project_set_input = {}) {
  update_app_project_by_pk(pk_columns: {id: $id}, _set: $_set) {
    header
    label
    photo
    status
    step
    title
    created_at
    id
    updated_at
    public_description
    private_description
    year
    owner
    description_short
    contact_pnrfo
    budget
    partner
  }
}
    `;
export type UpdateProjectByPkMutationFn = Apollo.MutationFunction<UpdateProjectByPkMutation, UpdateProjectByPkMutationVariables>;

/**
 * __useUpdateProjectByPkMutation__
 *
 * To run a mutation, you first call `useUpdateProjectByPkMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateProjectByPkMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateProjectByPkMutation, { data, loading, error }] = useUpdateProjectByPkMutation({
 *   variables: {
 *      id: // value for 'id'
 *      _set: // value for '_set'
 *   },
 * });
 */
export function useUpdateProjectByPkMutation(baseOptions?: Apollo.MutationHookOptions<UpdateProjectByPkMutation, UpdateProjectByPkMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateProjectByPkMutation, UpdateProjectByPkMutationVariables>(UpdateProjectByPkDocument, options);
      }
export type UpdateProjectByPkMutationHookResult = ReturnType<typeof useUpdateProjectByPkMutation>;
export type UpdateProjectByPkMutationResult = Apollo.MutationResult<UpdateProjectByPkMutation>;
export type UpdateProjectByPkMutationOptions = Apollo.BaseMutationOptions<UpdateProjectByPkMutation, UpdateProjectByPkMutationVariables>;
export const UpdateFullProjectByPkDocument = gql`
    mutation updateFullProjectByPk($id: uuid = "", $_set: app_project_set_input = {}, $project_themes: [app_project_theme_insert_input!] = [], $project_territories: [app_project_territory_insert_input!] = [], $project_financings: [app_project_financing_insert_input!] = [], $project_types: [app_project_type_insert_input!] = []) {
  update_app_project_by_pk(pk_columns: {id: $id}, _set: $_set) {
    header
    label
    photo
    status
    step
    title
    created_at
    id
    updated_at
    public_description
    private_description
    year
    owner
    description_short
    contact_pnrfo
    budget
    partner
  }
  delete_app_project_theme(where: {project_id: {_eq: $id}}) {
    affected_rows
  }
  insert_app_project_theme(objects: $project_themes) {
    affected_rows
  }
  delete_app_project_territory(where: {project_id: {_eq: $id}}) {
    affected_rows
  }
  insert_app_project_territory(objects: $project_territories) {
    affected_rows
  }
  delete_app_project_financing(where: {project_id: {_eq: $id}}) {
    affected_rows
  }
  insert_app_project_financing(objects: $project_financings) {
    affected_rows
  }
  delete_app_project_type(where: {project_id: {_eq: $id}}) {
    affected_rows
  }
  insert_app_project_type(objects: $project_types) {
    affected_rows
  }
}
    `;
export type UpdateFullProjectByPkMutationFn = Apollo.MutationFunction<UpdateFullProjectByPkMutation, UpdateFullProjectByPkMutationVariables>;

/**
 * __useUpdateFullProjectByPkMutation__
 *
 * To run a mutation, you first call `useUpdateFullProjectByPkMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateFullProjectByPkMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateFullProjectByPkMutation, { data, loading, error }] = useUpdateFullProjectByPkMutation({
 *   variables: {
 *      id: // value for 'id'
 *      _set: // value for '_set'
 *      project_themes: // value for 'project_themes'
 *      project_territories: // value for 'project_territories'
 *      project_financings: // value for 'project_financings'
 *      project_types: // value for 'project_types'
 *   },
 * });
 */
export function useUpdateFullProjectByPkMutation(baseOptions?: Apollo.MutationHookOptions<UpdateFullProjectByPkMutation, UpdateFullProjectByPkMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateFullProjectByPkMutation, UpdateFullProjectByPkMutationVariables>(UpdateFullProjectByPkDocument, options);
      }
export type UpdateFullProjectByPkMutationHookResult = ReturnType<typeof useUpdateFullProjectByPkMutation>;
export type UpdateFullProjectByPkMutationResult = Apollo.MutationResult<UpdateFullProjectByPkMutation>;
export type UpdateFullProjectByPkMutationOptions = Apollo.BaseMutationOptions<UpdateFullProjectByPkMutation, UpdateFullProjectByPkMutationVariables>;
export const InsertProjectDocument = gql`
    mutation insertProject($budget: numeric = null, $contact_pnrfo: String = "", $description_short: String = "", $header: String = "", $label: String = "", $owner: String = "", $photo: String = "", $private_description: String = "", $public_description: String = "", $status: String = "", $step: String = "", $title: String = "", $partner: String = "", $year: Int = null, $project_themes: [app_project_theme_insert_input!] = [], $project_types: [app_project_type_insert_input!] = [], $project_territories: [app_project_territory_insert_input!] = [], $project_financings: [app_project_financing_insert_input!] = []) {
  insert_app_project_one(
    object: {budget: $budget, contact_pnrfo: $contact_pnrfo, description_short: $description_short, header: $header, label: $label, owner: $owner, photo: $photo, private_description: $private_description, public_description: $public_description, status: $status, step: $step, title: $title, partner: $partner, year: $year, project_themes: {data: $project_themes}, project_types: {data: $project_types}, project_territories: {data: $project_territories}, project_financings: {data: $project_financings}}
  ) {
    created_at
    header
    id
    label
    photo
    status
    step
    title
    updated_at
    project_financings {
      financing {
        label
        id
      }
    }
    project_territories {
      territory {
        label
        id
      }
    }
    project_themes {
      theme {
        label
        id
      }
    }
    project_types {
      type {
        label
        id
      }
    }
    year
    owner
    public_description
    private_description
    description_short
    status
    step
    budget
    contact_pnrfo
    partner
  }
}
    `;
export type InsertProjectMutationFn = Apollo.MutationFunction<InsertProjectMutation, InsertProjectMutationVariables>;

/**
 * __useInsertProjectMutation__
 *
 * To run a mutation, you first call `useInsertProjectMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertProjectMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertProjectMutation, { data, loading, error }] = useInsertProjectMutation({
 *   variables: {
 *      budget: // value for 'budget'
 *      contact_pnrfo: // value for 'contact_pnrfo'
 *      description_short: // value for 'description_short'
 *      header: // value for 'header'
 *      label: // value for 'label'
 *      owner: // value for 'owner'
 *      photo: // value for 'photo'
 *      private_description: // value for 'private_description'
 *      public_description: // value for 'public_description'
 *      status: // value for 'status'
 *      step: // value for 'step'
 *      title: // value for 'title'
 *      partner: // value for 'partner'
 *      year: // value for 'year'
 *      project_themes: // value for 'project_themes'
 *      project_types: // value for 'project_types'
 *      project_territories: // value for 'project_territories'
 *      project_financings: // value for 'project_financings'
 *   },
 * });
 */
export function useInsertProjectMutation(baseOptions?: Apollo.MutationHookOptions<InsertProjectMutation, InsertProjectMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertProjectMutation, InsertProjectMutationVariables>(InsertProjectDocument, options);
      }
export type InsertProjectMutationHookResult = ReturnType<typeof useInsertProjectMutation>;
export type InsertProjectMutationResult = Apollo.MutationResult<InsertProjectMutation>;
export type InsertProjectMutationOptions = Apollo.BaseMutationOptions<InsertProjectMutation, InsertProjectMutationVariables>;
export const SoftDeleteOneProjectDocument = gql`
    mutation softDeleteOneProject($id: uuid = "", $_set: app_project_set_input = {}) {
  update_app_project_by_pk(pk_columns: {id: $id}, _set: $_set) {
    deleted_at
  }
}
    `;
export type SoftDeleteOneProjectMutationFn = Apollo.MutationFunction<SoftDeleteOneProjectMutation, SoftDeleteOneProjectMutationVariables>;

/**
 * __useSoftDeleteOneProjectMutation__
 *
 * To run a mutation, you first call `useSoftDeleteOneProjectMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSoftDeleteOneProjectMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [softDeleteOneProjectMutation, { data, loading, error }] = useSoftDeleteOneProjectMutation({
 *   variables: {
 *      id: // value for 'id'
 *      _set: // value for '_set'
 *   },
 * });
 */
export function useSoftDeleteOneProjectMutation(baseOptions?: Apollo.MutationHookOptions<SoftDeleteOneProjectMutation, SoftDeleteOneProjectMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SoftDeleteOneProjectMutation, SoftDeleteOneProjectMutationVariables>(SoftDeleteOneProjectDocument, options);
      }
export type SoftDeleteOneProjectMutationHookResult = ReturnType<typeof useSoftDeleteOneProjectMutation>;
export type SoftDeleteOneProjectMutationResult = Apollo.MutationResult<SoftDeleteOneProjectMutation>;
export type SoftDeleteOneProjectMutationOptions = Apollo.BaseMutationOptions<SoftDeleteOneProjectMutation, SoftDeleteOneProjectMutationVariables>;
export const UpdateProjectFileByPkDocument = gql`
    mutation updateProjectFileByPk($id: uuid = "", $_set: app_project_file_set_input = {}) {
  update_app_project_file_by_pk(pk_columns: {id: $id}, _set: $_set) {
    created_at
    filename
    preview
    url
    label
    id
    project_id
    updated_at
    deleted_at
  }
}
    `;
export type UpdateProjectFileByPkMutationFn = Apollo.MutationFunction<UpdateProjectFileByPkMutation, UpdateProjectFileByPkMutationVariables>;

/**
 * __useUpdateProjectFileByPkMutation__
 *
 * To run a mutation, you first call `useUpdateProjectFileByPkMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateProjectFileByPkMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateProjectFileByPkMutation, { data, loading, error }] = useUpdateProjectFileByPkMutation({
 *   variables: {
 *      id: // value for 'id'
 *      _set: // value for '_set'
 *   },
 * });
 */
export function useUpdateProjectFileByPkMutation(baseOptions?: Apollo.MutationHookOptions<UpdateProjectFileByPkMutation, UpdateProjectFileByPkMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateProjectFileByPkMutation, UpdateProjectFileByPkMutationVariables>(UpdateProjectFileByPkDocument, options);
      }
export type UpdateProjectFileByPkMutationHookResult = ReturnType<typeof useUpdateProjectFileByPkMutation>;
export type UpdateProjectFileByPkMutationResult = Apollo.MutationResult<UpdateProjectFileByPkMutation>;
export type UpdateProjectFileByPkMutationOptions = Apollo.BaseMutationOptions<UpdateProjectFileByPkMutation, UpdateProjectFileByPkMutationVariables>;
export const InsertProjectFileDocument = gql`
    mutation insertProjectFile($url: String = null, $filename: String = null, $preview: String = null, $label: String = null, $project_id: uuid = null) {
  insert_app_project_file_one(
    object: {url: $url, filename: $filename, preview: $preview, label: $label, project_id: $project_id}
  ) {
    created_at
    filename
    preview
    url
    id
    label
    project_id
    updated_at
    deleted_at
  }
}
    `;
export type InsertProjectFileMutationFn = Apollo.MutationFunction<InsertProjectFileMutation, InsertProjectFileMutationVariables>;

/**
 * __useInsertProjectFileMutation__
 *
 * To run a mutation, you first call `useInsertProjectFileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertProjectFileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertProjectFileMutation, { data, loading, error }] = useInsertProjectFileMutation({
 *   variables: {
 *      url: // value for 'url'
 *      filename: // value for 'filename'
 *      preview: // value for 'preview'
 *      label: // value for 'label'
 *      project_id: // value for 'project_id'
 *   },
 * });
 */
export function useInsertProjectFileMutation(baseOptions?: Apollo.MutationHookOptions<InsertProjectFileMutation, InsertProjectFileMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertProjectFileMutation, InsertProjectFileMutationVariables>(InsertProjectFileDocument, options);
      }
export type InsertProjectFileMutationHookResult = ReturnType<typeof useInsertProjectFileMutation>;
export type InsertProjectFileMutationResult = Apollo.MutationResult<InsertProjectFileMutation>;
export type InsertProjectFileMutationOptions = Apollo.BaseMutationOptions<InsertProjectFileMutation, InsertProjectFileMutationVariables>;
export const DeleteProjectFileByPkDocument = gql`
    mutation deleteProjectFileByPk($id: uuid = "") {
  delete_app_project_file_by_pk(id: $id) {
    id
  }
}
    `;
export type DeleteProjectFileByPkMutationFn = Apollo.MutationFunction<DeleteProjectFileByPkMutation, DeleteProjectFileByPkMutationVariables>;

/**
 * __useDeleteProjectFileByPkMutation__
 *
 * To run a mutation, you first call `useDeleteProjectFileByPkMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteProjectFileByPkMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteProjectFileByPkMutation, { data, loading, error }] = useDeleteProjectFileByPkMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteProjectFileByPkMutation(baseOptions?: Apollo.MutationHookOptions<DeleteProjectFileByPkMutation, DeleteProjectFileByPkMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteProjectFileByPkMutation, DeleteProjectFileByPkMutationVariables>(DeleteProjectFileByPkDocument, options);
      }
export type DeleteProjectFileByPkMutationHookResult = ReturnType<typeof useDeleteProjectFileByPkMutation>;
export type DeleteProjectFileByPkMutationResult = Apollo.MutationResult<DeleteProjectFileByPkMutation>;
export type DeleteProjectFileByPkMutationOptions = Apollo.BaseMutationOptions<DeleteProjectFileByPkMutation, DeleteProjectFileByPkMutationVariables>;
export const FetchProjectsLightDocument = gql`
    query fetchProjectsLight($limit: Int = 30, $offset: Int = 0, $where: app_project_bool_exp, $order_by: [app_project_order_by!] = {}) {
  app_project(limit: $limit, offset: $offset, where: $where, order_by: $order_by) {
    header
    id
    label
    photo
    title
    project_territories {
      id
      territory {
        id
        label
        coords
      }
    }
    project_themes {
      id
      theme {
        id
        label
      }
    }
  }
  app_project_aggregate(where: $where) {
    aggregate {
      count(distinct: false)
    }
  }
}
    `;

/**
 * __useFetchProjectsLightQuery__
 *
 * To run a query within a React component, call `useFetchProjectsLightQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchProjectsLightQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchProjectsLightQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      where: // value for 'where'
 *      order_by: // value for 'order_by'
 *   },
 * });
 */
export function useFetchProjectsLightQuery(baseOptions?: Apollo.QueryHookOptions<FetchProjectsLightQuery, FetchProjectsLightQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchProjectsLightQuery, FetchProjectsLightQueryVariables>(FetchProjectsLightDocument, options);
      }
export function useFetchProjectsLightLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchProjectsLightQuery, FetchProjectsLightQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchProjectsLightQuery, FetchProjectsLightQueryVariables>(FetchProjectsLightDocument, options);
        }
export type FetchProjectsLightQueryHookResult = ReturnType<typeof useFetchProjectsLightQuery>;
export type FetchProjectsLightLazyQueryHookResult = ReturnType<typeof useFetchProjectsLightLazyQuery>;
export type FetchProjectsLightQueryResult = Apollo.QueryResult<FetchProjectsLightQuery, FetchProjectsLightQueryVariables>;
export const FetchProjectsDocument = gql`
    query fetchProjects($limit: Int = 30, $offset: Int = 0, $where: app_project_bool_exp, $order_by: [app_project_order_by!] = {}) {
  app_project(limit: $limit, offset: $offset, where: $where, order_by: $order_by) {
    created_at
    header
    id
    label
    photo
    status
    step
    title
    updated_at
    project_territories {
      territory {
        id
        label
        coords
      }
      id
    }
    budget
    contact_pnrfo
    deleted_at
    description_short
    owner
    partner
    private_description
    project_financings {
      financing {
        id
        label
      }
      id
    }
    project_themes {
      theme {
        id
        label
      }
      id
    }
    public_description
    year
    project_types {
      id
      type {
        id
        label
      }
    }
  }
  app_project_aggregate(where: $where) {
    aggregate {
      count(distinct: false)
    }
  }
}
    `;

/**
 * __useFetchProjectsQuery__
 *
 * To run a query within a React component, call `useFetchProjectsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchProjectsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchProjectsQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      where: // value for 'where'
 *      order_by: // value for 'order_by'
 *   },
 * });
 */
export function useFetchProjectsQuery(baseOptions?: Apollo.QueryHookOptions<FetchProjectsQuery, FetchProjectsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchProjectsQuery, FetchProjectsQueryVariables>(FetchProjectsDocument, options);
      }
export function useFetchProjectsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchProjectsQuery, FetchProjectsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchProjectsQuery, FetchProjectsQueryVariables>(FetchProjectsDocument, options);
        }
export type FetchProjectsQueryHookResult = ReturnType<typeof useFetchProjectsQuery>;
export type FetchProjectsLazyQueryHookResult = ReturnType<typeof useFetchProjectsLazyQuery>;
export type FetchProjectsQueryResult = Apollo.QueryResult<FetchProjectsQuery, FetchProjectsQueryVariables>;
export const FetchProjectByPkDocument = gql`
    query fetchProjectByPk($id: uuid = "") {
  app_project_by_pk(id: $id) {
    created_at
    header
    id
    label
    photo
    status
    step
    title
    updated_at
    budget
    contact_pnrfo
    deleted_at
    description_short
    project_files(
      order_by: {created_at: asc}
      where: {deleted_at: {_is_null: true}}
    ) {
      created_at
      filename
      id
      label
      url
      preview
      project_id
      updated_at
      deleted_at
    }
    year
    public_description
    private_description
    partner
    owner
    project_themes {
      id
      theme {
        id
        label
      }
    }
    project_territories {
      id
      territory {
        id
        label
        coords
      }
    }
    project_types {
      id
      type {
        id
        label
      }
    }
    project_financings {
      id
      financing {
        id
        label
      }
    }
  }
}
    `;

/**
 * __useFetchProjectByPkQuery__
 *
 * To run a query within a React component, call `useFetchProjectByPkQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchProjectByPkQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchProjectByPkQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFetchProjectByPkQuery(baseOptions?: Apollo.QueryHookOptions<FetchProjectByPkQuery, FetchProjectByPkQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchProjectByPkQuery, FetchProjectByPkQueryVariables>(FetchProjectByPkDocument, options);
      }
export function useFetchProjectByPkLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchProjectByPkQuery, FetchProjectByPkQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchProjectByPkQuery, FetchProjectByPkQueryVariables>(FetchProjectByPkDocument, options);
        }
export type FetchProjectByPkQueryHookResult = ReturnType<typeof useFetchProjectByPkQuery>;
export type FetchProjectByPkLazyQueryHookResult = ReturnType<typeof useFetchProjectByPkLazyQuery>;
export type FetchProjectByPkQueryResult = Apollo.QueryResult<FetchProjectByPkQuery, FetchProjectByPkQueryVariables>;
export const FetchDataForProjectFormDocument = gql`
    query fetchDataForProjectForm {
  app_theme(order_by: {label: asc}) {
    label
    id
  }
  app_type(order_by: {label: asc}) {
    id
    label
  }
  app_territory(order_by: {label: asc}) {
    id
    label
    coords
  }
  app_financing(order_by: {label: asc}) {
    id
    label
  }
}
    `;

/**
 * __useFetchDataForProjectFormQuery__
 *
 * To run a query within a React component, call `useFetchDataForProjectFormQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchDataForProjectFormQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchDataForProjectFormQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchDataForProjectFormQuery(baseOptions?: Apollo.QueryHookOptions<FetchDataForProjectFormQuery, FetchDataForProjectFormQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchDataForProjectFormQuery, FetchDataForProjectFormQueryVariables>(FetchDataForProjectFormDocument, options);
      }
export function useFetchDataForProjectFormLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchDataForProjectFormQuery, FetchDataForProjectFormQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchDataForProjectFormQuery, FetchDataForProjectFormQueryVariables>(FetchDataForProjectFormDocument, options);
        }
export type FetchDataForProjectFormQueryHookResult = ReturnType<typeof useFetchDataForProjectFormQuery>;
export type FetchDataForProjectFormLazyQueryHookResult = ReturnType<typeof useFetchDataForProjectFormLazyQuery>;
export type FetchDataForProjectFormQueryResult = Apollo.QueryResult<FetchDataForProjectFormQuery, FetchDataForProjectFormQueryVariables>;
export const FetchDataForProjectsListDocument = gql`
    query fetchDataForProjectsList($where_territory_projects_aggregate: app_project_territory_bool_exp = {}) {
  app_project_year: app_project(
    distinct_on: year
    order_by: {year: desc}
    where: {year: {_is_null: false}, deleted_at: {_is_null: true}}
  ) {
    year
  }
  app_territory(order_by: {label: asc}) {
    coords
    id
    label
    projects_aggregate: territory_projects_aggregate(distinct_on: project_id) {
      aggregate {
        count
      }
    }
    filtered_projects_aggregate: territory_projects_aggregate(
      distinct_on: project_id
      where: $where_territory_projects_aggregate
    ) {
      aggregate {
        count
      }
    }
  }
  app_theme(order_by: {label: asc}) {
    id
    label
  }
  app_type(order_by: {label: asc}) {
    id
    label
  }
}
    `;

/**
 * __useFetchDataForProjectsListQuery__
 *
 * To run a query within a React component, call `useFetchDataForProjectsListQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchDataForProjectsListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchDataForProjectsListQuery({
 *   variables: {
 *      where_territory_projects_aggregate: // value for 'where_territory_projects_aggregate'
 *   },
 * });
 */
export function useFetchDataForProjectsListQuery(baseOptions?: Apollo.QueryHookOptions<FetchDataForProjectsListQuery, FetchDataForProjectsListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchDataForProjectsListQuery, FetchDataForProjectsListQueryVariables>(FetchDataForProjectsListDocument, options);
      }
export function useFetchDataForProjectsListLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchDataForProjectsListQuery, FetchDataForProjectsListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchDataForProjectsListQuery, FetchDataForProjectsListQueryVariables>(FetchDataForProjectsListDocument, options);
        }
export type FetchDataForProjectsListQueryHookResult = ReturnType<typeof useFetchDataForProjectsListQuery>;
export type FetchDataForProjectsListLazyQueryHookResult = ReturnType<typeof useFetchDataForProjectsListLazyQuery>;
export type FetchDataForProjectsListQueryResult = Apollo.QueryResult<FetchDataForProjectsListQuery, FetchDataForProjectsListQueryVariables>;
export const FetchProjectFileByPkDocument = gql`
    query fetchProjectFileByPk($id: uuid = "") {
  app_project_file_by_pk(id: $id) {
    created_at
    deleted_at
    filename
    id
    label
    preview
    project_id
    updated_at
    url
  }
}
    `;

/**
 * __useFetchProjectFileByPkQuery__
 *
 * To run a query within a React component, call `useFetchProjectFileByPkQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchProjectFileByPkQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchProjectFileByPkQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFetchProjectFileByPkQuery(baseOptions?: Apollo.QueryHookOptions<FetchProjectFileByPkQuery, FetchProjectFileByPkQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchProjectFileByPkQuery, FetchProjectFileByPkQueryVariables>(FetchProjectFileByPkDocument, options);
      }
export function useFetchProjectFileByPkLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchProjectFileByPkQuery, FetchProjectFileByPkQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchProjectFileByPkQuery, FetchProjectFileByPkQueryVariables>(FetchProjectFileByPkDocument, options);
        }
export type FetchProjectFileByPkQueryHookResult = ReturnType<typeof useFetchProjectFileByPkQuery>;
export type FetchProjectFileByPkLazyQueryHookResult = ReturnType<typeof useFetchProjectFileByPkLazyQuery>;
export type FetchProjectFileByPkQueryResult = Apollo.QueryResult<FetchProjectFileByPkQuery, FetchProjectFileByPkQueryVariables>;