import type { NextApiRequest, NextApiResponse } from "next";
import { unstable_getServerSession } from "next-auth";
import { authOptions } from "../auth/[...nextauth]";
import { initializeApollo } from "../../../lib/apolloClient";
import {
  DeleteProjectFileByPkDocument,
  DeleteProjectFileByPkMutation,
  DeleteProjectFileByPkMutationVariables,
  FetchProjectFileByPkDocument,
  FetchProjectFileByPkQuery,
  FetchProjectFileByPkQueryVariables,
  UpdateProjectFileByPkDocument,
  UpdateProjectFileByPkMutation,
  UpdateProjectFileByPkMutationVariables,
} from "../../../generated/graphql";
import { removeFromMinioAndImagor } from "../../../lib/apiUtils";
import { safeJSONParse } from "../../../lib/utils";

// TODO ? mv to api/projects/[project_id]/files/[file_id]
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const id = req.query.id;
  const session = await unstable_getServerSession(req, res, authOptions);
  if (!session || !session.roles?.includes("admin")) {
    return res.status(401).end();
  }

  if (req.method === "PATCH") {
    try {
      const apolloClient = initializeApollo(session);

      const { data } = await apolloClient.query<
        FetchProjectFileByPkQuery,
        FetchProjectFileByPkQueryVariables
      >({
        query: FetchProjectFileByPkDocument,
        variables: {
          id,
        },
      });

      if (!data?.app_project_file_by_pk) {
        return {
          notFound: true,
        };
      }

      const obj = data.app_project_file_by_pk;
      const _set: UpdateProjectFileByPkMutationVariables["_set"] =
        safeJSONParse(req.body);
      const projectId = obj.project_id;

      if (
        obj.filename &&
        typeof _set?.filename !== "undefined" &&
        obj.filename != _set?.filename
      ) {
        const objectName = `${projectId}/${obj.filename}`;
        try {
          await removeFromMinioAndImagor(process.env, objectName);
        } catch (error) {}
      }
      if (
        obj.preview &&
        typeof _set?.preview !== "undefined" &&
        obj.preview != _set?.preview
      ) {
        const objectName = `${projectId}/${obj.preview}`;
        try {
          await removeFromMinioAndImagor(process.env, objectName);
        } catch (error) {}
      }

      const response = await apolloClient.query<
        UpdateProjectFileByPkMutation,
        UpdateProjectFileByPkMutationVariables
      >({
        query: UpdateProjectFileByPkDocument,
        variables: {
          id,
          _set,
        },
      });

      if (response.errors) {
        return res.status(400).json(response);
      }

      return res.json(response);
    } catch (error) {
      console.log("patch file error", error);
      try {
        res.status(500).send((error as any).toString());
      } catch (error) {}
    }

    res.status(500).end();
  }

  if (req.method === "DELETE") {
    try {
      const apolloClient = initializeApollo(session);

      const { data } = await apolloClient.query<
        FetchProjectFileByPkQuery,
        FetchProjectFileByPkQueryVariables
      >({
        query: FetchProjectFileByPkDocument,
        variables: {
          id,
        },
      });

      if (!data?.app_project_file_by_pk) {
        return {
          notFound: true,
        };
      }

      const obj = data.app_project_file_by_pk;
      const projectId = obj.project_id;

      if (obj.filename) {
        const objectName = `${projectId}/${obj.filename}`;
        try {
          await removeFromMinioAndImagor(process.env, objectName);
        } catch (error) {}
      }
      if (obj.preview) {
        const objectName = `${projectId}/${obj.preview}`;
        try {
          await removeFromMinioAndImagor(process.env, objectName);
        } catch (error) {}
      }

      const response = await apolloClient.query<
        DeleteProjectFileByPkMutation,
        DeleteProjectFileByPkMutationVariables
      >({
        query: DeleteProjectFileByPkDocument,
        variables: {
          id,
        },
      });

      if (response.errors) {
        return res.status(400).json(response);
      }

      return res.json(response);
    } catch (error) {
      console.log("delete file error", error);
      try {
        res.status(500).send((error as any).toString());
      } catch (error) {}
    }

    res.status(500).end();
  }
}
