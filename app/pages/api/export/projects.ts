import format from "date-fns/format";
import type { NextApiRequest, NextApiResponse } from "next";
import { Session, unstable_getServerSession } from "next-auth";
import Papa from "papaparse";
import XLSX from "xlsx";
import { authOptions } from "../auth/[...nextauth]";
import { get } from "lodash";
import { EXPORT_COLS } from "../../../lib/constants";
import { initializeApollo } from "../../../lib/apolloClient";
import { QueryVariables } from "../../../types/app-types";
import { deserializeRouterQuery, qsFiltersToGraphQL } from "../../../lib/utils";
import {
  FetchProjectsDocument,
  FetchProjectsQuery,
  FetchProjectsQueryVariables,
} from "../../../generated/graphql";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await unstable_getServerSession(req, res, authOptions);
  if (!session || !session.roles?.includes("admin")) {
    return res.status(401).end();
  }

  if (req.method === "GET") {
    try {
      if (req.query.format === "xlsx") {
        exportXLSX(req, res, session);
      } else {
        exportCSV(req, res, session);
      }
      return;
    } catch (error) {
      console.log("export projects error", error);
      try {
        res.status(500).send((error as any).toString());
      } catch (error) {}
    }

    res.status(500).end();
  }
}

const formatCsvValue = (col: any, value: any) => {
  if (value === null) {
    return "";
  }
  if (col.type === "date") {
    return !value ? "" : format(new Date(value), "dd-MM-yyyy");
  }

  return value;
};

async function exportCSV(
  req: NextApiRequest,
  res: NextApiResponse,
  session: Session
) {
  const colLabels = EXPORT_COLS.map((col) => col.label);
  const result = await exportProjects(req, session);
  const projects = result.data.app_project;
  const rows = [colLabels].concat(
    projects.map((project: any) => {
      return EXPORT_COLS.map((col) => {
        if (!col.path) {
          return formatCsvValue(col, project[col.name]);
        }
        return ((project[col.name] as any[]) || [])
          .map((item) => formatCsvValue(col, get(item, col.path, "")))
          .join(", ");
      });
    })
  );

  res.setHeader("Content-Type", "application/csv");
  res.setHeader("Content-Disposition", "attachment; filename=projects.csv");

  return res.send("\ufeff" + Papa.unparse(rows, { delimiter: ";" }));
}

async function exportXLSX(
  req: NextApiRequest,
  res: NextApiResponse,
  session: Session
) {
  const dateCols = EXPORT_COLS.filter((col) => col.type === "date");
  const arrayCols = EXPORT_COLS.filter((col) => col.path);
  const colLabels = EXPORT_COLS.map((col) => col.label);
  const result = await exportProjects(req, session);

  const projects = result.data.app_project.map((project: any) => {
    dateCols.forEach((col) => {
      try {
        project[col.name] = new Date(project[col.name].split("T").shift());
      } catch (error) {
        project[col.name] = "";
      }
    });
    arrayCols.forEach((col) => {
      try {
        project[col.name] = (project[col.name] as any[])
          .map((item) => get(item, col.path as string, ""))
          .join(", ");
      } catch (error) {}
    });

    return project;
  });
  const workbook = XLSX.utils.book_new();
  const worksheet = XLSX.utils.json_to_sheet(projects, {
    header: EXPORT_COLS.map((col) => col.name),
  });
  XLSX.utils.book_append_sheet(workbook, worksheet, "Projets");
  XLSX.utils.sheet_add_aoa(worksheet, [colLabels], { origin: "A1" });

  const buffer = XLSX.write(workbook, { type: "buffer", bookType: "xlsx" });

  res.setHeader("Content-Disposition", 'attachment; filename="projects.xlsx"');
  res.setHeader("Content-Type", "application/vnd.ms-excel");

  return res.send(buffer);
}

async function exportProjects(req: NextApiRequest, session: Session) {
  const client = initializeApollo(session);

  const deserialized: QueryVariables = {
    ...deserializeRouterQuery(req.query),
  };

  const where = qsFiltersToGraphQL(
    "project",
    deserialized.where || [],
    deserialized.search
  );
  where._and.push({
    deleted_at: {
      _is_null: true,
    },
  });

  return await client.query<FetchProjectsQuery, FetchProjectsQueryVariables>({
    query: FetchProjectsDocument,
    variables: {
      /* offset: ((deserialized.page || 1) - 1) * PAGE_SIZE,
          limit: PAGE_SIZE, */
      where: where,
      order_by: deserialized.order_by,
    },
  });
}
