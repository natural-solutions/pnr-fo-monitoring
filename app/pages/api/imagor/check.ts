import type { NextApiRequest, NextApiResponse } from "next";
import { IMAGOR_PRESETS, IMAGOR_PRESET_NAME } from "../../../lib/constants";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // Keep this comment to remember how to handle session
  //const session = await unstable_getServerSession(req, res, authOptions);
  let uri = req.headers["x-forwarded-uri"] as string;
  if (!uri) {
    return res.status(403).send("missing or invalid x-forwarded-uri header");
  }
  if (uri.charAt(0) == "/") {
    uri = uri.slice(1);
  }
  const reqPreset = uri.split("/")[2] as IMAGOR_PRESET_NAME;
  const preset = IMAGOR_PRESETS[reqPreset];
  if (!preset) {
    return res.status(403).send(`invalid preset: ${reqPreset}`);
  }
  //console.log(session?.user);

  return res.status(200).send("");
}
