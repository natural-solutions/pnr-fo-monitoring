import type { NextApiRequest, NextApiResponse } from "next";
import { unstable_getServerSession } from "next-auth";
import {
  FetchProjectByPkDocument,
  FetchProjectByPkQuery,
  FetchProjectByPkQueryVariables,
  UpdateFullProjectByPkDocument,
  UpdateFullProjectByPkMutation,
  UpdateFullProjectByPkMutationVariables,
} from "../../../../generated/graphql";
import { removeFromMinioAndImagor } from "../../../../lib/apiUtils";
import { initializeApollo } from "../../../../lib/apolloClient";
import { safeJSONParse } from "../../../../lib/utils";
import { authOptions } from "../../auth/[...nextauth]";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await unstable_getServerSession(req, res, authOptions);
  if (!session || !session.roles?.includes("admin")) {
    return res.status(401).end();
  }

  const id = req.query.project_id as string;

  if (req.method === "PATCH") {
    try {
      const apolloClient = initializeApollo(session);

      const { data } = await apolloClient.query<
        FetchProjectByPkQuery,
        FetchProjectByPkQueryVariables
      >({
        query: FetchProjectByPkDocument,
        variables: {
          id,
        },
      });

      if (!data?.app_project_by_pk) {
        return {
          notFound: true,
        };
      }

      const obj = data.app_project_by_pk;
      const variables: UpdateFullProjectByPkMutationVariables = safeJSONParse(
        req.body
      );

      const _set = { ...variables._set };

      const response = await apolloClient.query<
        UpdateFullProjectByPkMutation,
        UpdateFullProjectByPkMutationVariables
      >({
        query: UpdateFullProjectByPkDocument,
        variables,
      });

      if (response.errors) {
        return res.status(400).json(response);
      }

      if (
        obj.photo &&
        typeof _set?.photo !== "undefined" &&
        obj.photo != _set?.photo
      ) {
        const objectName = `${id}/${obj.photo}`;
        try {
          await removeFromMinioAndImagor(process.env, objectName);
        } catch (error) {}
      }

      return res.json(response);
    } catch (error) {
      console.log("patch project error", error);
      try {
        res.status(500).send((error as any).toString());
      } catch (error) {}
    }

    res.status(500).end();
  }
}
