import type { NextApiRequest, NextApiResponse } from "next";
import { unstable_getServerSession } from "next-auth";
import { getMinioClientDistant } from "../../../../../lib/apiUtils";
import { authOptions } from "../../../auth/[...nextauth]";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await unstable_getServerSession(req, res, authOptions);
  if (!session || !session.roles?.includes("admin")) {
    return res.status(401).end();
  }
  try {
    const filename = req.query.filename as string;
    const regex = /^[a-zA-Z0-9\.\-_]*$/;
    if (!regex.test(filename)) {
      return res
        .status(400)
        .send(
          "Filename can only contains numbers, letters without accent, and .-_"
        );
    }
    const minioClient = getMinioClientDistant(process.env);
    const bucketName = process.env.MINIO_BUCKET_LOADER!;
    const projectId = req.query.project_id as string;
    const objectName = `${projectId}/${filename}`;
    try {
      await minioClient.statObject(bucketName, objectName);
      return res.status(409).send(`File exists: ${filename}`);
    } catch (error) {}
    const url = await minioClient.presignedPutObject(
      bucketName,
      objectName,
      60
    );
    res.json({
      url,
    });
  } catch (error) {
    console.log("presigned_put error", error);
    try {
      res.status(500).send((error as any).toString());
    } catch (error) {}
  }

  res.status(500).end();
}
