import type { NextApiRequest, NextApiResponse } from "next";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

export default function logout(_req: NextApiRequest, res: NextApiResponse) {
  const redirectUri = encodeURIComponent(process.env["NEXTAUTH_URL"]!);
  const path = `${publicRuntimeConfig.KEYCLOAK_ISSUER}/protocol/openid-connect/logout?redirect_uri=${redirectUri}`;

  res.status(200).json({ path });
}
