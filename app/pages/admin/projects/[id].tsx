import type {
  GetServerSideProps,
  GetServerSidePropsContext,
  NextPage,
} from "next";
import { Stack, Typography } from "@mui/material";
import LayoutAdmin from "../../../components/Layout/Admin";
import { ProjectForm } from "../../../components/form/ProjectForm";
import {
  FetchDataForProjectFormDocument,
  FetchDataForProjectFormQuery,
  FetchDataForProjectFormQueryVariables,
  FetchProjectByPkDocument,
  FetchProjectByPkQuery,
  FetchProjectByPkQueryVariables,
} from "../../../generated/graphql";
import { getSession } from "next-auth/react";
import { initializeApollo } from "../../../lib/apolloClient";
import { handleGraphQLErrorsServerSide } from "../../../lib/utils";

type ProjectFormPageProps = {
  project?: FetchProjectByPkQuery["app_project_by_pk"];
  dataset?: FetchDataForProjectFormQuery;
};

const ProjectFormPage: NextPage<ProjectFormPageProps> = ({
  project,
  dataset,
}) => {
  return (
    <LayoutAdmin>
      <Stack p={2}>
        <Typography variant="h4">INFOS DU PROJET</Typography>
        {dataset && <ProjectForm project={project} dataset={dataset} />}
      </Stack>
    </LayoutAdmin>
  );
};

export const getServerSideProps: GetServerSideProps<
  ProjectFormPageProps
> = async (ctx: GetServerSidePropsContext) => {
  const session = await getSession(ctx);
  const { id } = ctx.query;
  if (!session?.roles?.includes("admin")) {
    return {
      redirect: {
        permanent: false,
        destination: `/login?callback_url=${ctx.resolvedUrl}`,
      },
    };
  }
  const client = initializeApollo(session);

  try {
    const { data: dataset, errors } = await client.query<
      FetchDataForProjectFormQuery,
      FetchDataForProjectFormQueryVariables
    >({
      query: FetchDataForProjectFormDocument,
    });
    if (errors) {
      return handleGraphQLErrorsServerSide(errors, ctx);
    }
    if (id !== "add") {
      const { data: projectData, errors } = await client.query<
        FetchProjectByPkQuery,
        FetchProjectByPkQueryVariables
      >({
        query: FetchProjectByPkDocument,
        variables: {
          id: ctx.query.id,
        },
      });
      if (errors) {
        return handleGraphQLErrorsServerSide(errors, ctx);
      }

      if (projectData?.app_project_by_pk) {
        return {
          props: {
            project: projectData.app_project_by_pk,
            dataset,
          },
        };
      }
    } else {
      if (dataset) {
        return {
          props: {
            dataset: dataset,
          },
        };
      }
    }
  } catch (error) {
    console.log(error);
  }
  return {
    props: {},
  };
};

export default ProjectFormPage;
