import {
  Box,
  Button,
  IconButton,
  InputAdornment,
  Menu,
  MenuItem,
  Stack,
  TextField,
} from "@mui/material";
import type {
  GetServerSideProps,
  GetServerSidePropsContext,
  NextPage,
} from "next";
import { useRouter } from "next/router";
import {
  deserializeRouterQuery,
  getCtxURL,
  getProjectImgURL,
  handleGraphQLErrorsServerSide,
  qsFiltersToGraphQL,
  variablesToQs,
} from "../../lib/utils";
import { getGridBuilder } from "../../lib/gridUtils";
import { getSession } from "next-auth/react";
import { initializeApollo } from "../../lib/apolloClient";
import { QueryVariables, TProject } from "../../types/app-types";
import {
  FetchDataForProjectsListDocument,
  FetchDataForProjectsListQuery,
  FetchDataForProjectsListQueryVariables,
  FetchProjectsDocument,
  FetchProjectsQuery,
  FetchProjectsQueryVariables,
} from "../../generated/graphql";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import MaterialReactTable, {
  MRT_ColumnDef,
  MRT_TableInstance,
} from "material-react-table";
import {
  ColumnFiltersState,
  PaginationState,
  SortingState,
} from "@tanstack/react-table";
import format from "date-fns/format";
import {
  IMAGOR_PRESET_NAME,
  PAGE_SIZE,
  PROJECT_STATUSES,
  PROJECT_STEPS,
} from "../../lib/constants";
import LayoutAdmin from "../../components/Layout/Admin";
import Link from "next/link";
import { Clear, Edit, Search } from "@mui/icons-material";
import { useTranslation } from "react-i18next";
import { debounce } from "lodash";

type ProjectsGridPageProps = {
  dataset?: FetchDataForProjectsListQuery;
  rows: TProject[];
  aggregate?: {
    count: number;
  } | null;
};

const ProjectsGridPage: NextPage<ProjectsGridPageProps> = ({
  rows,
  aggregate,
  dataset,
}) => {
  const router = useRouter();
  const { t } = useTranslation();
  const [anchorMenuExport, setAnchorMenuExport] = useState<null | HTMLElement>(
    null
  );
  const deserialized = deserializeRouterQuery(router.query);
  const [qs, setQs] = useState(variablesToQs(deserialized));
  const qsNoPagination = variablesToQs(deserialized, {
    exclude: ["limit", "page"],
  });
  const [colFilters, setColsFilters] = useState<ColumnFiltersState>(
    deserialized.where || []
  );
  const [pagination, setPagination] = useState<PaginationState>({
    pageIndex: (deserialized.page ? (deserialized.page || 1) - 1 : undefined)!,
    pageSize: PAGE_SIZE,
  });
  const [sorting, setSorting] = useState<SortingState>(
    deserialized.order_by?.map((col) => {
      const id = Object.keys(col)[0];
      return {
        id,
        desc: col[id] == "desc",
      };
    }) || []
  );
  const [search, setSearch] = useState(deserialized.search || "");

  const tableInstanceRef = useRef<MRT_TableInstance<TProject>>(null);

  //TODO Memo ?
  const getWhere = () => {
    return colFilters
      .filter((colFilter) => {
        const column = tableInstanceRef?.current?.getColumn(colFilter.id);
        if (!column) {
          return false;
        }
        const meta: any = column.columnDef.meta as any;
        const filterType: string = meta.filter || meta.type;
        return (
          filterType !== "multi-select" ||
          Boolean((colFilter.value as any[])?.length)
        );
      })
      .map((colFilter) => {
        const column = tableInstanceRef?.current?.getColumn(colFilter.id);
        if (!column) {
          return;
        }
        const filter: any = {
          ...colFilter,
          operator: column.columnDef._filterFn,
        };
        const meta: any = column.columnDef.meta as any;
        const filterType: string = meta.filter || meta.type;
        if (filterType === "date") {
          const fmt = "yyyy-MM-dd";
          filter.value = !Array.isArray(filter.value)
            ? format(new Date(filter.value), fmt)
            : [
                format(new Date(filter.value[0]), fmt),
                format(new Date(filter.value[1]), fmt),
              ];
        }

        return filter;
      });
  };

  useEffect(() => {
    const where = getWhere();
    //TODO better comparison
    if (JSON.stringify(where) == (router.query.where || "[]")) {
      return;
    }
    // Reset pagination to fetch data from 1st page
    setPagination({
      ...pagination,
      pageIndex: 0,
    });
  }, [colFilters]);

  useEffect(() => {
    const q: QueryVariables = {
      where: getWhere(),
      page: pagination.pageIndex + 1,
      order_by: sorting.map((sort) => {
        return {
          [sort.id]: sort.desc ? "desc" : "asc",
        };
      }),
      search,
    };

    const qs = variablesToQs(q);
    setQs(qs);
  }, [pagination, sorting, search]);

  //Avoid double call to router.replace
  useEffect(() => {
    if (qs == variablesToQs(deserializeRouterQuery(router.query))) {
      return;
    }

    delayedQuery(qs);
  }, [qs]);

  const delayedQuery = useCallback(
    debounce((q) => sendQuery(q), 300),
    []
  );

  const sendQuery = (q: string) => {
    router.replace(!q ? "/admin" : `/admin?${q}`);
  };

  const builder = getGridBuilder<TProject>("project", deserialized.where);

  //TODO beautofu getCurColumnFilterMode, getColumnFilterModeOptions with a Memo ?
  const columns: MRT_ColumnDef<TProject>[] = useMemo(
    () => [
      {
        id: "column_actions",
        header: "",
        size: 70,
        enableColumnActions: false,
        Cell: ({ row }) => {
          const link = `/admin/projects/${row.id}${
            qs ? `?goback=${encodeURIComponent(qs)}` : ""
          }`;

          return (
            <Box>
              <Link href={link} passHref>
                <IconButton component="a">
                  <Edit />
                </IconButton>
              </Link>
            </Box>
          );
        },
      },
      builder.getColProps("id", "ID"),
      builder.getColProps("step", "État", {
        accessorFn: (row: TProject) => {
          return t(`common.project.step.${row.step}`);
        },
        filterSelectOptions: PROJECT_STEPS.map((value) => {
          return {
            value,
            text: t(`common.project.step.${value}`),
          };
        }),
      }),
      builder.getColProps("photo", "Photo", {
        enableColumnFilter: false,
        Cell: ({ row }) => {
          const url = getProjectImgURL(
            row.original,
            IMAGOR_PRESET_NAME["50x50"],
            row.original.photo
          );

          return <Box>{Boolean(url) && <img src={url} />}</Box>;
        },
      }),
      builder.getColProps("label", "Libellé"),
      builder.getColProps("year", "Année"),
      builder.getColProps("budget", "Budget"),
      builder.getColProps("title", "Titre"),
      builder.getColProps("public_description", "Descr. pub."),
      builder.getColProps("private_description", "Descr. priv."),
      builder.getColProps("contact_pnrfo", "Contact PNR FO"),
      builder.getColProps("partner", "Partenaires"),
      builder.getColProps("owner", "Porteur"),
      builder.getColProps("project_territories.territory.id", "Territoires", {
        enableSorting: false,
        accessorFn: (row: TProject) => {
          const labels = (row.project_territories || []).map(
            (item) => item.territory.label
          );
          const nb = 2;
          const diff = labels.length - nb;
          return (
            <div title={diff > 0 ? labels.join(", ") : ""}>
              {labels.filter((_l, i) => i < nb).join(", ")}
              {diff > 0 ? ` +${diff}` : ""}
            </div>
          );
        },
        filterSelectOptions: dataset?.app_territory.map((item) => {
          return {
            value: item.id,
            text: item.label,
          };
        }),
      }),
      builder.getColProps("project_themes.theme.id", "Thèmes", {
        enableSorting: false,
        accessorFn: (row: TProject) => {
          return (row.project_themes || [])
            .map((item) => item.theme.label)
            .join(", ");
        },
        filterSelectOptions: dataset?.app_theme.map((item) => {
          return {
            value: item.id,
            text: item.label,
          };
        }),
      }),
      builder.getColProps("project_types.type.id", "Type", {
        enableSorting: false,
        accessorFn: (row: TProject) => {
          return (row.project_types || [])
            .map((item) => item.type.label)
            .join(", ");
        },
        filterSelectOptions: dataset?.app_type.map((item) => {
          return {
            value: item.id,
            text: item.label,
          };
        }),
      }),
      builder.getColProps("status", "Publication", {
        accessorFn: (row: TProject) => {
          return t(`common.project.status.${row.status}`);
        },
        filterSelectOptions: PROJECT_STATUSES.map((value) => {
          return {
            value,
            text: t(`common.project.status.${value}`),
          };
        }),
      }),
      builder.getColProps("created_at", "Créé le", {
        accessorFn: (row: TProject) => {
          if (!row.created_at) {
            return "";
          }
          return row.created_at?.toString().split("T")[0];
        },
      }),
      builder.getColProps("updated_at", "Modifié le", {
        accessorFn: (row: TProject) => {
          if (!row.created_at) {
            return "";
          }
          return row.created_at?.toString().split("T")[0];
        },
      }),
    ],
    [qs]
  );

  return (
    <LayoutAdmin>
      <Box height={"100%"} className="grid-wrapper">
        <MaterialReactTable
          muiTableContainerProps={{
            sx: {
              height: "clamp(350px, calc(100vh - 162px), 9999px)",
              maxHeight: "clamp(350px, calc(100vh - 162px), 9999px)",
            },
          }}
          tableInstanceRef={tableInstanceRef}
          enableStickyHeader
          enableStickyFooter
          enablePinning
          columns={columns}
          data={rows}
          renderTopToolbarCustomActions={() => (
            <Stack direction={"row"} sx={{ flex: 1 }}>
              <Link href="/admin/projects/add" passHref>
                <Button variant="contained" component="a">
                  Add
                </Button>
              </Link>
              <Button
                sx={{ ml: 2 }}
                onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
                  setAnchorMenuExport(event.currentTarget);
                }}
              >
                Exporter
              </Button>
              <Menu
                anchorEl={anchorMenuExport}
                open={Boolean(anchorMenuExport)}
                onClose={() => setAnchorMenuExport(null)}
              >
                <MenuItem
                  component="a"
                  href="/api/export/projects"
                  target="_blank"
                  onClick={() => {
                    setAnchorMenuExport(null);
                  }}
                >
                  Exporter tout
                </MenuItem>
                <MenuItem
                  component="a"
                  href={`/api/export/projects?${qsNoPagination}`}
                  target="_blank"
                  onClick={() => {
                    setAnchorMenuExport(null);
                  }}
                >
                  Utiliser les filtres et le tri de la grille
                </MenuItem>
              </Menu>
              <TextField
                size="small"
                sx={{ marginLeft: "auto" }}
                placeholder="Rechercher..."
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Search />
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton edge="end" onClick={() => setSearch("")}>
                        <Clear />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </Stack>
          )}
          getRowId={(row) => row.id}
          state={{
            columnFilters: colFilters,
            pagination,
            sorting,
          }}
          initialState={{
            showColumnFilters: Boolean(colFilters.length),
            columnVisibility: {
              id: false,
              public_description: false,
              private_description: false,
            },
            columnPinning: { left: ["column_actions"] },
          }}
          muiTablePaginationProps={{
            rowsPerPageOptions: [PAGE_SIZE],
            showFirstButton: true,
            showLastButton: true,
          }}
          rowCount={aggregate?.count}
          enableHiding={true}
          enableColumnFilterModes
          manualFiltering
          manualPagination
          manualSorting
          enableGlobalFilter={false}
          onColumnFiltersChange={setColsFilters}
          onPaginationChange={setPagination}
          onSortingChange={setSorting}
          /* muiToolbarAlertBannerProps={
              isError
                ? {
                    color: "error",
                    children: "Error loading data",
                  }
                : undefined
            } */
          /*
            onGlobalFilterChange={setGlobalFilter}
            state={{
              globalFilter,
              isLoading,
              showAlertBanner: isError,
              showProgressBars: isRefetching,
            }} */
        />
      </Box>
    </LayoutAdmin>
  );
};

export const getServerSideProps: GetServerSideProps<
  ProjectsGridPageProps
> = async (ctx: GetServerSidePropsContext) => {
  const session = await getSession(ctx);

  if (!session?.roles?.includes("admin")) {
    return {
      redirect: {
        permanent: false,
        destination: `/login?callback_url=${encodeURIComponent(
          getCtxURL(ctx)
        )}`,
      },
    };
  }
  const client = initializeApollo(session);
  try {
    const deserialized: QueryVariables = {
      ...deserializeRouterQuery(ctx.query),
    };

    const where = qsFiltersToGraphQL(
      "project",
      deserialized.where || [],
      deserialized.search
    );
    where._and.push({
      deleted_at: {
        _is_null: true,
      },
    });

    const result = await client.query<
      FetchProjectsQuery,
      FetchProjectsQueryVariables
    >({
      query: FetchProjectsDocument,
      variables: {
        offset: ((deserialized.page || 1) - 1) * PAGE_SIZE,
        limit: PAGE_SIZE,
        where: where,
        order_by: deserialized.order_by,
      },
    });

    const resultDataset = await client.query<
      FetchDataForProjectsListQuery,
      FetchDataForProjectsListQueryVariables
    >({
      query: FetchDataForProjectsListDocument,
    });

    if (result.errors || resultDataset.errors) {
      return handleGraphQLErrorsServerSide(
        (result.errors || resultDataset.errors)!,
        ctx
      );
    }

    if (!result.data) {
      console.log(JSON.stringify(result.errors));

      return {
        props: {
          rows: [],
          aggregate: {
            count: 0,
          },
        },
      };
    }
    return {
      props: {
        dataset: resultDataset.data,
        rows: result.data?.app_project || [],
        aggregate: result.data.app_project_aggregate.aggregate,
      },
    };
  } catch (error) {}

  // Pass rows to the page via props
  return {
    props: {
      rows: [],
      aggregate: {
        count: 0,
      },
    },
  };
};

export default ProjectsGridPage;
