import { useEffect, useState } from "react";
import App from "next/app";
import Head from "next/head";
import type { AppContext, AppProps } from "next/app";
import { Session } from "next-auth";
import { CacheProvider, EmotionCache } from "@emotion/react";
import { ThemeProvider, CssBaseline, createTheme } from "@mui/material";
import { I18nextProvider } from "react-i18next";
import { getSession, SessionProvider } from "next-auth/react";
import { ApolloProvider } from "@apollo/client";
import { useRouter } from "next/router";
import NextNProgress from "nextjs-progressbar";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "../style.css";

import i18n from "../lib/i18n";
import createEmotionCache from "../lib/createEmotionCache";
import themeOptions from "../lib/theme";
import { initializeApollo } from "../lib/apolloClient";

import LayoutApp from "../components/Layout/App";
import { AppContextProvider } from "../lib/AppContext";

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

const clientSideEmotionCache = createEmotionCache();
const lightTheme = createTheme(themeOptions);

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
  pageProps: any;
  session: Session;
}

function MyApp({
  Component,
  emotionCache = clientSideEmotionCache,
  pageProps,
  session,
}: MyAppProps) {
  const router = useRouter();
  const [apolloClient, setApolloClient] = useState(initializeApollo(session));

  useEffect(() => {
    i18n.changeLanguage(router.locale);
  }, [router.locale]);

  useEffect(() => {
    setApolloClient(initializeApollo(session));
  }, [session]);

  return (
    <>
      <NextNProgress color="#FF6600" />
      <SessionProvider
        session={session}
        refetchInterval={60}
        refetchOnWindowFocus={true}
      >
        <ApolloProvider client={apolloClient}>
          <CacheProvider value={emotionCache}>
            <I18nextProvider i18n={i18n}>
              <ThemeProvider theme={lightTheme}>
                {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                <AppContextProvider>
                  <CssBaseline />
                  <Head>
                    <title>Project</title>
                  </Head>
                  <LayoutApp>
                    <Component {...pageProps} />
                  </LayoutApp>

                  <style jsx global>{`
                    /* Other global styles such as 'html, body' etc... */
                    body,
                    html {
                      height: 100%;
                    }

                    #__next {
                      height: 100%;
                    }
                  `}</style>
                </AppContextProvider>
              </ThemeProvider>
            </I18nextProvider>
          </CacheProvider>
        </ApolloProvider>
      </SessionProvider>
    </>
  );
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  const appProps = await App.getInitialProps(appContext);
  const session = await getSession(appContext.ctx);

  return {
    ...appProps,
    session,
  };
};

export default MyApp;
