import { Stack } from "@mui/material";
import { NextPage } from "next";
import { useSession } from "next-auth/react";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { LoginPaper } from "../components/LoginPaper";

export const LoginPage: NextPage = () => {
  const session = useSession();
  const router = useRouter();
  useEffect(() => {
    if (session.status === "authenticated") {
      router.replace((router.query.callback_url as string) || "/");
    }
  }, [session]);
  return (
    <>
      <Head>
        <title>PNR FO</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Stack
        sx={{
          height: "100%",
          alignItems: "center",
          justifyContent: "center",
        }}
        className="app-admin"
      >
        <LoginPaper callbackUrl={router.query.callback_url as string} />
      </Stack>
    </>
  );
};

export default LoginPage;
