import {
  Clear,
  Image,
  KeyboardArrowDown,
  KeyboardArrowUp,
  Search,
} from "@mui/icons-material";
import {
  Box,
  Stack,
  List,
  ListItemText,
  Pagination,
  ListItemAvatar,
  Avatar,
  ListItem,
  Tooltip,
  Typography,
  TextField,
  InputAdornment,
  IconButton,
  Button,
  Collapse,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { format } from "date-fns";
import { debounce } from "lodash";
import { NextPage, GetServerSideProps, GetServerSidePropsContext } from "next";
import { getSession } from "next-auth/react";
import Head from "next/head";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { SimpleAutocomplete } from "../components/form-input/SimpleAutocomplete";
import LayoutPublic from "../components/Layout/Public";
import { ProjectTooltipContent } from "../components/ProjectTooltipContent";
import {
  FetchDataForProjectsListDocument,
  FetchDataForProjectsListQuery,
  FetchDataForProjectsListQueryVariables,
  FetchProjectsLightDocument,
  FetchProjectsLightQuery,
  FetchProjectsLightQueryVariables,
  Order_By,
} from "../generated/graphql";
import { initializeApollo } from "../lib/apolloClient";
import { IMAGOR_PRESET_NAME, OBJ_CONF, PAGE_SIZE } from "../lib/constants";
import {
  deserializeRouterQuery,
  getProjectImgURL,
  qsFiltersToGraphQL,
  variablesToQs,
} from "../lib/utils";
import {
  QueryVariables,
  TProject,
  TTerritorySelection,
} from "../types/app-types";

import dynamic from "next/dynamic";
import proj4 from "proj4";
import { useTranslation } from "react-i18next";
import { useAppContext } from "../lib/AppContext";

proj4.defs(
  "EPSG:2154",
  "+proj=lcc +lat_0=46.5 +lon_0=3 +lat_1=49 +lat_2=44 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs"
);

const optsDynamic = { ssr: false };
const Map = dynamic(() => import("../components/Map"), { ...optsDynamic });
const MarkerTerritory = dynamic(
  () => import("../components/Map/MarkerTerritory"),
  { ...optsDynamic }
);

type HomePageProps = {
  dataset?: FetchDataForProjectsListQuery;
  list?: {
    rows: TProject[];
    aggregate: {
      count: number;
    };
  };
};

const HomePage: NextPage<HomePageProps> = ({ dataset, list }) => {
  const router = useRouter();

  const deserialized = deserializeRouterQuery(router.query);
  const [qs, setQs] = useState(variablesToQs(deserialized));
  const [filters, setFilters] = useState<any[]>(deserialized.where || []);
  const [search, setSearch] = useState(deserialized.search || "");
  const [page, setPage] = useState(deserialized.page || 1);
  const nbPages = Math.ceil((list?.aggregate?.count || 0) / PAGE_SIZE);
  const [loading, setLoading] = useState(false);
  const [geoTerritories, setGeoTerritories] =
    useState<GeoJSON.FeatureCollection<
      GeoJSON.Geometry,
      TTerritorySelection
    > | null>(null);
  const [curTooltip, setCurTooltip] = useState<TProject | null>(null);
  const { t } = useTranslation(["common"]);
  const theme = useTheme();
  const isBreakpointMd = useMediaQuery(theme.breakpoints.up("md"));
  const [isFiltersOpen, setIsFiltersOpen] = useState(isBreakpointMd);
  const [isListOpen, setIsListOpen] = useState(isBreakpointMd);
  const appContext = useAppContext();

  useEffect(() => {
    appContext.onBoarding.tryAutoShow();
  }, []);

  useEffect(() => {
    setIsFiltersOpen(isBreakpointMd);
    setIsListOpen(isBreakpointMd);
  }, [isBreakpointMd]);

  //TODO Memo ?
  const getWhere = () => {
    return filters.map((filter) => {
      const colType: string = OBJ_CONF.project[filter.id].type;
      if (colType === "date") {
        const fmt = "yyyy-MM-dd";
        filter.value = !Array.isArray(filter.value)
          ? format(new Date(filter.value), fmt)
          : [
              format(new Date(filter.value[0]), fmt),
              format(new Date(filter.value[1]), fmt),
            ];
      }

      return filter;
    });
  };

  useEffect(() => {
    console.log("loading", loading);
    setLoading(false);
  }, [list]);

  useEffect(() => {
    const where = getWhere();
    //TODO better comparison
    if (JSON.stringify(where) == (router.query.where || "[]")) {
      return;
    }
    triggerQs();
  }, [filters]);

  useEffect(() => {
    triggerQs();
  }, [page, search]);

  const triggerQs = () => {
    setQs(
      variablesToQs({
        where: getWhere(),
        page,
        search,
      })
    );
  };

  //Avoid double call to router.replace
  useEffect(() => {
    if (qs == variablesToQs(deserializeRouterQuery(router.query))) {
      return;
    }
    delayedQuery(qs);
  }, [qs]);

  const delayedQuery = useCallback(
    debounce((q) => sendQuery(q), 300),
    []
  );

  const sendQuery = (q: string) => {
    setLoading(true);
    router.replace(!q ? "/" : `/?${q}`);
  };

  useEffect(() => {
    (async () => {
      const resp = await fetch("/assets/territories.geojson");
      setGeoTerritories(await resp.json());
    })();
    return () => {
      delayedQuery.cancel();
    };
  }, []);

  const resetFilters = () => {
    setFilters([]);
    setPage(1);
  };

  const onFilterChange = (fieldName: string, value: any) => {
    const filterExists = filters.find((filter) => {
      return filter.id == fieldName;
    });
    if (filterExists) {
      Object.assign(filterExists, {
        value: value,
      });
    } else {
      filters.push({
        id: fieldName,
        value: value,
      });
    }
    if (page !== 1) {
      // We can return without doing setFilters !
      // Crazy side effect ?
      return setPage(1);
    }
    setFilters(
      filters.filter((filter) => {
        return filter.value?.length;
      })
    );
  };

  const getTerritoryFilterExists = (id: string) => {
    return filters.find((filter) => {
      return filter.id == "project_territories.territory.id";
    });
  };

  const getTerritoryFilterIndex = (id: string): number => {
    const filterExists = getTerritoryFilterExists(id);
    if (!filterExists) {
      return -1;
    }
    const value: string[] = filterExists.value || [];
    return value.findIndex((territoryId: string) => {
      return territoryId === id;
    });
  };

  const onMarkerTerritoryClick = (id?: string) => {
    onFilterChange("project_territories.territory.id", [id]);
    if (!isBreakpointMd) {
      setIsListOpen(true);
    }
  };

  const geojson: GeoJSON.FeatureCollection<
    GeoJSON.Geometry,
    TTerritorySelection
  > = {
    type: "FeatureCollection",
    features:
      (geoTerritories?.features || []).map((geoTerritory) => {
        const territory = dataset?.app_territory.find(
          (terr) => terr.id === geoTerritory.properties.id
        );

        return {
          type: "Feature",
          geometry: geoTerritory.geometry,
          properties: {
            ...geoTerritory.properties,
            ...territory,
            isSelected: Boolean(
              territory && getTerritoryFilterIndex(territory.id) > -1
            ),
            isHighlighted: Boolean(
              territory &&
                curTooltip?.project_territories?.some(
                  (project_territory) =>
                    project_territory.territory.id === territory.id
                )
            ),
          },
        };
      }) || [],
  };

  return (
    <LayoutPublic title="PNR Forêt d'Orient">
      <Head>
        <title>PNR Forêt d'Orient</title>
        <meta
          name="description"
          content="Liste des projets du PNR Forêt d'Orient"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Stack
        sx={{
          height: "100%",
          pt: {
            xs: "52px",
            md: "130px",
          },
        }}
      >
        {!dataset || !geoTerritories ? (
          <></>
        ) : (
          <>
            <Box
              sx={{
                px: 2,
                pt: 2,
                display: {
                  md: "none",
                },
              }}
            >
              <Button
                variant="contained"
                sx={{ width: "100%" }}
                onClick={() => setIsFiltersOpen(true)}
              >
                Filtres
              </Button>
            </Box>
            <Stack
              style={{
                display: !isFiltersOpen ? "none" : "flex",
              }}
              direction={{ xs: "column", md: "row" }}
              spacing={2}
              sx={{
                backgroundColor: {
                  xs: "#FFFFFF",
                  md: "#FFFFFFCC",
                },
                px: 2,
                pb: 1,
                pt: {
                  xs: 2,
                  md: 0,
                },
                zIndex: 1100,
                position: "absolute",
                left: {
                  xs: "8px",
                  md: 0,
                },
                right: {
                  xs: "8px",
                  md: 0,
                },
                top: {
                  xs: "8px",
                  md: "auto",
                },
                bottom: {
                  xs: "8px",
                  md: "auto",
                },
                boxShadow: {
                  xs: "0 0 8px 1px #00000088",
                  md: "none",
                },
              }}
            >
              <Stack direction={{ xs: "column", md: "row" }} sx={{ flex: 1 }}>
                <Typography
                  variant="h5"
                  className="custom-title"
                  sx={{
                    textTransform: "uppercase",
                    display: {
                      md: "none",
                    },
                  }}
                >
                  FILTRES -{" "}
                  {t("filters.result", { count: list?.aggregate.count })}
                </Typography>
                <Stack
                  direction={{ xs: "column", md: "row" }}
                  spacing={2}
                  sx={{
                    flex: 1,
                    mt: {
                      xs: 2,
                      md: 0,
                    },
                  }}
                >
                  <SimpleAutocomplete
                    label="Année"
                    valueKey="year"
                    labelKey="year"
                    options={dataset.app_project_year || []}
                    value={filters.find((filter) => filter.id == "year")?.value}
                    onChange={(value) => {
                      onFilterChange(
                        "year",
                        value.map((item: any) => {
                          return item.year;
                        })
                      );
                    }}
                  />
                  <SimpleAutocomplete
                    label="Type"
                    options={dataset.app_type}
                    value={
                      filters.find(
                        (filter) => filter.id == "project_types.type.id"
                      )?.value
                    }
                    onChange={(value) => {
                      onFilterChange(
                        "project_types.type.id",
                        value.map((item: any) => {
                          return item.id;
                        })
                      );
                    }}
                  />
                  <SimpleAutocomplete
                    label="Thématique"
                    options={dataset.app_theme}
                    value={
                      filters.find(
                        (filter) => filter.id == "project_themes.theme.id"
                      )?.value
                    }
                    onChange={(value) => {
                      onFilterChange(
                        "project_themes.theme.id",
                        value.map((item: any) => {
                          return item.id;
                        })
                      );
                    }}
                  />
                  <SimpleAutocomplete
                    label="Territoire"
                    options={dataset.app_territory}
                    value={
                      filters.find(
                        (filter) =>
                          filter.id == "project_territories.territory.id"
                      )?.value
                    }
                    onChange={(value) => {
                      onFilterChange(
                        "project_territories.territory.id",
                        value.map((item: any) => {
                          return item.id;
                        })
                      );
                    }}
                  />
                </Stack>
              </Stack>
              <Stack
                direction={"row"}
                spacing={2}
                sx={{
                  justifyContent: {
                    xs: "end",
                    md: "start",
                  },
                }}
              >
                <Button
                  size="large"
                  variant="contained"
                  style={{ height: "56px" }}
                  onClick={resetFilters}
                  sx={{
                    display: {
                      xs: "none",
                      md: "inline-flex",
                    },
                  }}
                >
                  Réinitialiser
                </Button>
                <Button
                  size="large"
                  onClick={resetFilters}
                  sx={{
                    display: {
                      md: "none",
                    },
                  }}
                >
                  Réinitialiser
                </Button>
                <Button
                  size="large"
                  variant="contained"
                  onClick={() => setIsFiltersOpen(false)}
                  sx={{
                    display: {
                      md: "none",
                    },
                  }}
                >
                  OK
                </Button>
              </Stack>
            </Stack>
            <Stack
              sx={{
                flex: 1,
                overflow: "hidden",
                pt: {
                  md: "64px",
                },
                position: "relative",
              }}
              direction="row"
            >
              <Stack
                sx={{
                  background: "#FFF",
                  zIndex: 1001,
                  position: {
                    xs: "absolute",
                    md: "relative",
                  },
                  width: {
                    xs: "100%",
                    md: "300px",
                  },
                  height: {
                    md: "100%",
                  },
                  paddingRight: "1px",
                }}
              >
                <Box>
                  <Box
                    sx={{
                      p: 1,
                      display: {
                        xs: "none",
                        md: "block",
                      },
                    }}
                  >
                    <TextField
                      fullWidth
                      size="small"
                      placeholder="Rechercher..."
                      value={search}
                      onChange={(e) => {
                        if (page !== 1) {
                          return setPage(1);
                        }
                        setSearch(e.target.value);
                      }}
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Search />
                          </InputAdornment>
                        ),
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              edge="end"
                              onClick={() => setSearch("")}
                            >
                              <Clear />
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                    />
                  </Box>

                  <Stack
                    onClick={() => {
                      if (!isBreakpointMd) {
                        setIsListOpen(!isListOpen);
                      }
                    }}
                    direction="row"
                    sx={{
                      alignItems: "center",
                      justifyContent: "space-between",
                      height: {
                        xs: "54px",
                        md: "auto",
                      },
                    }}
                  >
                    <Typography
                      variant="h5"
                      className="custom-title"
                      sx={{
                        p: 1,
                        pl: {
                          xs: 2,
                          md: 1,
                        },
                        textTransform: "uppercase",
                      }}
                    >
                      Liste des projets
                    </Typography>
                    <KeyboardArrowDown
                      fontSize="large"
                      sx={{
                        display:
                          isListOpen || isBreakpointMd ? "none" : undefined,
                      }}
                    />
                    <KeyboardArrowUp
                      fontSize="large"
                      sx={{
                        display:
                          !isListOpen || isBreakpointMd ? "none" : undefined,
                      }}
                    />
                  </Stack>
                </Box>
                <Collapse
                  in={isListOpen}
                  sx={{
                    flex: {
                      md: 1,
                    },
                    "& .MuiCollapse-wrapper": {
                      height: {
                        md: "100%",
                      },
                    },
                    "& .MuiCollapse-wrapperInner": {
                      height: {
                        md: "100%",
                      },
                    },
                  }}
                >
                  <Stack
                    sx={{
                      height: {
                        xs: "calc(100vh - 158px)",
                        md: "100%",
                      },
                    }}
                  >
                    <List sx={{ flex: 1, px: 1, overflow: "auto" }}>
                      {list?.rows.map((project, index) => {
                        const link = `/projects/${project.id}${
                          qs ? `?goback=${encodeURIComponent(qs)}` : ""
                        }`;
                        return (
                          <Tooltip
                            key={project.id}
                            arrow
                            placement="right"
                            title={
                              <ProjectTooltipContent
                                project={project}
                                link={link}
                              />
                            }
                            onOpen={() => {
                              if (isBreakpointMd) {
                                setCurTooltip(project);
                              }
                            }}
                            onClose={() => setCurTooltip(null)}
                            PopperProps={{
                              className: "tooltip-light",
                              popperOptions: {
                                modifiers: [
                                  {
                                    name: "offset",
                                    enabled: true,
                                    options: {
                                      offset: [0, -10],
                                    },
                                  },
                                ],
                              },
                            }}
                          >
                            <ListItem
                              button
                              component="a"
                              href={`/projects/${project.id}`}
                              alignItems="flex-start"
                              onClick={(e) => {
                                e.preventDefault();
                                router.push(link);
                              }}
                              sx={{
                                pl: 0,
                                pr: 1,
                              }}
                            >
                              <Stack
                                sx={{
                                  maxWidth: "64px",
                                  minWidth: "64px",
                                  alignItems: "center",
                                }}
                              >
                                <ListItemAvatar
                                  sx={{
                                    minWidth: "40px",
                                  }}
                                >
                                  <Avatar
                                    alt={`icon ${project.label}`}
                                    src={getProjectImgURL(
                                      project,
                                      IMAGOR_PRESET_NAME["50x50"],
                                      project.photo
                                    )}
                                  >
                                    <Image />
                                  </Avatar>
                                </ListItemAvatar>
                                {(() => {
                                  const leaderTheme =
                                    project.project_themes?.find((t) =>
                                      t.theme.id.startsWith("leader_")
                                    );

                                  return !leaderTheme ? null : (
                                    <ListItemAvatar>
                                      <Avatar
                                        alt={`icon ${leaderTheme.theme.label}`}
                                        src={`/assets/${leaderTheme.theme.id}.png`}
                                        variant="square"
                                        sx={{
                                          width: "64px",
                                          height: "40px",
                                          ".MuiAvatar-img": {
                                            "object-fit": "contain",
                                          },
                                        }}
                                      >
                                        <Image />
                                      </Avatar>
                                    </ListItemAvatar>
                                  );
                                })()}
                              </Stack>
                              <ListItemText
                                sx={{
                                  pl: 1,
                                }}
                                primary={
                                  <Typography
                                    variant="button"
                                    component="strong"
                                  >
                                    {project.label}
                                  </Typography>
                                }
                                secondary={
                                  <>
                                    <Typography component={"span"}>
                                      {`${
                                        Number(
                                          project.project_territories?.length
                                        ) > 1
                                          ? `${Number(
                                              project.project_territories
                                                ?.length
                                            )} communes`
                                          : project.project_territories?.[0]
                                              .territory.label
                                      }`}
                                    </Typography>
                                  </>
                                }
                              />
                            </ListItem>
                          </Tooltip>
                        );
                      })}
                    </List>
                    {nbPages > 1 && (
                      <Pagination
                        page={page}
                        count={nbPages}
                        size="small"
                        color="primary"
                        sx={{
                          borderTop: "1px solid #CCC",
                          p: 2,
                          display: "flex",
                          justifyContent: "center",
                        }}
                        onChange={(e, page) => setPage(page)}
                      />
                    )}
                  </Stack>
                </Collapse>
              </Stack>
              <Box
                sx={{
                  flex: 1,
                  pt: {
                    xs: "54px",
                    md: 0,
                  },
                }}
              >
                <Map geojson={geojson}>
                  {geojson.features
                    .filter((feature) =>
                      Boolean(
                        feature.properties.filtered_projects_aggregate
                          ?.aggregate?.count
                      )
                    )
                    .map((feature) => {
                      return (
                        <MarkerTerritory
                          key={feature.properties.id}
                          feature={feature}
                          onClick={() =>
                            onMarkerTerritoryClick(feature.properties.id)
                          }
                        />
                      );
                    })}
                </Map>
              </Box>
            </Stack>
          </>
        )}
      </Stack>
    </LayoutPublic>
  );
};

export const getServerSideProps: GetServerSideProps<HomePageProps> = async (
  ctx: GetServerSidePropsContext
) => {
  const session = await getSession(ctx);
  const client = initializeApollo(session, {
    isAnonymous: true,
  });
  try {
    const deserialized: QueryVariables = {
      ...deserializeRouterQuery(ctx.query),
    };

    const where = qsFiltersToGraphQL(
      "project",
      !deserialized.where
        ? []
        : deserialized.where.map((filter) => {
            return {
              ...filter,
              operator: "in",
            };
          }),
      ctx.query.search as string
    );
    where._and.push(
      {
        status: {
          _eq: "online",
        },
      },
      {
        deleted_at: {
          _is_null: true,
        },
      }
    );

    const result = await client.query<
      FetchProjectsLightQuery,
      FetchProjectsLightQueryVariables
    >({
      query: FetchProjectsLightDocument,
      variables: {
        offset: ((deserialized.page || 1) - 1) * PAGE_SIZE,
        limit: PAGE_SIZE,
        where: where,
        order_by: { label: Order_By.Asc },
      },
    });

    if (!result.data) {
      console.log("result !", JSON.stringify(result));
    }

    /* const where_territory_projects_aggregate = cloneDeep(where);
    where_territory_projects_aggregate._and =
      where_territory_projects_aggregate._and.filter((filter) => {
        return Object.keys(filter).indexOf("project_territories") < 0;
      }); */

    const { data: dataset } = await client.query<
      FetchDataForProjectsListQuery,
      FetchDataForProjectsListQueryVariables
    >({
      query: FetchDataForProjectsListDocument,
      variables: {
        where_territory_projects_aggregate: {
          project: where,
        },
      },
    });

    dataset.app_territory = dataset.app_territory.filter((territory) => {
      return Boolean(territory.projects_aggregate.aggregate?.count);
    });

    return {
      props: {
        dataset: dataset || null,
        list: {
          rows: result.data?.app_project || [],
          aggregate: result.data?.app_project_aggregate.aggregate || {
            count: 0,
          },
        },
      },
    };
  } catch (error) {
    console.log("ERROR", JSON.stringify(error));
  }

  // Pass rows to the page via props
  return {
    props: {},
  };
};

export default HomePage;
