import {
  Box,
  Breadcrumbs,
  Container,
  Grid,
  Link as MuiLink,
  Paper,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { endsWith } from "lodash";
import { GetServerSideProps, GetServerSidePropsContext, NextPage } from "next";
import { getSession } from "next-auth/react";
import Head from "next/head";
import { useRouter } from "next/router";
import React from "react";
import LayoutPublic from "../../components/Layout/Public";
import {
  FetchProjectByPkDocument,
  FetchProjectByPkQuery,
  FetchProjectByPkQueryVariables,
} from "../../generated/graphql";
import { initializeApollo } from "../../lib/apolloClient";
import { IMAGOR_PRESET_NAME } from "../../lib/constants";
import { getMinioUrl, getProjectImgURL, markdownToHtml } from "../../lib/utils";
import { TProject } from "../../types/app-types";
import { Navigation, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { useTranslation } from "react-i18next";
import Link from "next/link";
import { ArrowRight } from "@mui/icons-material";

interface ProjectPageProps {
  project: TProject;
}

const ProjectPage: NextPage<ProjectPageProps> = ({ project }) => {
  const photo = getProjectImgURL(
    project,
    IMAGOR_PRESET_NAME["500x375"],
    project.photo
  );
  const router = useRouter();
  const { t } = useTranslation(["common"]);
  const theme = useTheme();
  const isBreakpointXs = useMediaQuery(theme.breakpoints.down("md"));

  const nbPhotos = project.photos?.length || 0;
  const backUrl = `/${router.query.goback ? `?${router.query.goback}` : "/"}`;

  return (
    <LayoutPublic title={project.label} backUrl={backUrl}>
      <Head>
        <title>Projet : {project.label}</title>
        <meta name="description" content="Un projet du PNR Forêt d'Orient" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Stack
        className="content-public"
        sx={{ pb: 3, alignItems: "center" }}
        spacing={3}
      >
        {!isBreakpointXs && (
          <Container
            disableGutters
            sx={{
              pb: 5,
            }}
          >
            <Typography variant="h2" sx={{ textAlign: "center", mt: 6, mb: 1 }}>
              {project.title}
            </Typography>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Breadcrumbs separator={<ArrowRight color="primary" />}>
                <Link href={backUrl} passHref>
                  <MuiLink variant="overline" underline="hover">
                    Revenir à la carte
                  </MuiLink>
                </Link>
                <Typography color="text.primary" variant="overline">
                  {project.label}
                </Typography>
              </Breadcrumbs>
            </Box>
          </Container>
        )}
        <Container disableGutters>
          <Stack
            direction={{
              xs: "column",
              md: "row",
            }}
          >
            {Boolean(photo) && (
              <Box
                sx={{
                  pt: {
                    xs: 0,
                    md: 6,
                  },
                  pb: {
                    xs: 0,
                    md: 6,
                  },
                  zIndex: 1,
                }}
              >
                <img
                  src={photo}
                  alt={`photo ${project.label}`}
                  style={{
                    display: "block",
                    width: isBreakpointXs ? "100%" : "auto",
                    height: "auto",
                  }}
                />
              </Box>
            )}
            <Box
              className="project"
              sx={{
                flex: 1,
                background: "#EEE",
                ml: { md: -8 },
                pl: {
                  xs: 2,
                  md: 12,
                },
                pt: {
                  xs: 2,
                  md: 10,
                },
                pb: 4,
                pr: 2,
              }}
            >
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Typography color="text.secondary">
                    {t("project.pageLabels.type", {
                      count: project.project_types?.length,
                    })}
                  </Typography>
                  <Typography variant="subtitle1" component="p">
                    {project.project_types
                      ?.map((row) => {
                        return row.type.label;
                      })
                      .join(", ")}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography variant="caption" color="text.secondary">
                    {t("project.pageLabels.year")}
                  </Typography>
                  <Typography>{project.year}</Typography>
                </Grid>
                {Boolean(project.budget) && (
                  <Grid item xs={6}>
                    <Typography variant="caption" color="text.secondary">
                      {t("project.pageLabels.budget")}
                    </Typography>
                    <Typography>{project.budget} EUR</Typography>
                  </Grid>
                )}
                <Grid item xs={6}>
                  <Typography variant="caption" color="text.secondary">
                    {t("project.pageLabels.theme", {
                      count: project.project_themes?.length,
                    })}
                  </Typography>
                  <Typography>
                    {project.project_themes
                      ?.map((row) => {
                        return row.theme.label;
                      })
                      .join(", ")}
                  </Typography>
                </Grid>
                {Boolean(project.partner) && (
                  <Grid item xs={6}>
                    <Typography variant="caption" color="text.secondary">
                      {t("project.pageLabels.partner")}
                    </Typography>
                    <Typography>{project.partner}</Typography>
                  </Grid>
                )}
                {Boolean(project.project_financings?.length) && (
                  <Grid item xs={6}>
                    <Typography variant="caption" color="text.secondary">
                      {t("project.pageLabels.financing", {
                        count: project.project_financings?.length,
                      })}
                    </Typography>
                    <Typography>
                      {project.project_financings
                        ?.map((row) => {
                          return row.financing.label;
                        })
                        .join(", ")}
                    </Typography>
                  </Grid>
                )}
                <Grid item xs={6}>
                  <Typography variant="caption" color="text.secondary">
                    {t("project.pageLabels.contact_pnrfo")}
                  </Typography>
                  <Typography>{project.contact_pnrfo}</Typography>
                </Grid>
              </Grid>
            </Box>
          </Stack>
        </Container>
        <Container
          disableGutters
          sx={{
            px: {
              xs: 2,
              md: 0,
            },
          }}
        >
          <Paper sx={{ px: 3, py: 1 }}>
            <Typography component={"div"}>
              <div
                dangerouslySetInnerHTML={{
                  __html: project.public_description || "",
                }}
              ></div>
            </Typography>
          </Paper>
        </Container>
        {Boolean(project.private_description) && (
          <Container
            disableGutters
            sx={{
              px: {
                xs: 2,
                md: 0,
              },
            }}
          >
            <Paper sx={{ px: 3, py: 1 }}>
              <Typography component={"div"}>
                <div
                  dangerouslySetInnerHTML={{
                    __html: project.private_description || "",
                  }}
                ></div>
              </Typography>
            </Paper>
          </Container>
        )}
        <Container
          disableGutters
          sx={{
            px: {
              xs: 2,
              md: 0,
            },
          }}
        >
          <Paper sx={{ px: 3, py: 3, background: "#EEE" }}>
            <Typography
              variant="h5"
              className="custom-title"
              sx={{ textTransform: "uppercase" }}
            >
              {t("project.pageLabels.territory", {
                count: project.project_territories?.length,
              })}
            </Typography>
            <Typography>
              {project.project_territories
                ?.map((row) => {
                  return row.territory.label;
                })
                .join(", ")}
            </Typography>
          </Paper>
        </Container>
        {nbPhotos > 0 && (
          <Container disableGutters sx={{ position: "relative" }}>
            <Swiper
              modules={[Navigation, Pagination]}
              navigation
              pagination={{ clickable: true }}
            >
              {project.photos?.map((photo) => {
                return (
                  <SwiperSlide
                    key={photo.id}
                    style={{
                      background: "#EEE",
                      height: isBreakpointXs ? "auto" : "600px",
                    }}
                  >
                    <img
                      src={getProjectImgURL(
                        project,
                        IMAGOR_PRESET_NAME["800x600"],
                        photo.filename
                      )}
                      style={{
                        display: "block",
                        width: "100%",
                        height: "100%",
                        objectFit: "contain",
                      }}
                    />
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </Container>
        )}
        {project.docs && Boolean(project.docs.length) && (
          <Stack sx={{ width: "100%", background: "#EEE", pb: 2 }}>
            <Stack
              direction={{
                xs: "column",
                sm: "row",
              }}
              sx={{
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: {
                  xs: "center",
                  md: "normal",
                },
              }}
            >
              {project.docs?.map((doc) => {
                return (
                  <Box
                    key={doc.id}
                    sx={{
                      width: "200px",
                      mt: "40px",
                      mx: {
                        sm: 2,
                      },
                    }}
                  >
                    <MuiLink
                      href={
                        doc.url || getMinioUrl(`${project.id}/${doc.filename}`)
                      }
                      color={"inherit"}
                      underline="none"
                      target={"_blank"}
                    >
                      <img
                        src={getProjectImgURL(
                          project,
                          IMAGOR_PRESET_NAME["200x280"],
                          doc.preview || doc.filename
                        )}
                        style={{
                          display: "block",
                          width: "100%",
                          height: "auto",
                          objectFit: "cover",
                          background: "#EEE",
                        }}
                      />
                      {
                        <Typography
                          variant="h5"
                          className="custom-title"
                          sx={{
                            visibility: Boolean(doc.label)
                              ? "visible"
                              : "hidden",
                          }}
                        >
                          {doc.label}
                        </Typography>
                      }
                    </MuiLink>
                  </Box>
                );
              })}
            </Stack>
          </Stack>
        )}
      </Stack>
    </LayoutPublic>
  );
};

export const getServerSideProps: GetServerSideProps<ProjectPageProps> = async (
  ctx: GetServerSidePropsContext
) => {
  const session = await getSession(ctx);
  const { id } = ctx.query;
  const client = initializeApollo(session);

  const { data: projectData } = await client.query<
    FetchProjectByPkQuery,
    FetchProjectByPkQueryVariables
  >({
    query: FetchProjectByPkDocument,
    variables: {
      id,
    },
  });

  if (
    !projectData?.app_project_by_pk ||
    projectData?.app_project_by_pk.status != "online" ||
    projectData?.app_project_by_pk.deleted_at !== null
  ) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      project: {
        ...projectData.app_project_by_pk,
        public_description: await markdownToHtml(
          projectData.app_project_by_pk.public_description
        ),
        private_description: await markdownToHtml(
          projectData.app_project_by_pk.private_description
        ),
        photos: projectData.app_project_by_pk.project_files?.filter((file) => {
          return ["png", "jpg", "jpeg"].some((ext) => {
            return endsWith((file.filename || "").toLowerCase(), `.${ext}`);
          });
        }),
        docs: projectData.app_project_by_pk.project_files?.filter((file) => {
          return !["png", "jpg", "jpeg"].some((ext) => {
            return endsWith((file.filename || "").toLowerCase(), `.${ext}`);
          });
        }),
      },
    },
  };
};

export default ProjectPage;
