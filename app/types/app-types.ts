import {
  App_Project_File,
  FetchDataForProjectsListQuery,
  FetchProjectsQuery,
} from "../generated/graphql";
import { IMAGOR_PRESET_NAME, OBJ_CONF } from "../lib/constants";

export type QueryVariables = {
  page?: number;
  limit?: number;
  order_by?: {
    [field: string]: "asc" | "desc" | null | undefined;
  }[];
  where?: any[];
  search?: string;
};

export type QueryVariablesStr = {
  page?: string;
  limit?: string;
  order_by?: string;
  where?: string;
  search?: string;
};

export type ErrorMessage = {
  label: string;
  message: string;
};

export type ObjectName = keyof typeof OBJ_CONF;

export type TProjectFile = Partial<App_Project_File> & {
  presets?: {
    file?: Partial<Record<IMAGOR_PRESET_NAME, string>>;
    preview?: Partial<Record<IMAGOR_PRESET_NAME, string>>;
  };
};

export type TProject = Partial<
  Omit<FetchProjectsQuery["app_project"][number], "project_files">
> & {
  photoPresets?: Partial<Record<IMAGOR_PRESET_NAME, string>>;
  project_files?: TProjectFile[];
  photos?: TProjectFile[];
  docs?: TProjectFile[];
};

export type TFile = File & {
  preview: string;
  slugified: string;
};

export type TGeometry = {
  type: string;
  crs?: {
    type: "name";
    properties: {
      name: "urn:ogc:def:crs:EPSG::2154";
    };
  };
  coordinates: number[];
};

export type TTerritory = Partial<
  Omit<FetchDataForProjectsListQuery["app_territory"][number], "coords">
> & {
  coords?: TGeometry;
};

export type TTerritorySelection = TTerritory & {
  isHighlighted: boolean;
  isSelected: boolean;
};
